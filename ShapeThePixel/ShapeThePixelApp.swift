//
//  ShapeThePixelApp.swift
//  ShapeThePixel
//
//  Created by Semba on 27/06/2021.
//

import SwiftUI

@main
struct ShapeThePixelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
