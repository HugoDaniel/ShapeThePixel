import "https://deno.land/x/dotenv/load.ts";


console.log(
`
globalThis.Environment = {
  iceServer: "${Deno.env.get("ICESERVER") ?? "turn:in.shapethepixel.com:5349"}",
  iceCredential: "${Deno.env.get("ICECREDENTIAL") ?? ""}",
  iceUsername: "${Deno.env.get("ICEUSERNAME") ?? ""}",
  signalingServer: "${Deno.env.get("SIGNALINGSERVER") ?? ""}"
}
`
)
