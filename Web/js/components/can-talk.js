class CanTalk extends HTMLElement {
	template = `
<style>
	.talk {
		list-style-type: none;
		padding: 0;
		margin: 0;
		display: flex;
		flex-direction: column;
		align-items: flex-start;
		transition: all 300ms ease-out;
	}
	.talk li {
		text-transform: none;
		background: #E7E7EE;
		padding: 10px;
		border-radius: 12px;
		font-size: 14px;
		transition: all 300ms ease-out;
		margin-top: 3px;
		opacity: 0;
	}
	.talk li.user, .talk li.typing.user {
		background: #8080F0;
		color: #FFEEF0;
		margin-left: 64px;
		align-self: flex-end;
	}
	.talk li:nth-last-child(n+6).user,
	.talk li:nth-last-child(n+6).single,
	.talk li:nth-last-child(n+6).text {
		opacity: 0;
		visibility: hidden;
		animation-fill-mode: none;
	}
	.talk li:nth-last-child(5).user,
	.talk li:nth-last-child(5).single,
	.talk li:nth-last-child(5).text {
		opacity: 0;
		animation-fill-mode: none;
	}
	.talk li:nth-last-child(4).user,
	.talk li:nth-last-child(4).single,
	.talk li:nth-last-child(4).text {
		opacity: 1;
		animation-fill-mode: none;
	}

	.talk li.text:nth-last-child(2) {
		border-radius: 12px 12px 12px 2px;
	}
	.talk li:nth-last-child(1) {
		background: transparent;
	}
	.talk li.user:nth-last-child(3),
	.talk li.user:nth-last-child(2),
	.talk li.user:nth-last-child(1) {
		border-radius: 12px 12px 2px 12px;
	}
	.talk li.typing {
		padding: 0;
		height: 36px;
		width: 56px;
		display: flex;
		justify-content: center;
		align-items: center;
		opacity: 1;
	}
	.talk li.single {
		padding: 0;
		background: transparent;
		border-radius: 0;
		font-size: 30px;
		opacity: 1;
	}
	.talk .circles .circle {
		transform: translateY(10px);
	}
	.talk .circles .circle:nth-child(1) {
		animation: 500ms ease-out 0s infinite alternate typing;
	}
	.talk .circles .circle:nth-child(2) {
		animation: 500ms ease-out 83ms infinite alternate typing;
	}
	.talk .circles .circle:nth-child(3) {
		animation: 500ms ease-out 166ms infinite alternate typing;
	}
	.talk li.user,
	.talk li.text {
		opacity: 0;
		animation: 600ms ease-in appear;
		animation-fill-mode: forwards;
	}
	/** Limits
	 * 20 - bottom
	 * 0 - top
	 * 10 - center
	 */
	@keyframes typing {
		from { transform: translateY(15px); }
		to   { transform: translateY(5px); }
	}
	@keyframes appear {
		from { opacity: 0; }
		to   { opacity: 1; }
	}
</style>
<slot name="speaking-agent"></slot>
<ol class="talk">
</ol>
`
	/**
	 * Initializes this component. Creates a shadow dom root and an empty
	 * hidden `<li>` element to hold the next speech bubble. It is useful
	 * that this `<li>` element is already available at the DOM before the
	 * message is created to allow for a smooth animation of messages showing
	 * up in the .talk container.
	 *
	 * Sets the following properties:
	 *
	 * - `this.talkElement` - the .talk `<ol>` container of messages
	 * - `this.currentSentence` - this hidden `<li>` element for the next message
	 */
	init() {
		this.attachShadow({ mode: "open" })
		this.shadowRoot.innerHTML = this.template

		this.talkElement = this.shadowRoot.querySelector(".talk")

		this.currentSentence = document.createElement("li")
		this.talkElement.appendChild(this.currentSentence)
	}
	/**
	 * Creates the typing animated circles. This is an `<svg>` with circles,
	 * if the `isUser` option is set, then these circles will be styled with
	 * the user colors and appearance. Otherwise the normal gray colors are
	 * used instead.
	 *
	 * The `<svg>` element is placed at the `circlesLI` argument element.
	 * If this argument is undefined (the default), a new `<li>` is created
	 * by this function.
	 *
	 * This function sets the `circlesLI` (or the created `<li>`) to be
	 * available in the `this.circles` property.
	 *
	 * The final `<li>` has the class name "typing".
	 */
	createCircles(
		circlesLI = undefined,
		{ isUser = false, color = "#778899" } = {}
	) {
		// "r" is the radius of each circle, all measures are set relative
		// to this amount:
		const r = 3.8,
			// "w" is the total width of the circles container:
			// diameter: r*2
			// number of circles: 3
			// total margin between circles: r
			w = r * 2 * 3 + r
		const svgNS = "http://www.w3.org/2000/svg"
		// The main SVG element wraps the mouth
		const circles = document.createElementNS(svgNS, "svg")
		// Body dimensions and viewport
		circles.setAttributeNS(null, "viewBox", `0 0 ${Math.round(w)} 32`)
		circles.setAttributeNS(null, "width", `${w}px`)
		//circles.setAttributeNS(null, "height", `${r * 2 - r / 2}px`)
		circles.setAttributeNS(null, "height", `32px`)
		circles.setAttributeNS(null, "class", "circles")
		// The body is just a simple rect with rounded corners (svg rect rx)
		const circle = document.createElementNS(
			"http://www.w3.org/2000/svg",
			"circle"
		)
		// These values are coming from the Figma export file
		circle.setAttributeNS(null, "width", `${r * 2}`)
		circle.setAttributeNS(null, "height", `${r * 2}`)
		circle.setAttributeNS(null, "r", `${r}`)
		circle.setAttributeNS(null, "cx", `${r}`)
		circle.setAttributeNS(null, "cy", `${r}`)
		circle.setAttributeNS(null, "fill", color)
		circle.setAttributeNS(null, "class", "circle")
		let circle2 = circle.cloneNode()
		circle2.setAttributeNS(null, "cx", `${r * 2 + r / 2 + r}`)
		let circle3 = circle.cloneNode()
		circle3.setAttributeNS(null, "cx", `${r * 4 + 2 * r}`)
		circles.append(circle, circle2, circle3)
		if (!circlesLI) {
			circlesLI = document.createElement("li")
		}
		circlesLI.classList.add("typing")
		circlesLI.classList.toggle("user", isUser)
		circlesLI.appendChild(circles)
		this.circles = circlesLI
	}

	/**
	 * Sets the location for the text messages, receives the
	 * BoundingClientRect of the assigned node to the <slot> of this
	 * component. The BoundingClientRect calculating is expected to be
	 * done elsewhere, at your convenience.
	 */
	updateLocation({ speakerRect, margin = -20 } = { margin: -20 }) {
		if (speakerRect) {
			this.speakerRect = speakerRect
		}
		const rect = this.speakerRect || { width: 1, x: 0, y: 0 }
		const marginFromSpeaker = margin
		this.talkElement.style.position = "absolute"
		this.talkElement.style.maxWidth = `${rect.width * 2.25}px`
		this.talkRect = this.talkElement.getBoundingClientRect()
		this.talkElement.style.left = `${rect.x}px`
		this.talkElement.style.top = `${
			rect.y - this.talkRect.height + marginFromSpeaker
		}px`
	}

	/**
	 * Toggles the typing circles animation for the user.
	 * If the argument `showTyping` is true, the circles are created and
	 * placed in the .talk container as a user "typing".
	 *
	 * If the argument is false, they get removed.
	 */
	userTyping(showTyping) {
		if (showTyping) {
			this.createCircles(this.currentSentence, {
				isUser: true,
				color: "#FFEEDD",
			})
		} else {
			// Remove the thinking circles
			this.currentSentence.parentElement.removeChild(this.currentSentence)
			const speechElement = document.createElement("li")
			this.currentSentence = speechElement
			this.talkElement.appendChild(speechElement)
		}
		// This updates the .talk container location to make sure it has
		// space for the new user typing circles messages. The provided
		// margin works in negative numbers (-8 means, 8px space from the
		// talking element - cliddy)
		this.updateLocation({ margin: -8 })
	}

	/**
	 * Show a speech bubble with a message.
	 * This speech bubble can be a "user" bubble (shown on the right, with
	 * the user background color and text color), or a "normal" bubble, from
	 * the speaking agent - shown aligned to the left, with the gray agent
	 * background color.
	 *
	 * Animated circles are shown to provide a "typing" animation similar to
	 * most expected chat apps. The typing animation is not shown if this is
	 * a user message (please use the `userTyping(true/false)` to show/hide
	 * them according to user interactions).
	 *
	 * The "timeout" option sets the time that the animated circles are
	 * visible before the message is shown.
	 */
	say(
		something,
		{ isUser = false, timeout = Math.min(1500, Math.random() * 2600) } = {}
	) {
		return new Promise((resolve, reject) => {
			// Top margin, in pixel negative units (-10 -> 10 pixels away from
			// speaker agent, 10 -> 10 pixels closer to the agent)
			// This is useful to adjust the main ".talk" container whenever
			// there is a new speech bubble to show (which is done in
			// `this.updateLocation()`)
			let margin = 30
			// "this.currentSentence"
			let speechElement = this.currentSentence
			// Small sentences do not trigger the "typing dots" animation and
			// do not need to wait for a timeout and the "animationend" event
			// to show up.
			if (something.length <= 2) {
				this.currentSentence.textContent = something
				this.currentSentence.classList.add("single")
				this.currentSentence.classList.toggle("user", isUser)
				margin = 0
			} else {
				// User sentences don't have the "typing dots" animation
				// To show the typing dots animation for the user there is the
				// specific function "userTyping()"
				if (!isUser) {
					this.createCircles(this.currentSentence)
					speechElement = this.circles
				}
				setTimeout(
					() => {
						// Timeout has happened, this means that it is time
						// to replace the typing circles with the message to
						// display
						const typedText = document.createElement("span")
						typedText.textContent = something
						// The message to be shown will have the html structure
						// of: <li class="text"><span>message</span></li>
						const li = document.createElement("li")
						li.appendChild(typedText)
						li.classList.add("text")
						li.classList.toggle("user", isUser)
						// The circles are replaced with the message <li>
						speechElement.replaceWith(li)
						speechElement = li
						// Update the .talk messages container location to make
						// space for the new message height
						this.updateLocation({ margin: 0 })
						speechElement.addEventListener("animationend", () => {
							// Only call resolve after the animation that
							// shows the speech bubble is finished.
							// This allows new messages to show only after this
							// one has finished animating (the returned promise
							// is useful to allow messages to be stacked)
							resolve(speechElement)
						})
					},
					isUser ? 0 : timeout
					//      ^ this is the time it takes to show the typing dots
				)
			}
			// Always keep one hidden <li> element at the bottom, referenced by
			// "this.currentSentence". This hidden element is used to prepare
			// the new sentence that is going to be displayed and trigger its
			// animation from the bottom.
			//
			// Now that the sentence is shown, it is time to create a new
			// hidden <li> element and place it at the "currentSentence" attrib.
			// ready to be filled with text
			const newSpeechElement = document.createElement("li")
			this.currentSentence = newSpeechElement
			this.talkElement.appendChild(newSpeechElement)
			this.updateLocation({ margin })
			if (something.length <= 2) {
				resolve(speechElement)
			}
		})
	}
}
customElements.define("can-talk", CanTalk)
