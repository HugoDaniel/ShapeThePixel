/**
 * The `<loading-messages>` web component.
 *
 * The loading procedure consists in showing the Cliddy web component as soon
 * as possible and make it speak some words. This loading component is
 * responsible to show some loading messages while the app loads.
 *
 * It expects cliddy to registered and ready to display messages.
 *
 * It is intended to be used in HTML inside the cliddy web component:
 *
 * ```html
 * <cliddy-friend>
 *   <loading-messages></loading-messages>
 * </cliddy-friend>
 * ```
 *
 */
class Loading extends HTMLElement {}

customElements.define("loading-messages", Loading)

customElements.whenDefined("cliddy-friend").then(() => {
	window.addEventListener("DOMContentLoaded", () => {
		var talk = document.querySelector("can-talk")
		var cliddy = document.querySelector("cliddy-friend")
		// If cliddy is not found, return early to avoid errors and
		// registering the event handlers associated with it
		if (!cliddy) {
			console.warn("Cliddy not found")
			return
		}
		cliddy.init()
		cliddy.positionFor(window.innerWidth, window.innerHeight)
		talk.init()
		talk.updateLocation({ speakerRect: cliddy.clientRect })
		let talking = Promise.resolve()
		let userTyping = false
		this.addEventListener("keydown", async e => {
			const sentences = [
				"I wish I could be at the beach",
				"...Yeah...",
				"💩",
				"Having fun?",
				"Let's try something different",
				"All my clothes are made of wool",
				"This is a big piece of text, I am not even sure why it is so big, maybe it will break something or introduce some kind of flickering. I do hope it works out fine\nYeah, fine\nPeace.",
			]
			if (e.key === "t") {
				await talking

				talking = talk.say(
					sentences[
						Math.round((sentences.length - 1) * Math.random())
					]
				)
			}
			if (e.key === "u") {
				talk.say(
					sentences[
						Math.round((sentences.length - 1) * Math.random())
					],
					{ isUser: true }
				)
			}
			if (e.key === "s") {
				userTyping = !userTyping
				talk.userTyping(userTyping)
			}
		})

		addEventListener("mousemove", e => {
			if (cliddy && cliddy instanceof Cliddy) {
				cliddy.lookAt = [e.pageX, e.pageY]
			}
		})
		addEventListener("touchstart", e => {
			var { pageX, pageY } = e.targetTouches[0]
			cliddy.lookAt = [pageX, pageY]
		})
		addEventListener("touchmove", e => {
			var { pageX, pageY } = e.targetTouches[0]
			cliddy.lookAt = [pageX, pageY]
		})
		addEventListener("touchend", () => {
			setTimeout(() => cliddy.face(">:("), 50)
		})
	})
})
