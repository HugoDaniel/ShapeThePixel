// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2
import shapeaux from "./shapeaux.mjs"
import { Model } from "./state/model.mjs"
import { WasmAPI } from "./state/wasm-api.mjs"
import { View } from "./view/view.mjs"

const { cuid } = External

globalThis.initView = (app, wasmApi, intents) => {
	globalThis.view = new View(app, wasmApi, intents)
	globalThis.view.init()
	globalThis.view.updateWorld()
	globalThis.view.render()
}

/**
 * This method creates the Wasm API with Shapeaux and places it at the
 * `globalThis.wasm` attribute.
 * It waits for the wasm file to be loaded and creates a WasmAPI object,
 * which sets high-level JS bindings to the wasm C functions.
 *
 * This function is used on the `index.html` to start the app.
 */
globalThis.initShapeaux = () => {
	// Shapeaux is a wasm C code that is loaded when this promise
	// resolves. The idea here is to initialize it with the Model
	// actions.
	return new Promise((resolve, reject) => {
		shapeaux().then(Module => {
			// Make the API available as a global var
			globalThis.wasm = new WasmAPI(Module)
			globalThis.wasm.initializationDone.then(() => {
				// Call the global `onWasmReady()` function if it is defined.
				if (
					globalThis.onWasmReady &&
					typeof globalThis.onWasmReady === "function"
				) {
					globalThis.onWasmReady()
				}
			})
			globalThis.wasm.initializationDone.then(resolve).catch(reject)
		})
	})
}

/**
 * This method creates the Model and places it at the `globalThis.model`
 * attribute.
 * If there is a window url hash code, the Model is initialized with it.
 * Otherwise a new window url hash code is created for the Model.
 *
 * This url hash code is the current room code. It can be used to send the url
 * to somebody on the internet to collaborate in the same project.
 * Read more about how it is used to fetch an existing project in the
 * `./state/model.mjs` file.
 *
 * This function is used on the `index.html` to start the app.
 */
globalThis.initModel = wasm => {
	// Get the room id from the URL if present, otherwise generate
	// a new room id and set it in the URL
	let id = undefined

	if (window.location.hash) {
		id = window.location.hash.split("#").pop()
	} else {
		id = cuid()
		window.location.hash = id
	}
	console.log("Room: ", id)

	// The Model keeps track of all the actions that have been applied
	// to the shapeaux state. The model makes use of the yjs CRDT and
	// indexeddb in order to make sure that the list of actions to
	// apply is always consistent between users. Each user runs an instance of
	// Shapeaux (wasm), which is their local source of truth.
	globalThis.model = new Model(id)
	globalThis.model.initializationDone.then(() => {
		// Call the global `onModelReady()` function if it is defined.
		if (
			globalThis.onModelReady &&
			typeof globalThis.onModelReady === "function"
		) {
			globalThis.onModelReady()
		}
	})
	// Returns the promise that resolves when the Model has finished
	// initializing
	return globalThis.model.initializationDone
}
