// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2
import { Containers } from "../deps/deno_bundle.js"

const { THREE } = External

class ElementEditor {
	constructor(size, vertices) {
		this.size = size
		// The guides and drawing grid
		this.guides = new THREE.Group()
		// The final produced shape:
		this.shape = new THREE.Group()
		this.createEditorDebug()
	}

	/** Creates an editor debug scene */
	createEditorDebug() {
		const geometry = new THREE.BoxGeometry(200, 400, 200)
		const material = new THREE.MeshBasicMaterial({ color: 0x221133 })

		const mesh = new THREE.Mesh(geometry, material)
		this.guides.add(mesh)
	}
}

/**
 * An element is made of a position, its editor and the material it shows.
 * The geometry is instanced from the parent (the Elements class).
 *
 * ?!?The Editor purpose is to produce a material to be used by this element?!
 *
 * - For now the editor produces a Three.Group that represents the shape to draw
 */
export class Element {
	position = new THREE.Vector2()
	editor = new ElementEditor()
	rotation = 0
	get visible() {
		return this._isEditorVisible
	}
	set visible(value) {
		if (value && !this._isEditorVisible) {
			this.showEditor()
		} else if (!value && this._isEditorVisible) {
			this.hideEditor()
		}
	}
	_isEditorVisible = false

	constructor(x, y, geometry, background, shape, editor) {
		this.position.x = x
		this.position.y = y
		this.geometry = geometry
		if (background) {
			this.background = background
		} else {
			// The background material
			this.background = new THREE.MeshBasicMaterial({
				color: 0xffeeaa,
				transparent: true,
				opacity: 1,
			})
		}
		if (editor) {
			this.editor = editor
		}
		this.mesh = new THREE.Mesh(this.geometry, this.background)
		this.root = new THREE.Group()
		this.root.name = `(${x}, ${y})`
		this.root.add(this.mesh)
		if (shape) {
			this.root.add(shape)
		}
		/*
		//this.root.add(this.animatedMesh)
		this.animatedMesh = new THREE.Mesh(
			this.geometry,
			this.animatedBackground
		)

				this.animatedBackground = this.background.clone()
		this.animatedBackground.transparent = true
		this.animatedBackground.opacity = 0.0

		*/
		// this.createAnimation()
	}

	createAnimation = () => {
		const opacityKF = new THREE.NumberKeyframeTrack(
			".material.opacity",
			[0, 1],
			[0, this.background.opacity]
		)

		const scaleKF = new THREE.VectorKeyframeTrack(
			".scale",
			[0, 1],
			[0.9, 0.9, 1, 1, 1, 1],
			THREE.InterpolateSmooth
		)

		const clip = new THREE.AnimationClip("Action", 2, [opacityKF, scaleKF])
		this.mixer = new THREE.AnimationMixer(this.animatedMesh)
		this.mixer.timeScale = 10
		const clipAction = this.mixer.clipAction(clip)
		clipAction.loop = THREE.LoopOnce
		clipAction.clampWhenFinished = true
		this.mixer.addEventListener("finished", () => {
			this.root.add(this.mesh)
			this.root.remove(this.animatedMesh)
			this.animatedBackground.dispose()
		})
		clipAction.play()
	}

	init = () => {
		// Add the editor guides and shape to this element root
		this.root.add(this.editor.shape)
	}

	hideEditor() {
		this._isEditorVisible = false
		this.root.remove(this.editor.guides)
	}

	showEditor() {
		this._isEditorVisible = true
		this.root.add(this.editor.guides)
	}

	rotate(r) {
		this.rotation = r
	}

	update = delta => {
		// Call update
		// this.mixer.update(delta)
	}
}

export class Elements {
	// This layer elements three js group
	root = new THREE.Group()
	// Keep a map of all x,y positions filled, for quick access
	elements = new Containers.Map2D()
	// Keep a set of all x,y positions, for quick validation and cloning
	// This is useful to know which elements were deleted from a new snapshot
	// Which happens by cloning this "positions" set and
	positions = new Containers.Set2D()
	// A THREE.Vector2 that keeps the x,y position for the current editor
	// being shown, if undefined then no editor is shown
	visibleEditor = undefined

	constructor(
		elementSize,
		sides,
		positionFromGridCoordinates = (x, y) => new THREE.Vector2()
	) {
		if (sides === 4) {
			this.elementGeometry = new THREE.PlaneGeometry(
				elementSize,
				elementSize
			)
			this.baseRotation = 0
		} else {
			this.elementGeometry = new THREE.CircleGeometry(
				elementSize / 2,
				sides
			)
		}

		this.positionFromGridCoordinates = positionFromGridCoordinates
		// this.addElement(0, 0)
	}

	addElement = element => {
		element.init()
		this.elements.set(element.position, element)
		this.positions.add(element.position)

		const elementWorldPosition = this.positionFromGridCoordinates(
			element.position.x,
			element.position.y
		)
		element.root.translateX(elementWorldPosition.x)
		element.root.translateY(elementWorldPosition.y)
		// Z defines the layer, -1000 is the bottom most layer
		// -1001 would stack on top of it, because growth is negative in z-axis
		element.root.translateZ(-1000)
		element.root.rotateZ(this.baseRotation)

		this.root.add(element.root)
		return element
	}

	/**
	 * Returns the element at the x y position provided by the elem argument
	 * x and y attributes.
	 *
	 * The "elem" argument must have both x and y attributes set to numbers
	 */
	getElement = elem => {
		return this.elements.get(elem)
	}
	/**
	 * Shows the element editor for the element at the argument position
	 * it hides any other visible element editor
	 **/
	showEditorAt(position) {
		this.hideEditor()
		this.visibleEditor = position
		this.elements.get(position)?.showEditor()
	}
	/** Hides the element editor, if visible */
	hideEditor() {
		if (this.visibleEditor) {
			this.elements.get(this.visibleEditor)?.hideEditor()
		}
		this.visibleEditor = undefined
	}

	/**
	 * Updates the element animation, with the delta time that has passed since
	 * the last update call
	 */
	update(delta) {
		for (const element of this.elements.values()) {
			element.update(delta)
		}
	}
}
