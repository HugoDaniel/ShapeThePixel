// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2

import { Elements } from "./grid-element.mjs"

const { THREE } = External

class SquareLines {
	panes = []
	root = new THREE.Group()
	constructor(elementsInPlaneSide = 16, elementSize = 16, color = 0xffaa00) {
		this.dimension = elementsInPlaneSide * elementSize
		this.linesGeometry = new THREE.BufferGeometry()
		this.linesMaterial = new THREE.LineBasicMaterial({ color })

		this.linesBuffer = new THREE.BufferAttribute(
			this.createLines(elementsInPlaneSide, elementSize),
			3
		)
		this.linesGeometry.setAttribute("position", this.linesBuffer)
	}

	/**
	 * Adds new planes of grid lines to make sure that the camera is filled
	 * with lines. This function receives the viewport and calls `addPane()`
	 * to add a pane of lines if needed. Panes are added whenever the camera
	 * moves/zooms in a way that makes the grid go into a position that
	 * is new (and needs to be rendered as if it was infinite)
	 */
	updateViewport(viewport) {
		// The needed viewport attributes:
		const w = viewport.w
		const h = viewport.h
		const x = viewport.originAt.x
		const y = viewport.originAt.y
		const zoom = viewport.camera.zoom

		// Update the panes:
		const horizontalPixels = w / zoom
		const verticalPixels = h / zoom
		const topLeftX = -horizontalPixels / 2 - x
		const topLeftY = -verticalPixels / 2 - y
		const startPane = this.paneCoordsForPos(topLeftX, topLeftY)
		const endPane = this.paneCoordsForPos(
			topLeftX + horizontalPixels,
			topLeftY + verticalPixels
		)
		for (
			let i = Math.min(endPane.x, startPane.x);
			i <= Math.max(endPane.x, startPane.x);
			i++
		) {
			for (
				let j = Math.min(endPane.y, startPane.y);
				j <= Math.max(endPane.y, startPane.y);
				j++
			) {
				this.addPane(i, j)
			}
		}
	}

	paneCoordsForPos(x, y) {
		const result = { x: 0, y: 0 }
		result.x = Math.floor(x / this.dimension)
		result.y = Math.floor(y / this.dimension)
		return result
	}

	addPane(x, y) {
		if (this.paneIndex(x, y) === -1) {
			const lines = new THREE.LineSegments(
				this.linesGeometry,
				this.linesMaterial
			)
			lines.translateX(x * this.dimension)
			lines.translateY(y * this.dimension)
			this.panes.push({ x, y, lines })
			this.root.add(lines)
		}
	}

	paneIndex(x, y) {
		return this.panes.findIndex(p => p.x === x && p.y === y)
	}

	createLines(elementsInPlaneSide, elementSize) {
		const result = new Float32Array(
			// 2 vertices per line, with 3 coordinates per vertex
			(1 + elementsInPlaneSide) * elementsInPlaneSide * 2 * 3
		)
		// Horizontal
		let offset = 2 * 3
		const z = -50
		for (let i = 0; i <= elementsInPlaneSide; i++) {
			result[offset * i + 0] = 0
			result[offset * i + 1] = i * elementSize
			result[offset * i + 2] = z
			result[offset * i + 3] = elementsInPlaneSide * elementSize
			result[offset * i + 4] = i * elementSize
			result[offset * i + 5] = z
		}
		// Vertical
		offset = (1 + elementsInPlaneSide) * 2 * 3
		for (let i = 0; i < elementsInPlaneSide; i++) {
			result[offset * i + 0] = i * elementSize
			result[offset * i + 1] = 0
			result[offset * i + 2] = z
			result[offset * i + 3] = i * elementSize
			result[offset * i + 4] = elementsInPlaneSide * elementSize
			result[offset * i + 5] = z
		}
		return result
	}
}

class Cursors {
	root = new THREE.Group()
	geometry = new THREE.BufferGeometry()
	cursors = []

	needsUpdate = false
	constructor(size = 32) {
		this.size = size
		this.margin = size / 32
		// Creates the square geometry in `this.geometry`:
		this.square(
			size / 2 / window.devicePixelRatio,
			size / 2 / window.devicePixelRatio,
			this.margin
		)
	}

	addCursor(id, color) {
		const cursorMaterial = new THREE.LineDashedMaterial({
			color,
			dashSize: this.margin,
			gapSize: this.margin,
		})
		const cursorOutline = new THREE.LineSegments(
			this.geometry,
			cursorMaterial
		)
		cursorOutline.computeLineDistances()
		this.root.add(cursorOutline)
		this.needsUpdate = true
		// Return the index of the current cursor
		return this.cursors.push({
			id,
			x: 0,
			y: 0,
			color,
			material: cursorMaterial,
			line: cursorOutline,
		})
	}

	hasCursor(id) {
		return this.cursors.findIndex(c => c.id === id) >= 0
	}

	updateCursorPosition(id, x, y) {
		const cursor = this.cursors.find(c => c.id === id)
		if (cursor) {
			cursor.x = x
			cursor.y = y
		}
		this.needsUpdate = true
	}

	update() {
		if (this.needsUpdate) {
			this.cursors.forEach(c => {
				// The `translateX/Y()` functions work with deltas. To work with
				// this, the object is moved to (0, 0), and then moved into the
				// desired position.
				c.line.translateY(-c.line.position.y + c.y)
				c.line.translateX(-c.line.position.x + c.x)
			})
			this.needsUpdate = false
		}
	}

	square(width, height, margin = 1) {
		const depth = 3
		const position = []
		const w = 2 * width - margin
		const h = 2 * height - margin
		position.push(
			margin,
			margin,
			-depth,
			margin,
			h,
			-depth,

			margin,
			h,
			-depth,
			w,
			h,
			-depth,

			w,
			h,
			-depth,
			w,
			margin,
			-depth,

			w,
			margin,
			-depth,
			margin,
			margin,
			-depth
		)

		this.geometry.setAttribute(
			"position",
			new THREE.BufferAttribute(new Float32Array(position), 3)
		)
	}
}

/**
 * A square grid layer.
 *
 * A layer is a threejs Group of objects that holds the users cursors being
 * shown, the grid lines, and the elements container (see `grid-element.js`).
 *
 * A grid needs to be initialized before usage, this is done by the `init()`
 * function, which is called by the application world Scene when a new square
 * grid layer is created.
 */
export class SquareGrid {
	get name() {
		return this.id // set at the constructor
	}
	root = new THREE.Group()
	/**
	 * True if the `init()` function was called, false otherwise
	 */
	isInitialized = false

	/** Total number of THREE nodes under this root */
	get count() {
		let total = 0
		this.root.traverse(() => {
			total += 1
		})
		return total
	}

	constructor(id, squareSize = 1024) {
		this.id = id
		this.elementSize = squareSize
		this.root.name = id
	}

	/**
	 * Initializes this layer.
	 *
	 * The layer initialization consists of creating the cursors container,
	 * the infinite grid lines, and the elements container. These are added
	 * to the layer `root` attribute (a `THREE.Group()`).
	 */
	init = viewport => {
		this.dprElementSize = this.elementSize / viewport.dpr
		// The cursors for the users:
		this.cursors = new Cursors(this.elementSize)
		// The infinite lines for this grid:
		this.squareLines = new SquareLines(16, this.elementSize / 2)
		this.squareLines.updateViewport(viewport)
		// Create the function to determine an element x,y pixel positioning
		// for this grid (gridX, gridY) coordinates:
		this.positionFromGridCoordinates = (gridX, gridY) => {
			// This is a square grid, the element is also a square and the
			// resulting position is the multiplication of the gridX,gridY
			// with the element size (adjusted for the device pixel ratio)
			return new THREE.Vector2(
				gridX * this.dprElementSize + this.dprElementSize / 2,
				gridY * this.dprElementSize + this.dprElementSize / 2
			)
		}
		// The elements container, created for a square grid:
		this.elements = new Elements(
			this.dprElementSize, // size adjusted for DPR
			4, // Number of sides
			this.positionFromGridCoordinates // grid coords to pixel coords
		)
		// Add the objects to the three scene graph
		this.root.add(this.elements.root)
		this.root.add(this.squareLines.root)
		this.root.add(this.cursors.root)
	}

	/**
	 * Hides the square grid lines and user cursors
	 */
	hideGuides() {
		this.squareLines.root.visible = false
		this.cursors.root.visible = false
	}
	/**
	 * Shows the square grid lines and user cursors. This is the default.
	 */
	showGuides() {
		this.squareLines.root.visible = true
		this.cursors.root.visible = true
	}

	/**
	 * Sets the user cursors to be rendered. This either creates the cursor if
	 * it is not currently being drawn, or updates the positions of the
	 * existing cursors.
	 */
	setCursors(cursors) {
		cursors.forEach(([cursorId, { position, color }]) => {
			if (this.cursors.hasCursor(cursorId) && position) {
				this.cursors.updateCursorPosition(
					cursorId,
					position.x * this.dprElementSize,
					position.y * this.dprElementSize
				)
			} else {
				this.cursors.addCursor(cursorId, color)
			}
		})
	}

	/**
	 * Returns the grid coordinates for the screen x,y position passed as
	 * argument.
	 *
	 * x,y are in screen coordinates, that is 0 < x < width, and 0 < y < height
	 */
	gridCoordsFor(x, y, viewport) {
		const z = viewport.prevZoom
		const xAtCenter = x - viewport.w / 2
		const xFromOrigin = xAtCenter - viewport.originAt.x * z
		const yAtCenter = y - viewport.h / 2
		const yFromOrigin = yAtCenter + viewport.originAt.y * z
		const screenSquareSize = this.dprElementSize * z
		const sq = new THREE.Vector2(
			xFromOrigin / screenSquareSize,
			-yFromOrigin / screenSquareSize
		)

		if (sq.x > 0) {
			sq.x = Math.ceil(sq.x) - 1
		} else {
			sq.x = Math.floor(sq.x)
		}
		if (sq.y > 0) {
			sq.y = Math.ceil(sq.y) - 1
		} else {
			sq.y = Math.floor(sq.y)
		}

		return sq
	}

	/** Returns the x,y position in scene coordinates for the given grid x y */
	positionFromGridCoordinates = (gridX, gridY) => {
		// defined in init() - here only for documentation purposes
	}

	elementEditorVisibilityRatio = 2
	/** Shows/Hides the editors for a given viewport position */
	updateEditors = viewport => {
		// Find the grid x,y position at the center of the viewport
		const pos = this.gridCoordsFor(viewport.w / 2, viewport.h / 2, viewport)
		// For a given zoom level hide/show the editor for that grid element
		const sqSize = this.dprElementSize * viewport.camera.zoom
		if (
			viewport.w / sqSize < this.elementEditorVisibilityRatio ||
			viewport.h / sqSize < this.elementEditorVisibilityRatio
		) {
			this.elements.showEditorAt(pos)
		} else {
			this.elements.hideEditor()
		}
	}

	/**
	 * Updates the layer to conform to new values for the scene, time and
	 * viewport.
	 */
	update({ scene, deltaTime, viewport, isViewportChanged }) {
		// Only update the grid position if the camera has changed
		if (isViewportChanged) {
			this.squareLines.updateViewport(viewport)
			// Update the visible editors
			// Editors can be shown or hidden according to the viewport
			// position/zoom level
			this.updateEditors(viewport)
		}
		this.elements.update(deltaTime)
		this.cursors.update()
	}
}
