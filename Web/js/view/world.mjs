// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2
import { Fills } from "./fills.mjs"
import { Shapes } from "./shapes.mjs"
import { SquareGrid } from "./square-grid.mjs"
import { Element } from "./grid-element.mjs"
import Assert from "../state/asserts.mjs"
import { Containers } from "../deps/deno_bundle.js"
const { THREE } = External

/**
 * The World class is responsible to perform full THREE scenegraph updates
 * using the pixels from the wasm (shapeaux.c) code.
 *
 * These full updates are done by keeping track of the elements from the
 * previous update, and checking which of those need to be removed, updated
 * or created.
 */
export class World {
	fills = new Map()
	styles = new Map()
	shapes = new Map()
	// TODO: editors = new Map()
	backgrounds = new Map()
	layers = new Map()

	// layers
	layerIds = new Map() // string -> number (pointer)
	shapeIds = new Map() // string -> number (pointer)
	fillIds = new Map() // string -> number (pointer)
	styleIds = new Map() // string -> number (pointer)

	constructor(root, viewport) {
		this.root = root
		this.viewport = viewport
	}
	/**
	 * Create the current layer and its THREE js scene root.
	 * This function is called by the view.mjs `init()` and it is expected
	 * to be called after `this.updateArrays()` have been called in order to
	 * have the layerIds Map() already filled in.
	 */
	init() {
		// Create the initial layer
		// When more layers are supported, this logic needs to be moved to
		// its own shapeaux iterator and layer update function
		this.layer0 = new SquareGrid(this.currentLayerId)
		this.layer0.init(this.viewport)
		this.root.add(this.layer0.root)
	}

	/**
	 * Updates all of the scene arrays items from the current state of
	 * Shapeaux webasm.
	 *
	 * This function updates all layer ids, shape ids, style ids and fill ids
	 */
	updateArrays(app, wasm) {
		this.layerIds.clear()
		this.shapeIds.clear()
		this.fillIds.clear()
		this.styleIds.clear()

		// Read all fills:
		for (let i = 0; i < wasm.fillsLength(app); i++) {
			const fill = wasm.getFillByIndex(app, i)
			const fillId = wasm.fillId(fill)
			this.fillIds.set(fillId, fill)
		}

		// Read all shapes:
		for (let i = 0; i < wasm.shapesLength(app); i++) {
			const shape = wasm.getShapeByIndex(app, i)
			const shapeId = wasm.shapeId(shape)
			this.shapeIds.set(shapeId, shape)
		}

		// Read all styles:
		for (let i = 0; i < wasm.stylesLength(app); i++) {
			const style = wasm.getStyleByIndex(app, i)
			const styleId = wasm.styleId(style)
			this.styleIds.set(styleId, style)
		}

		// Read all layers:
		for (let i = 0; i < wasm.layersLength(app); i++) {
			const layer = wasm.getLayerByIndex(app, i)
			const layerId = wasm.layerId(layer)
			this.layerIds.set(layerId, layer)
		}
	}

	get currentLayer() {
		return this.layer0
	}
	get currentLayerId() {
		return this.layerIds.keys().next().value
	}
	get currentStyleId() {
		return "PixelStyle1"
	}

	fromShapeaux(app, wasm) {
		// Traverse each pixel by resetting the shapeaux iterator
		wasm.iterator.start(app, 0)
		// Perform the pixel iteration, for each pixel property produce an
		// update object like this:
		const update = {
			id: undefined,
			wasm,
			diff: "DIFF_UNCHANGED",
			// The args to the update functions:
			prop: { shape: undefined, fill: undefined },
			// The results of the update functions (useful to pass to
			// the THREE objects to updateLayerPixel)
			result: { shape: undefined, background: undefined },
		}
		// The update object gets reused throughout the pixel iteration
		for (const pixel of wasm.iterator.pixels()) {
			// Check if there is a new shape that needs to be created
			update.id = pixel.shape.id
			update.diff = pixel.diffs.shape
			update.result.shape = this.updateShape(update)
			// Check if there is an update needed for the background
			update.id = pixel.shape.id
			update.diff = pixel.diffs.background
			update.prop.fill = pixel.background
			update.result.fill = this.updateFill(update)
			// TODO: Check if there is an update needed for the style
			// Update the layer pixel:
			this.updateLayerPixel(pixel, update.prop)
		}
		wasm.iterator.stop()
	}

	updateShape({ id, diff, wasm, prop }) {
		let shape = prop.shape
		switch (diff) {
			case "DIFF_IS_NEW":
				// Create a new shape with this id
				shape = new THREE.Group()
				this.shapes.set(id, shape)
				return shape

			default:
				// Do nothing
				return this.shapes.get(id)
		}
	}
	updateFill({ id, diff, wasm, prop }) {
		let fill = prop.fill
		if (!fill) {
			// Get fill from the wasm api
			const fillPtr = this.fillIds.get(id)
		}
		let material
		switch (diff) {
			case "DIFF_IS_NEW":
				material = new THREE.MeshBasicMaterial({
					color: new THREE.Color(
						fill.color.r,
						fill.color.g,
						fill.color.b
					),
					transparent: fill.color.a !== 255,
					opacity: fill.color.a / 255,
				})
				// Create a new shape with this id
				this.fills.set(id, material)
				break
			default:
				// Do nothing
				return this.fills.get(id)
		}
	}
	updateLayerPixel(pixel, props) {
		this.layer0.elements.addElement(
			new Element(
				pixel.x,
				pixel.y,
				this.layer0.elements.elementGeometry,
				props.background,
				props.shape
			)
		)
	}
}
