const { THREE } = External

const NO_SHAPE = 0
const PATHS_SHAPE = 1

export class Shapes {
	static updateShapes(shapesMap, newShapesList, changedSet) {
		let newShape, shape, isChanged
		for (let i = 0; i < newShapesList.length; i++) {
			newShape = newShapesList[i]
			shape = shapesMap.get(newShape.id)

			isChanged = false
			if (shape) {
				// Update the existing shape if its values have changed
				isChanged = false // TODO: change this to true if the shape was changed
			} else {
				// No shape was found, a new shape is created
				const createdShape = new THREE.Group()
				/*
                        const shapeGuides = new THREE.Group()
                        shapeGuides.name = "GuideLines"
                        createdShape.add(shapeGuides)
                        */
				shapesMap.set(newShape.id, createdShape)
				isChanged = true
			}
			if (isChanged) {
				changedSet.add(newShape.id)
			}
		}
	}
}
