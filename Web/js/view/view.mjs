// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2

import { World } from "./world.mjs"
import { SquareGrid } from "./square-grid.mjs"
import { MapControls } from "./controls.mjs"
import Assert from "../state/asserts.mjs"

const { THREE } = External

export class View {
	width = window.innerWidth
	height = window.innerHeight
	dpr = window.devicePixelRatio
	aspect = this.width / this.height

	root = new THREE.Scene()
	camera = new THREE.OrthographicCamera(
		this.width / -2,
		this.width / 2,
		this.height / 2,
		this.height / -2,
		1,
		1000
	)
	// Viewport is sent to the layers so that they can update the visibility
	// of their items (which editors are shown and the infinite grid planes
	// that are shown)
	viewport = {
		w: this.width,
		h: this.height,
		dpr: this.dpr,
		aspect: this.aspect,
		camera: this.camera,
		prevZoom: this.camera.zoom,
		originAt: new THREE.Vector4(),
		prevOriginAt: new THREE.Vector4(),
		deltaOriginAt: new THREE.Vector4(),
	}
	renderObjects = {
		viewport: this.viewport,
		scene: this.root,
		deltaTime: 0,
		isViewportChanged: true,
	}
	world = new World(this.root, this.viewport)
	myCursor = null // a Vector2 that holds the grid x,y coords for the local cursor

	constructor(
		app,
		wasmApi,
		intents,
		container = document.body,
		renderer = new THREE.WebGLRenderer({
			antialias: true,
		})
	) {
		// Read all info from Shapeaux (wasm api) into the World
		this.world.updateArrays(app, wasmApi)
		// Create the THREE renderer
		this.renderer = renderer
		this.renderer.setPixelRatio(this.dpr)
		this.renderer.setSize(this.width, this.height)

		// MapControls listens to touch/mouse events and updates the
		// `this.camera`, so that its matrix matches the current applied
		// pan and pinch/zoom actions.
		this.controls = new MapControls(this.camera, this.renderer.domElement)
		// It exposes a few constants that handle its sensitivity to rotation
		// and zoom
		this.controls.rotateSpeed = 1.0
		this.controls.zoomSpeed = this.isMobile ? 0.8 : 0.6
		this.controls.enableRotate = false
		// this.controls.panSpeed = 1.0

		// The current scene background color (a yellowish white)
		this.root.background = new THREE.Color(0xfafaf3)
		// Puts the renderer <canvas> in the supplied container (<body> by
		// default)
		container.appendChild(this.renderer.domElement)
		this.renderer.localClippingEnabled = true

		// These events are handled outside of the MapControls listener
		window.addEventListener("resize", this.onWindowResize)
		window.addEventListener("pointermove", this.onPointerMove)
		window.addEventListener("pointerdown", this.onPointerDown)
		this.intents = intents
		this.wasmApi = wasmApi
		this.app = app
	}

	dispose() {
		window.removeEventListener("resize", this.onWindowResize)
		window.removeEventListener("pointermove", this.onPointerMove)
		window.removeEventListener("pointerdown", this.onPointerDown)
		this.renderer.dispose()
	}

	init() {
		// this.squareGrid.init(this.root, this.camera)
		/*
		const sqSize = this.squareGrid.squareSize / this.dpr
		this.controls.pan(sqSize / 2, sqSize / 2)
		*/

		/** This is the limit at which Cliddy asks for grid options
		const limitGridOption = Math.max(
			this.width / sqSize,
			this.height / sqSize
		)
		 */

		/**
		 * To start by default start with a square fully zoomed in, occupying
		 * the whole screen, use this line in the controls.zoomIn instead:
		 * Math.min(sqSize / this.width, sqSize / this.height)
		 */
		this.controls.zoomIn(Math.PI)
		console.log("ZOOM", this.controls.object.zoom)
		// this.controls.onZoom(({ zoom }) => {})
		this.clock = new THREE.Clock()
		// Initialize the world
		this.world.updateArrays(this.app, this.wasmApi)
		this.world.init()
	}

	get isMobile() {
		// credit to Timothy Huang for this regex test:
		// https://dev.to/timhuang/a-simple-way-to-detect-if-browser-is-on-a-mobile-device-with-javascript-44j3
		if (
			/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
				navigator.userAgent
			)
		) {
			return true
		} else {
			return false
		}
	}

	onPointerMove = e => {
		if (!this.world.currentLayer) return
		if (this.myCursor) {
			const gridPosition = this.world.currentLayer.gridCoordsFor(
				e.clientX,
				e.clientY,
				this.viewport
			)
			if (
				gridPosition.x !== this.myCursor.x ||
				gridPosition.y !== this.myCursor.y
			) {
				gridPosition.z = 0 // TODO: add the layer
				// Trigger the cursor update

				this.intents.updateLocalCursor(gridPosition)
				this.myCursor = gridPosition
			}
		} else {
			const localCursor = this.intents.addLocalCursor()
			if (localCursor[1] && localCursor[1].position) {
				this.myCursor = localCursor[1].position
				Assert.isNumberVector2(this.myCursor)
			}
		}
	}

	onPointerDown = e => {
		// if (!this.ui.currentLayer || !this.ui.currentAppearance) return
		const at = this.world.currentLayer.gridCoordsFor(
			e.clientX,
			e.clientY,
			this.viewport
		)
		this.intents.paintLayerPixel(
			at.x,
			at.y,
			this.world.currentLayerId,
			this.world.currentStyleId,
			0 // Rotation
		)
	}

	onWindowResize = () => {
		this.width = window.innerWidth
		this.height = window.innerHeight
		this.aspect = this.width / this.height

		this.camera.left = this.width / -2
		this.camera.right = this.width / 2
		this.camera.top = this.height / 2
		this.camera.bottom = this.height / -2

		this.camera.updateProjectionMatrix()

		this.renderer.setSize(this.width, this.height)
		this.render()
	}

	/**
	 * Updates the cursors to be rendered.
	 */
	updateCursors = cursors => {
		this.world.currentLayer.setCursors(cursors, this.viewport)
	}

	/**
	 * Updates the world state. This is an async function, because
	 * updating the world can take longer than the frame boundary.
	 */
	updateWorld = async () => {
		/*
		if (actionState.errors.length > 0) {
			actionState.errors.forEach(e => console.error(`Wasm error: ${e}`))
		}
		this.world.update(actionState.current)
*/
		this.world.fromShapeaux(this.app, this.wasmApi)
	}

	/**
	 * Takes a camera and finds how much the origin (0,0) is distanced from
	 * the current center of the scene that the camera is pointing at.
	 *
	 * It calculates the delta for the previous point at the center of the
	 * camera. This delta is useful to know if the scene needs to be updated,
	 * if it is zero, then the camera did not move and a big part of the scene
	 * does not need to be updated (this is done in the update() function).
	 */
	updateViewport() {
		this.viewport.prevOriginAt.set(
			this.viewport.originAt.x,
			this.viewport.originAt.y,
			0,
			1
		)
		this.viewport.originAt.set(0, 0, 0, 1)
		this.viewport.originAt.applyMatrix4(this.camera.matrixWorldInverse)
		this.viewport.deltaOriginAt.set(
			this.viewport.originAt.x - this.viewport.prevOriginAt.x,
			this.viewport.originAt.y - this.viewport.prevOriginAt.y,
			0,
			1
		)
	}

	updateRenderObject() {
		this.renderObjects.deltaTime = this.clock.getDelta()
		this.renderObjects.isViewportChanged =
			this.viewport.deltaOriginAt.x !== 0 ||
			this.viewport.deltaOriginAt.y !== 0 ||
			this.viewport.prevZoom !== this.viewport.camera.zoom
		this.viewport.prevZoom = this.viewport.camera.zoom
	}

	render() {
		// Update the render objects
		this.updateViewport()
		this.updateRenderObject()
		// Update the layers
		if (this.world.currentLayer) {
			this.world.currentLayer.update(this.renderObjects)
		}
		this.renderer.render(this.root, this.camera)
	}

	startRenderLoop = () => {
		requestAnimationFrame(this.startRenderLoop)
		this.render()
	}
}
