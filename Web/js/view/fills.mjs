import Assert from "../state/asserts.mjs"
const { THREE } = External

export class Fills {
	static updateFills(fillsMap, newFillsList, changedSet) {
		let newFill, fillMaterial, isChanged

		for (let i = 0; i < newFillsList.length; i++) {
			newFill = newFillsList[i]
			fillMaterial = fillsMap.get(newFill.id)

			isChanged = false
			if (fillMaterial) {
				// Update the current fill (if needed)
				if (
					fillMaterial.color.r !== newFill.color.r ||
					fillMaterial.color.g !== newFill.color.g ||
					fillMaterial.color.b !== newFill.color.b
				) {
					fillMaterial.color.setRGB(
						newFill.color.r,
						newFill.color.g,
						newFill.color.b
					)
					isChanged = true
				}
				// Check if the opacity has changed and properly set the
				// transparent flag and opacity level of the current material
				if (fillMaterial.opacity !== newFill.color.a) {
					isChanged = true
					fillMaterial.opacity = newFill.color.a
					fillMaterial.transparent = newFill.color.a !== 1.0
				}
				fillMaterial.needsUpdate = isChanged
			} else {
				// Create a new fill
				fillsMap.set(
					newFill.id,
					new THREE.MeshBasicMaterial({
						color: new THREE.Color(
							newFill.color.r,
							newFill.color.g,
							newFill.color.b
						),
						transparent: newFill.color.a !== 1.0,
						opacity: newFill.color.a,
					})
				)
				isChanged = true
			}
			if (isChanged) {
				changedSet.add(newFill.id)
			}
		}
	}
}
