/**
 * Taken from: https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately/54024653#54024653
 * Convert HSV color to RGB
 **/
// input: h in [0,360] and s,v in [0,1] - output: r,g,b in [0,1]
function hsv2rgb(h, s, v) {
	let f = (n, k = (n + h / 60) % 6) =>
		v - v * s * Math.max(Math.min(k, 4 - k, 1), 0)
	return [f(5), f(3), f(1)]
}

/**
 * Taken from: https://stackoverflow.com/questions/8022885/rgb-to-hsv-color-in-javascript
 * Convert RGB color to HSV
 */
// input: r,g,b in [0,1], out: h in [0,360) and s,v in [0,1]
function rgb2hsv(r, g, b) {
	let v = Math.max(r, g, b),
		c = v - Math.min(r, g, b)
	let h =
		c && (v == r ? (g - b) / c : v == g ? 2 + (b - r) / c : 4 + (r - g) / c)
	return [60 * (h < 0 ? h + 6 : h), v && c / v, v]
}

/**
 * Returns a random color that keeps the same saturation and value as the
 * one provided.
 * @param {Number} r A color value between 0.0 and 1.0
 * @param {Number} g A color value between 0.0 and 1.0
 * @param {Number} b A color value between 0.0 and 1.0
 * @param {*} options Optional object that sets the limits for the variation
 * in S and V as well (0 by default - only Hue changes).
 * @returns an integer color (0xffffff)
 */
export function randomColor(color, options = { deltaS: 0, deltaV: 0 }) {
	const r = (color >> 16) / 255
	const g = ((color & 0x00ff00) >> 8) / 255
	const b = (color & 0x0000ff) / 255
	const hsv = rgb2hsv(r, g, b)
	const s = options.deltaS * Math.random() * (Math.random() > 0.5 ? -1 : 1)
	const v = options.deltaV * Math.random() * (Math.random() > 0.5 ? -1 : 1)
	let rgb = hsv2rgb(Math.round(Math.random() * 360), hsv[1] + s, hsv[2] + v)
	rgb = rgb.map(v => Math.round(v * 255))
	const result = (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]
	return result
}
