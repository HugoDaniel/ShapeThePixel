class Iterator {
	VALUES_PER_ITER = 4
	constructor(api, Module) {
		this.iter = api.createIterator()
		this.valueSize = api.sizeOfPixelIter()
		this.pxSize = api.sizeOfPixel()
		this.ptrSize = api.sizeOfPointer()
		this.fillTypeSize = api.sizeOfFillType()
		// Stores the iterator value
		this.values = new Array(this.VALUES_PER_ITER)
		this.byteValues = new Uint8Array(this.valueSize)
		this.valuesBuffer = Module._malloc(
			this.valueSize * this.VALUES_PER_ITER
		)
		this.api = api
		this.Module = Module
		this.getValue = this.Module.getValue
	}
	/**
	 * Checks if the malloc was properly done and resolves a Promise if so.
	 *
	 * @returns Promise that resolves when the initialization was successful
	 */
	initializationDone = new Promise((resolve, reject) => {
		if (this.valuesBuffer !== 0) {
			resolve()
		} else {
			reject("Unable to allocate memory for the Wasm Iterator")
		}
	})
	_get_str(ptr) {
		return this.Module.UTF8ToString(ptr, 64)
	}
	// Get the background fill_t values
	_fill_t(ptr) {
		const fillId = this._get_str(ptr)
		const color = this.api.fillColor(ptr)
		let a = color & 0x000000ff
		let b = (color >> 8) & 0x000000ff
		let g = (color >> 16) & 0x000000ff
		let r = (color >> 24) & 0x000000ff

		const typeValue = this.api.fillType(ptr)
		return {
			id: fillId,
			color: { r, g, b, a, int: color },
			typeValue,
		}
	}
	_style_t(ptr) {
		const shape_ptr = ptr + 64
		const shapeId = this._get_str(shape_ptr)
		return {
			shapeId,
		}
	}
	processPixelValues() {
		for (let i = 0; i < this.VALUES_PER_ITER; i++) {
			const px_ptr = this.api.px(this.iter, i)
			if (!px_ptr) {
				this.values[i] = null
			} else {
				// The pointer to the current iterator value struct
				const ptr = this.valuesBuffer + i * this.valueSize
				const style_ptr = this.getValue(ptr + this.ptrSize, "*")
				const style = this._style_t(style_ptr)
				const styleId = this._get_str(px_ptr + 4 + 4)
				const x = this.api.x(this.iter, i)
				const y = this.api.y(this.iter, i)
				const rot = this.api.rotation(this.iter, i)
				const bg_ptr = this.api.background(this.iter, i)
				this.values[i] = {
					x,
					y,
					rot,
					background: this._fill_t(bg_ptr),
					styleId,
					shape: { id: style.shapeId },
					diffs: {
						px: this.api.diff(this.api.diffPx(this.iter, i)),
						background: this.api.diff(
							this.api.diffBackground(this.iter, i)
						),
						style: this.api.diff(this.api.diffStyle(this.iter, i)),
						shape: this.api.diff(this.api.diffShape(this.iter, i)),
					},
				}
			}
		}
	}

	start(app, layerIndex) {
		this.api.startIterator(this.iter, app, layerIndex, this.valuesBuffer)
	}

	*pixels() {
		let iter = this.api.nextIterator(this.iter)
		while (iter !== 0) {
			this.processPixelValues()
			for (let i = 0; i < this.VALUES_PER_ITER; i++) {
				let px = this.values[i]
				if (px !== null) yield px
			}
			iter = this.api.nextIterator(this.iter)
		}
	}

	stop() {
		// Create a snapshot
		this.api.stopIterator(this.iter)
	}
}

/**
 * This class is the JS interface for the public C functions defined in
 * Shapeaux. The wasm API is created in the class constructor.
 *
 * A few utility functions are provided on top of the raw 1:1 wasm API set at
 * the constructor. These utility functions are defined as methods of this
 * class outside of the constructor, such methods include:
 *
 * - `createDeltasObserver()`, creates an observer function for the model deltas
 * - `applyActions()`, executes the corresponding C action for each item passed
 * in the argument array
 * - `updateState()`, updates the C state, by calling the `change_state()` wasm
 * function and keeping track of where the index of the
 * last action applied to it
 */
class WasmAPI {
	createDeltasObserver(app, callback) {
		if (!app) {
			console.error("Cannot create deltas observer function, app is null")
			return deltas => {
				console.error("Deltas unapplied because app is null", deltas)
			}
		}
		if (!callback || typeof callback !== "function") {
			console.error(
				"Cannot create deltas observer function. Callback is invalid. "
			)
			return deltas => {
				console.error(
					"Deltas unapplied because callback is not defined",
					deltas
				)
			}
		}
		return deltas => {
			this.deltasStart(app)
			for (let delta of deltas) {
				if (delta.retain) {
					this.deltaRetain(app, delta.retain)
				}
				if (delta.delete) {
					this.deltaDelete(app, delta.delete)
				}
				if (delta.insert) {
					for (let action of delta.insert) {
						this.act(app, action)
					}
				}
			}
			// Transforms the deltas into actions in the current working
			// project state (see shapeaux.c). These newly created actions then
			// need to be applied to the state with `change_state()` (wrapped
			// at `this.updateState()`)
			this.deltasApply(app)
			this.deltasStop(app)
			this.updateState(app)

			// Call the `callback`function passed as argument
			callback(app)
		}
	}
	applyActions(app, actions) {
		for (let action of actions) {
			this.act(app, action)
		}
		// Apply all the actions to the working project, this function calls
		// Shapeaux.changeState()
		this.updateState(app)
	}

	// A helper function on `changeState` that keeps track of the last
	// position updated in the action log
	_state_at = 0
	updateState(app) {
		this._state_at = this.changeState(app, this._state_at)
	}

	act(app, action) {
		switch (action.action) {
			case 1: // paintLayerPixel
				this.paintLayerPixel(
					app,
					// layer id string
					action.ids[0],
					// at.x
					action.vectors[0].x,
					// at.y
					action.vectors[0].y,
					// style id
					action.ids[1],
					// rotation
					action.values[0]
				)
				break
			default:
				// Do nothing, assume a NOP
				// Display a warning, NOPs should not happen at this stage
				console.warn("NOP", action)
				return
		}
	}
	constructor(Shapeaux) {
		this.createApp = Shapeaux.cwrap(
			"create_shape_the_pixel",
			"number",
			null
		)
		this.createProject = (app, name) => {
			if (app === 0 || !name) {
				console.error("Unable to create project with args:", app, name)
				return
			}
			Shapeaux.ccall(
				"create_project",
				null,
				["number", "string"],
				[app, name]
			)
			// Create an iterator:
			this.iterator = new Iterator(this, Shapeaux)
		}
		this.createProject2 = Shapeaux.cwrap("create_project", null, [
			"number",
			"string",
		])
		this.paintLayerPixel = Shapeaux.cwrap(
			"action_paint_layer_pixel",
			"number",
			[
				// app_t* const app
				"number",
				// char const * const layer_id
				"string",
				// int32_t const x
				"number",
				// int32_t const y
				"number",
				// char const * const style_id
				"string",
				// uint8_t const rotation
				"number",
			]
		)
		// number change_state(app_t* const app, size_t from);
		this.changeState = Shapeaux._change_state
		// void revert_state(app_t* const app, size_t to);
		this.revertState = Shapeaux.cwrap("revert_state", null, [
			"number",
			"number",
		])
		// void clear_error(app_t* const app);
		this.clearError = Shapeaux.cwrap("clear_error", null, ["number"])
		// pixel_iter_t* create_pixel_iter(void);
		this.createIterator = Shapeaux.cwrap(
			"create_pixel_iter",
			"number",
			null
		)
		// struct pixel_iter_t* pixel_iter_start(pixel_iter_t* const iter,
		// app_t* const app,
		// size_t layer_index);
		this.startIterator = Shapeaux.cwrap("pixel_iter_start", "number", [
			"number",
			"number",
			"number",
		])
		// struct pixel_iter_t* pixel_iter_next(pixel_iter_t* const iter);
		this.nextIterator = Shapeaux._pixel_iter_next
		// void pixel_iter_stop(pixel_iter_t* const iter);
		this.stopIterator = Shapeaux._pixel_iter_stop
		// uint32_t fill_color(fill_t const * const f);
		this.fillColor = Shapeaux._fill_color
		// fill_kind_t fill_type(fill_t const * const f);
		this.fillType = Shapeaux._fill_type
		// char const * fill_id(fill_t const * const f);
		this.fillId = Shapeaux.cwrap("fill_id", "string", ["number"])
		this.shapeId = Shapeaux.cwrap("shape_id", "string", ["number"])
		this.styleId = Shapeaux.cwrap("style_id", "string", ["number"])
		this.layerId = Shapeaux.cwrap("layer_id", "string", ["number"])
		// size_t fills_length(app_t const * const app);
		this.fillsLength = Shapeaux._fills_length
		// fill_t const * fill(app_t const * const app, size_t index);
		this.getFillByIndex = Shapeaux._fill
		// size_t shapes_length(app_t const * const app);
		this.shapesLength = Shapeaux._shapes_length
		// shape_t const * shape(app_t const * const app, size_t index);
		this.getShapeByIndex = Shapeaux._shape
		// size_t styles_length(app_t const * const app);
		this.stylesLength = Shapeaux._styles_length
		// style_t const * style(app_t const * const app, size_t index);
		this.getStyleByIndex = Shapeaux._style
		// size_t layers_length(app_t const * const app);
		this.layersLength = Shapeaux._layers_length
		// layer_t const * layer(app_t const * const app, size_t index);
		this.getLayerByIndex = Shapeaux._layer
		// fill_t const *
		// background(pixel_iter_t const * const iter);
		this.background = Shapeaux.cwrap("background", "number", ["number"])
		// int32_t x(pixel_iter_t const * const iter);
		this.x = Shapeaux._x // Shapeaux.cwrap("x", "number", ["number"]),
		// int32_t y(pixel_iter_t const * const iter);
		this.y = Shapeaux._y // Shapeaux.cwrap("y", "number", ["number"]),
		// uint8_t rotation(pixel_iter_t const * const iter);
		this.rotation = Shapeaux._rotation // Shapeaux.cwrap("rotation", "number", ["number"]),
		this.px = Shapeaux._px
		// diff_kind_t diff_px(pixel_iter_t const * const iter, size_t const pos);
		this.diffPx = Shapeaux._diff_px
		// diff_kind_t diff_background(pixel_iter_t const * const iter, size_t const pos);
		this.diffBackground = Shapeaux._diff_background
		// diff_kind_t diff_style(pixel_iter_t const * const iter, size_t const pos);
		this.diffStyle = Shapeaux._diff_style
		// diff_kind_t diff_shape(pixel_iter_t const * const iter, size_t const pos);
		this.diffShape = Shapeaux._diff_shape
		this.diff = enumCode => {
			/*
		typedef enum diff_kind_t {
    		DIFF_IS_NEW,
    		DIFF_UNCHANGED,
    		DIFF_HAS_CHANGED,
    		DIFF_WAS_REMOVED
		} diff_kind_t;
		*/
			switch (enumCode) {
				case 1:
					return "DIFF_UNCHANGED"
				case 2:
					return "DIFF_HAS_CHANGED"
				case 3:
					return "DIFF_WAS_REMOVED"
				default:
					return "DIFF_IS_NEW"
			}
		}

		// `sizeof` functions, useful to determine memory pointer offsets
		this.sizeOfPixelIter = Shapeaux._pixel_iter_value_size
		this.sizeOfPixel = Shapeaux._pixel_size
		this.sizeOfPointer = Shapeaux._pointer_size
		this.sizeOfFillType = Shapeaux._fill_type_size

		// Delta action functions
		// void deltas_start(app_t* const app);
		this.deltasStart = Shapeaux._deltas_start
		// void deltas_stop(app_t* const app);
		this.deltasStop = Shapeaux._deltas_stop
		// void deltas_apply(app_t* const app);
		this.deltasApply = Shapeaux._deltas_apply
		//void delta_retain(app_t* const app, size_t amount);
		this.deltaRetain = Shapeaux._delta_retain
		// void delta_delete(app_t* const app, size_t amount);
		this.deltaDelete = Shapeaux._delta_delete

		this.iterator = new Iterator(this, Shapeaux)
		this.initializationDone = new Promise((resolve, reject) => {
			this.iterator.initializationDone.then(() => {
				resolve(this)
			})
		})
	}
}

export { WasmAPI }
