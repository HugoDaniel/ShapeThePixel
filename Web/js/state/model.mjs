// Copyright © 2021 by Hugo Daniel Henriques Oliveira Gomes. All rights reserved.
// Licensed under the EUPL v1.2
import { randomColor } from "./color-helpers.mjs"
import Assert from "./asserts.mjs"
const { Y, WebrtcProvider, IndexeddbPersistence, cuid } = External
const { iceServer, iceCredential, iceUsername, signalingServer } = Environment

/**
 * Model holds all the documents available and keeps track of the list of
 * actions for the current document being edited.
 *
 * Documents are just a list of actions.
 **/
export class Model {
	/** The set of available documents to work on */
	docs = []
	/**
	 * The current document id - useful to keep track of it in indexedDB and
	 * WebRTC sessions. In the browser this document id is set in the url hash.
	 */
	id
	/**
	 * The document being worked on, this is where all the actions are going
	 * to be applied to.
	 */
	doc = new Y.Doc()
	actions = this.doc.getArray("actions")
	/**
	 * The IndexedDBProvider is used to keep a local copy of the current model
	 * document.
	 **/
	indexeddbProvider = undefined

	constructor(id, { useProvider = true, useIndexedDB = true } = {}) {
		if (!id) {
			this.id = cuid()
		} else {
			this.id = id
		}
		localStorage.log = null
		// localStorage.log = "true"
		if (useProvider) {
			this.provider = new WebrtcProvider(this.id, this.doc, {
				maxConns: 5 + Math.floor(Math.random() * 10),
				signaling: [signalingServer],
				// PeerJS options:
				peerOpts: {
					config: {
						iceServers: [
							{
								urls: iceServer,
								credential: iceCredential,
								username: iceUsername,
							},
						],
						sdpSemantics: "unified-plan",
					},
				},
				debug: 1, // Show errors if they happen
			})
		}

		this.initializationDone = new Promise((resolve, _) => {
			const resolvedResult = {
				actions: [],
				roomId: this.id,
			}
			if (useIndexedDB) {
				this.initIndexedDB().then(() => {
					resolvedResult.actions = this.actions.toArray()
					resolve(resolvedResult)
				})
			} else {
				resolve(resolvedResult)
			}
		})
	}

	get awareness() {
		if (this.provider) {
			return this.provider.awareness
		} else {
			// Provide a simple awareness object if no "provider" is set
			return {
				localState: new Map(),
				callback: () => {},
				setLocalState(c) {
					this.localState.set(cuid(), c)
					this.callback([])
				},
				setLocalStateField(field, value) {
					const key = this.localState.keys().next().value
					const localCursor = this.localState.get(key)
					if (localCursor) {
						localCursor[field] = value
					}
				},
				getStates() {
					return this.localState
				},
				on(_, callback) {
					this.callback = callback
				},
			}
		}
	}

	intents = {
		addLocalCursor: () => {
			const localCursor = {
				color: randomColor(0x30bced, { deltaS: 0.1, deltaV: 0 }),
				position: { x: 0, y: 0, z: 0 },
			}
			this.awareness.setLocalState(localCursor)
			const cursorId = Array.from(this.awareness.getStates().keys()).pop()
			return [cursorId, localCursor]
		},
		updateLocalCursor: position => {
			// Make sure the "position" arg has the required x,y,z attributes
			// Assert.isNumberVector3(position)
			this.awareness.setLocalStateField("position", {
				x: position.x,
				y: position.y,
				z: position.z,
			})
		},
		paintLayerPixel: (x, y, layerId, styleId, rotation) => {
			if (!Number.isInteger(x) || !Number.isInteger(y)) {
				const error = `Cannot paintPixel at position (${x}, ${y}),\
				painting grid elements can only happen in integer positions`
				console.error(error)
				return error
			}
			// From shapeaux `action_kind_t`
			const actionId = 1 // 0 is NOP
			this.actions.insert(this.actions.length, [
				{
					action: actionId,
					vectors: [{ x, y }],
					ids: [layerId, styleId],
					values: [rotation],
				},
			])
		},
	}

	onInit = f => {
		this.initializationDone.then(args => {
			f(this.actions.toArray())
		})
	}

	/**
	 * Places the current model document in the IndexedDB persistent browser
	 * storage. This is called by the 'shape-the-pixel.js' file when the document
	 * is loaded.
	 **/
	initIndexedDB() {
		// this instantly gets the (cached) documents data
		this.indexeddbProvider = new IndexeddbPersistence(this.id, this.doc)
		return this.indexeddbProvider.whenSynced
	}

	/**
	 * Set a function to be called whenever a change happens in the actions
	 * array. The function passed as argument is called with the deltas array
	 * of the changes. For more info on deltas format see:
	 * https://docs.yjs.dev/api/delta-format
	 */
	onObserve = f => {
		this.actions.observe(e => {
			f(e.changes.delta)
		})
	}

	onAwarenessChange = f => {
		// You can observe when a any user updated their awareness information
		this.awareness.on("update", changes => {
			f(Array.from(this.awareness.getStates().entries()), changes)
		})
	}
}
