import { Model } from "../state/model.js"
import { View } from "../view/shape-the-pixel-view.js"

const delta = [
	{
		insert: [
			{
				action: 1,
				vectors: [{ x: 0, y: 0 }],
				ids: ["Layer0", "Item0"],
				values: [0],
			},
		],
	},
]

const simpleModel = () => {
	return new Model("Test - initialization", {
		useProvider: false,
		useIndexedDB: false,
	})
}

const simpleView = model => {
	return new View(model.intents, document.createElement("div"))
}

const swiftStart = globalThis.startSwiftTest // a helper from test.html
describe("Initialization - `swift()` function", function () {
	it("should have a global `swift()` function defined", function () {
		assert.equal(typeof globalThis.swift, "function")
	})
	it("should return an error if no arguments are present", function () {
		const result = globalThis.swift()
		assert.equal(
			result,
			"Swift.main(): must be called with at least one argument, none was found"
		)
	})
	it("should return an error if argument does not have a '.model' attribute", function () {
		const result = globalThis.swift({
			models: true,
			updateState: () => {},
			updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): argument must be an object with a "model" attribute, but it is undefined.`
		)
	})
	it("should return an error if model does not have a '.onInit' function", function () {
		const result = globalThis.swift({
			model: {
				// onInit: () => {},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): "onInit()" function was not found in the "model" attribute of the provided argument object.`
		)
	})
	it("should return an error if model does not have a '.observe' function", function () {
		const result = globalThis.swift({
			model: {
				onInit: () => {},
				// observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): "observe()" function was not found in the "model" attribute of the provided argument object.`
		)
	})
	it("should return an error if model does not have a '.onAwarenessChange' function", function () {
		const result = globalThis.swift({
			model: {
				onInit: () => {},
				observe: () => {},
				// onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): "onAwarenessChange()" function was not found in the "model" attribute of the provided argument object.`
		)
	})
	it("should return an error if the argument object does not have a '.updateState()' function", function () {
		const result = globalThis.swift({
			model: {
				onInit: () => {},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			// updateState: () => {},
			updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): "updateState()" function was not found in the argument provided object.`
		)
	})
	it("should return an error if the argument object does not have a '.updateCursors()' function", function () {
		const result = globalThis.swift({
			model: {
				onInit: () => {},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			// updateCursors: () => {},
		})
		assert.equal(
			result,
			`Swift.main(): "updateCursors()" function was not found in the argument provided object.`
		)
	})
	it("should return undefined everything is ok", function () {
		const result = globalThis.swift({
			model: {
				onInit: () => {},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
		assert.isUndefined(result)
	})
	it("should call the model.onInit() function when it is executed", function (done) {
		globalThis.swift({
			model: {
				onInit: () => {
					assert.ok(true)
					done()
				},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
	})
	it("should provide a function to the model.onInit() function when it is executed", function (done) {
		globalThis.swift({
			model: {
				onInit: f => {
					assert.equal(typeof f, "function")
					done()
				},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: () => {},
			updateCursors: () => {},
		})
	})
	it("should call 'updateState()' after JS triggers the Swift initialization in 'onInit()'", function (done) {
		globalThis.swift({
			model: {
				onInit: f => {
					f([])
				},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				assert.ok(true)
				done()
			},
			updateCursors: () => {},
		})
	})
	it("should provide an error in the state sent with 'updateState()' if there are no actions to initialize", function (done) {
		globalThis.swift({
			model: {
				onInit: f => {
					f(undefined)
				},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				assert.isArray(newState.errors, ".errors is an array")
				assert.lengthOf(newState.errors, 1, ".errors has length of 1")
				assert.include(newState.errors[0], "no actions array found")
				done()
			},
			updateCursors: () => {},
		})
	})
	it("should provide an error in the state sent with 'updateState()' if there are is an invalid action provided", function (done) {
		globalThis.swift({
			model: {
				onInit: f => {
					f([{ thisIsInvalid: true }])
				},
				observe: () => {},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				assert.isArray(newState.errors, ".errors is an array")
				assert.lengthOf(newState.errors, 1, ".errors has length of 1")
				assert.include(
					newState.errors[0],
					"Error applying initialization actions"
				)
				done()
			},
			updateCursors: () => {},
		})
	})
	it("should call updateState() when an action is observed", function (done) {
		let isInitialized = false
		globalThis.swift({
			model: {
				onInit: f => {
					f([])
				},
				observe: observeAction => {
					assert.equal(typeof observeAction, "function")
					// Send a delta:
					observeAction(delta)
				},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				// The first time this is called is during initialization,
				// The second time is after the observeAction is called
				if (isInitialized) {
					assert.ok(true)
					done()
				} else {
					isInitialized = true
				}
			},
			updateCursors: () => {},
		})
	})
	it("should update the state with the delta passed by the observe function", function (done) {
		let isInitialized = false
		globalThis.swift({
			model: {
				onInit: f => {
					f([])
				},
				observe: observeAction => {
					// Send a delta:
					observeAction(delta)
				},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				// The first time this is called is during initialization,
				// The second time is after the observeAction is called
				if (isInitialized) {
					assert.lengthOf(newState.log, 1, ".log has length of 1")
					done()
				} else {
					assert.isArray(newState.log)
					assert.lengthOf(newState.log, 0, ".log has length of 0")
					isInitialized = true
				}
			},
			updateCursors: () => {},
		})
	})
	it("should produce an error when an invalid delta is passed to the observe function", function (done) {
		let isInitialized = false
		globalThis.swift({
			model: {
				onInit: f => {
					f([])
				},
				observe: observeAction => {
					// Send a delta:
					observeAction([{ thisIsInvalid: true }])
				},
				onAwarenessChange: () => {},
			},
			updateState: newState => {
				// The first time this is called is during initialization,
				// The second time is after the observeAction is called
				if (isInitialized) {
					console.log(newState.errors)
					assert.lengthOf(
						newState.errors,
						1,
						".errors has length of 1"
					)
					assert.include(newState.errors[0], "Unknown delta")
					done()
				} else {
					assert.lengthOf(
						newState.errors,
						0,
						".errors has length of 0"
					)
					isInitialized = true
				}
			},
			updateCursors: () => {},
		})
	})

	//////////////
	/// MODEL ////
	//////////////
	it("should have a default layer in initial state of the Model", function (done) {
		const model = simpleModel()
		swiftStart({ model }, newState => {
			assert.isArray(newState.current.layers)
			assert.lengthOf(newState.current.layers, 1)
			const layer = newState.current.layers[0]
			assert.equal(layer.id, "Layer0")
			assert.equal(layer.type, 0)
			assert.isArray(layer.elements)
			assert.lengthOf(layer.elements, 0)
			done()
		})
	})
	it("should have a default shape in initial state of the Model", function (done) {
		const model = simpleModel()
		swiftStart({ model }, newState => {
			assert.isArray(newState.current.shapes)
			assert.lengthOf(newState.current.shapes, 1)
			const shape = newState.current.shapes[0]
			assert.equal(shape.id, "Shape0")
			assert.equal(shape.type, 0)
			done()
		})
	})
	it("should have a default appearance in initial state of the Model", function (done) {
		const model = simpleModel()
		swiftStart({ model }, newState => {
			assert.isArray(newState.current.appearances)
			assert.lengthOf(newState.current.appearances, 1)
			const appearance = newState.current.appearances[0]
			assert.equal(appearance.id, "Item0")
			assert.equal(appearance.background, "Fill0")
			assert.equal(appearance.shape, "Shape0")
			assert.isArray(appearance.appearance)
			assert.lengthOf(appearance.appearance, 0)
			done()
		})
	})
	it("should have default fills in initial state of the Model", function (done) {
		const model = simpleModel()
		swiftStart({ model }, newState => {
			assert.isArray(newState.current.fills)
			assert.lengthOf(newState.current.fills, 2)
			const fill0 = newState.current.fills[0]
			assert.equal(fill0.id, "TransparentFill")
			assert.equal(fill0.type, 0)
			assert.equal(fill0.color.a, 0)
			const fill1 = newState.current.fills[1]
			assert.equal(fill1.id, "Fill0")
			assert.equal(fill1.type, 0)
			assert.isNumber(fill1.color.r)
			assert.isNumber(fill1.color.g)
			assert.isNumber(fill1.color.b)
			done()
		})
	})
	it("should produce an initial valid view world state", function (done) {
		const model = simpleModel()
		const view = simpleView(model)
		swiftStart({ model }, newState => {
			view.updateState(newState)
			assert.equal(view.world.fills.size, newState.current.fills.length)
			assert.equal(
				view.world.appearances.size,
				newState.current.appearances.length
			)
			assert.equal(
				view.world.backgrounds.size,
				newState.current.appearances.length
			)
			assert.equal(view.world.layers.size, newState.current.layers.length)
			assert.equal(view.world.shapes.size, newState.current.shapes.length)
			// Layer should be empty
			assert.equal(view.currentLayer.elements.elements.size, 0)
			view.dispose()
			done()
		})
	})
})
