describe("Cliddy", function () {
	it("should be registered in the customElements registry", function () {
		assert.equal(customElements.get("cliddy-friend"), Cliddy)
	})
	it("should be positioned at the bottom right by default", function () {
		const cliddy = new Cliddy()
		cliddy.init()
		const element = cliddy.shadowRoot.querySelector(".cliddy")
		const style = element.style
		assert.equal(style.position, "absolute")
		assert.equal(style.bottom, "0px")
		assert.equal(style.right, "0px")
	})
	it("should adjust position according to the provided width and height", function () {
		const cliddy = new Cliddy()
		cliddy.init()
		cliddy.positionFor(4000, 4000)
		const element = cliddy.shadowRoot.querySelector(".cliddy")
		const style = element.style
		// Adjustment happens with negative values, deltas to Cliddy at the
		// bottom right of the screen
		assert.equal(style.right[0], "-")
		assert.equal(style.bottom[0], "-")
	})
	it("should have a 'init' function in talk component", function () {
		const talk = new CanTalk()
		assert.isFunction(talk.init)
	})
	it("should have a 'say' function in talk component", function () {
		const talk = new CanTalk()
		assert.isFunction(talk.say)
	})
	it("should have a 'userTyping' function", function () {
		const talk = new CanTalk()
		assert.isFunction(talk.userTyping)
	})
	it("should create the current sentence element when 'init' is called", function () {
		const talk = new CanTalk()
		assert.isUndefined(talk.talkElement)
		assert.isUndefined(talk.currentSentence)
		talk.init()
		assert.instanceOf(talk.talkElement, HTMLElement)
		assert.instanceOf(talk.currentSentence, HTMLElement)
	})
	it("should return a promise with the HTML element in 'say' function", function (done) {
		const talk = new CanTalk()
		talk.init()
		// Test a simple string (<=2 characters), to avoid having to wait for
		// the "animationend" event in say() (which is when the say() function
		// calls the promise "resolve()")
		talk.say("no").then(element => {
			assert.instanceOf(element, HTMLElement)
			done()
		})
	})
})
