describe("Shapeaux", function () {
	it("should have a global wasm api object", function () {
		assert.equal(typeof wasm, "object")
	})
	it("should have a `createApp` function", function () {
		assert.equal(typeof wasm.createApp, "function")
	})
	it("should have a `createProject` function", function () {
		assert.equal(typeof wasm.createProject, "function")
	})
	it("should have a `paintLayerPixel` function", function () {
		assert.equal(typeof wasm.paintLayerPixel, "function")
	})
	it("should have a `deltasStart` function", function () {
		assert.equal(typeof wasm.deltasStart, "function")
	})
	it("should have a `deltasStop` function", function () {
		assert.equal(typeof wasm.deltasStop, "function")
	})
	it("should have a `deltasApply` function", function () {
		assert.equal(typeof wasm.deltasApply, "function")
	})
	it("should have a `deltaRetain` function", function () {
		assert.equal(typeof wasm.deltaRetain, "function")
	})
	it("should have a `deltaDelete` function", function () {
		assert.equal(typeof wasm.deltaDelete, "function")
	})
	it("should have a `changeState` function", function () {
		assert.equal(typeof wasm.changeState, "function")
	})
	it("should have a `revertState` function", function () {
		assert.equal(typeof wasm.revertState, "function")
	})
	it("should have a `clearError` function", function () {
		assert.equal(typeof wasm.clearError, "function")
	})
	it("should have a `createIterator` function", function () {
		assert.equal(typeof wasm.createIterator, "function")
	})
	it("should have a `startIterator` function", function () {
		assert.equal(typeof wasm.startIterator, "function")
	})
	it("should have a `nextIterator` function", function () {
		assert.equal(typeof wasm.nextIterator, "function")
	})
	it("should have a `fillColor` function", function () {
		assert.equal(typeof wasm.fillColor, "function")
	})
	it("should have a `fillType` function", function () {
		assert.equal(typeof wasm.fillType, "function")
	})
	it("should have a `fillId` function", function () {
		assert.equal(typeof wasm.fillId, "function")
	})
	it("should have a `background` function", function () {
		assert.equal(typeof wasm.background, "function")
	})
	it("should have a `x` function", function () {
		assert.equal(typeof wasm.x, "function")
	})
	it("should have a `y` function", function () {
		assert.equal(typeof wasm.y, "function")
	})
	it("should have a `rotation` function", function () {
		assert.equal(typeof wasm.rotation, "function")
	})
	it("should have a `diffPx` function", function () {
		assert.equal(typeof wasm.diffPx, "function")
	})
	it("should have a `diffBackground` function", function () {
		assert.equal(typeof wasm.diffBackground, "function")
	})
	it("should have a `diffStyle` function", function () {
		assert.equal(typeof wasm.diffStyle, "function")
	})
	it("should have a `diffShape` function", function () {
		assert.equal(typeof wasm.diffShape, "function")
	})
	it("should be able to create an app pointer", function () {
		const app = wasm.createApp()
		assert.equal(typeof app, "number")
		assert.notEqual(app, 0)
	})
	it("should be able to paint a pixel in a project", function () {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")
		// Place the paint pixel action in the project:
		wasm.paintLayerPixel(app, "Layer0", 123, 321, "PixelStyle1", 10)
		// Apply all the actions from the first one:
		wasm.changeState(app, 0)
		// Create the iterator that is going to be used to
		// read the pixel
		let iter = wasm.createIterator()
		// Make it point to the start
		wasm.startIterator(iter, app, 0)
		// Move the iterator to the first non-empty pixel
		iter = wasm.nextIterator(iter)
		// Check that the x is the same as the pixel
		assert.equal(wasm.x(iter), 123)
		// Check that the y is the same as the pixel
		assert.equal(wasm.y(iter), 321)
		// Check that the rotation is the same as the pixel
		assert.equal(wasm.rotation(iter), 10)
	})
	it("should be able to start a pixel iterator", function () {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")
		// Place the paint pixel action in the project:
		wasm.paintLayerPixel(app, "Layer0", 123, 321, "PixelStyle1", 10)
		// Apply all the actions from the first one:
		wasm.changeState(app, 0)
		// Create the iterator that is going to be used to
		// read the pixel
		wasm.iterator.start(app, 0)
		let iterations = 0
		for (let pixel of wasm.iterator.pixels()) {
			assert.equal(pixel.x, 123)
			assert.equal(pixel.y, 321)
			assert.equal(pixel.styleId, "PixelStyle1")
			assert.equal(pixel.rot, 10)
			assert.equal(pixel.shape.id, "Shape1")
			assert.equal(pixel.background.id, "Fill1")
			assert.equal(pixel.background.typeValue, 0)
			assert.equal(pixel.background.color.r, 128)
			assert.equal(pixel.background.color.g, 188)
			assert.equal(pixel.background.color.b, 163)
			assert.equal(pixel.background.color.a, 255)
			iterations++
			// This is a failsafe, to allow the test to
			// stop in case the iterator loops forever
			if (iterations === 10) break
		}
		// Make sure that only one iteration happened
		// There should be only one pixel to process
		assert.equal(iterations, 1)
	})
	it("should be able to paint many pixels in a project", async function (done) {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")
		const iterations = 1080
		console.time(`Write ${iterations} pixel actions in a project`)
		for (let i = 0; i < iterations; i++) {
			// Place the paint pixel action in the project:
			wasm.paintLayerPixel(app, "Layer0", i, -i, "PixelStyle1", 0)
		}
		console.timeEnd(`Write ${iterations} pixel actions in a project`)

		console.time(`changeState(0 -> ${iterations})`)
		// Apply all the actions from the first one:
		wasm.changeState(app, 0)
		console.timeEnd(`changeState(0 -> ${iterations})`)
		// Create the iterator that is going to be used to
		// read the pixel
		wasm.iterator.start(app, 0)
		console.time(`read ${iterations} pixels in a project`)
		let coordsSum = 0
		let i = 0
		for (let pixel of wasm.iterator.pixels()) {
			coordsSum += pixel.x + pixel.y
			i++
		}
		console.timeEnd(`read ${iterations} pixels in a project`)

		assert.isTrue(
			coordsSum === 0,
			`sum must be 0, it was ${coordsSum} instead`
		)
		assert.isTrue(i === iterations, "number of iterations do not match")
		done()
	})
	it("should be able to diff pixels in a project", function () {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")
		const iterations = 16

		for (let i = 0; i < iterations; i++) {
			// Place the paint pixel action in the project:
			wasm.paintLayerPixel(app, "Layer0", i, -i, "PixelStyle1", 0)
		}

		// Apply all the actions from the first one:
		const projectAt = wasm.changeState(app, 0)
		// Create the iterator that is going to be used to
		// read the pixels
		wasm.iterator.start(app, 0)
		let i = 0
		for (let pixel of wasm.iterator.pixels()) {
			assert.isTrue(pixel.x === i)
			assert.isTrue(pixel.y === -i)
			i++
			// First run of the iterator should always produce new pixels
			assert.isTrue(wasm.diff(pixel.diffs.px) === "DIFF_IS_NEW")
		}
		wasm.iterator.stop()

		wasm.iterator.start(app, 0)
		// 2nd run should produce UNCHANGED pixels
		i = 0
		for (let pixel of wasm.iterator.pixels()) {
			assert.isTrue(pixel.x === i, "2nd run pixel.x")
			assert.isTrue(pixel.y === -i, "2nd run pixel.y")
			i++
			// Second run of the iterator the diff must be done against the
			// state of the previous run:
			assert.isTrue(
				pixel.diffs.px === "DIFF_UNCHANGED",
				"DIFF: 2nd run pixel.diff.px"
			)
		}
		wasm.iterator.stop()

		// 3rd run, add more pixels to the project, these new pixels
		// should have a diff of NEW

		wasm.paintLayerPixel(app, "Layer0", 50, -50, "PixelStyle1", 0)
		wasm.changeState(app, projectAt)
		wasm.iterator.start(app, 0)
		for (let pixel of wasm.iterator.pixels()) {
			if (pixel.x === 50) {
				assert.isTrue(
					pixel.diffs.px === "DIFF_IS_NEW",
					"DIFF: 3rd run NEW pixel.diff.px"
				)
			} else {
				assert.isTrue(
					pixel.diffs.px === "DIFF_UNCHANGED",
					"DIFF: 3rd run OLD pixel.diff.px"
				)
			}
		}
	})

	it("should be able to insert delta actions", async function (done) {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")

		console.time(`Delta Start`)
		// Start the deltas
		wasm.deltasStart(app)
		// Actions should be considered delta inserts
		wasm.paintLayerPixel(app, "Layer0", 1024, -1024, "PixelStyle1", 128)
		// Transform the deltas into project actions
		wasm.deltasApply(app)

		// Start the deltas again...
		wasm.deltasStart(app)
		// Actions should be inserted at position 0 (unless a retain is used)
		wasm.paintLayerPixel(app, "Layer0", 555, -555, "PixelStyle1", 55)
		wasm.deltasApply(app)
		console.timeEnd(`Delta Start`)

		console.time(`Delta -> changeState`)
		// Check that the deltas were applied
		wasm.changeState(app, 0) // Apply the actions
		console.timeEnd(`Delta -> changeState`)
		// Create the iterator that is going to be used to
		// read the pixels
		console.time(`Delta -> iterator`)
		wasm.iterator.start(app, 0)
		let iterations = 0
		for (let pixel of wasm.iterator.pixels()) {
			if (iterations === 1) {
				assert.equal(pixel.x, 1024, "pixel.x")
				assert.equal(pixel.y, -1024, "pixel.y")
				assert.equal(pixel.rot, 128, "pixel.rot")
			} else if (iterations === 0) {
				assert.equal(pixel.x, 555, "pixel.x")
				assert.equal(pixel.y, -555, "pixel.y")
				assert.equal(pixel.rot, 55, "pixel.rot")
			}

			assert.equal(pixel.styleId, "PixelStyle1", "pixel.styleId")
			assert.equal(pixel.shape.id, "Shape1", "pixel.shape.id")
			assert.equal(pixel.background.id, "Fill1", "pixel.background.id")
			assert.equal(
				pixel.background.typeValue,
				0,
				"pixel.background.typeValue"
			)
			assert.equal(
				pixel.background.color.r,
				128,
				"pixel.background.color.r"
			)
			assert.equal(
				pixel.background.color.g,
				188,
				"pixel.background.color.g"
			)
			assert.equal(
				pixel.background.color.b,
				163,
				"pixel.background.color.b"
			)
			assert.equal(
				pixel.background.color.a,
				255,
				"pixel.background.color.a"
			)
			iterations++
			// This is a failsafe, to allow the test to
			// stop in case the iterator loops forever
			if (iterations === 10) break
		}
		console.timeEnd(`Delta -> iterator`)
		// Make sure that only two iterations happened
		// There should be only two pixels to process
		assert.equal(iterations, 2, "iterations")
		done()
	})

	it("should be able to retain delta actions", async function (done) {
		const app = wasm.createApp()
		wasm.createProject(app, "Some project")

		console.time(`Delta Start`)
		// Start the deltas
		wasm.deltasStart(app)
		// Actions should be considered delta inserts
		wasm.paintLayerPixel(app, "Layer0", 1024, -1024, "PixelStyle1", 128)
		// Transform the deltas into project actions
		wasm.deltasApply(app)

		// Start the deltas again...
		wasm.deltasStart(app)
		// Keep the previous paintLayerPixel at index 0
		wasm.deltaRetain(app, 1)
		// Should be inserted at position 1 (a retain is being used)
		wasm.paintLayerPixel(app, "Layer0", 555, -555, "PixelStyle1", 55)
		wasm.deltasApply(app)
		console.timeEnd(`Delta Start`)

		console.time(`Delta -> changeState`)
		// Check that the deltas were applied
		wasm.changeState(app, 0) // Apply the actions
		console.timeEnd(`Delta -> changeState`)
		// Create the iterator that is going to be used to
		// read the pixels
		console.time(`Delta -> iterator`)
		wasm.iterator.start(app, 0)
		let iterations = 0
		for (let pixel of wasm.iterator.pixels()) {
			if (iterations === 0) {
				assert.equal(pixel.x, 1024, "pixel.x")
				assert.equal(pixel.y, -1024, "pixel.y")
				assert.equal(pixel.rot, 128, "pixel.rot")
			} else if (iterations === 1) {
				assert.equal(pixel.x, 555, "pixel.x")
				assert.equal(pixel.y, -555, "pixel.y")
				assert.equal(pixel.rot, 55, "pixel.rot")
			}

			assert.equal(pixel.styleId, "PixelStyle1", "pixel.styleId")
			assert.equal(pixel.shape.id, "Shape1", "pixel.shape.id")
			assert.equal(pixel.background.id, "Fill1", "pixel.background.id")
			assert.equal(
				pixel.background.typeValue,
				0,
				"pixel.background.typeValue"
			)
			assert.equal(
				pixel.background.color.r,
				128,
				"pixel.background.color.r"
			)
			assert.equal(
				pixel.background.color.g,
				188,
				"pixel.background.color.g"
			)
			assert.equal(
				pixel.background.color.b,
				163,
				"pixel.background.color.b"
			)
			assert.equal(
				pixel.background.color.a,
				255,
				"pixel.background.color.a"
			)
			iterations++
			// This is a failsafe, to allow the test to
			// stop in case the iterator loops forever
			if (iterations === 10) break
		}
		console.timeEnd(`Delta -> iterator`)
		// Make sure that only two iterations happened
		// There should be only two pixels to process
		assert.equal(iterations, 2, "iterations")
		done()
	})
})
