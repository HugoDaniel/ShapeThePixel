#include "shapeaux.h"
/*
** Care was taken to provide for Swift compatibility in the C code. Here the
** public API functions are used with the SWIFT macro, allowing their names and
** usage to be set for Swift.
** For more info about this macro see:
** https://github.com/apple/swift/blob/main/docs/CToSwiftNameTranslation.md
** And here it is:
*/
#define SWIFT(n) __attribute__((swift_name(n)))
