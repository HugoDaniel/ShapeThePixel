#ifndef SHAPEAUX_H
/*
** Shapeaux.h
**  _______ __                       _______       ___ ___
** |   _   |  |--.---.-.-----.-----.|   _   .--.--(   Y   )
** |   1___|     |  _  |  _  |  -__||.  1   |  |  |\  1  /
** |____   |__|__|___._|   __|_____||.  _   |_____|/  _  \
** |:  1   |           |__|         |:  |   |     /:  |   \
** |::.. . |                        |::.|:. |    (::. |:.  )
** `-------'                        `--- ---'     `--- ---'
**
** The engine for the Shape The Pixel app.
** Shapeaux is where the logic happens and the app state is kept and transformed.
**
** We'll start by exposing all of the Shapeaux public API inside the SHAPEAUX_H
** define. The implementation of these functions follows in the shapeaux.c, a
** good place to read for an in-depth guide over what parts this public API
** juggles.
**
*/
/*-- MARK: The Shapeaux Public API (Forward-Declarations) --------------------*/
#define SHAPEAUX_H (1)
#include <stddef.h> // size_t
#include <stdbool.h> // bool, true, false
#include <stdint.h> // int32_t
#include <string.h> // strncmp
#include <stdio.h> // snprintf, perror
#include <stdlib.h> // exit
/*
** If you are using this code in your own implementation you can optionally
** provide the following defines:
**
**   SHAPEAUX_CALLOC(n,s) - your own calloc() implementation (default: calloc(n,s))
**   SHAPEAUX_FREE(p)     - your own free() implementation (default: free(p))
*/
#if !defined(SHAPEAUX_CALLOC)
    #define SHAPEAUX_CALLOC(n,s) calloc(n,s)
#endif
#if !defined(SHAPEAUX_FREE)
    #define SHAPEAUX_FREE(p) free(p)
#endif

typedef struct app_t app_t;

app_t* create_shape_the_pixel(void);

void destroy_shape_the_pixel(app_t * app);

void create_project(app_t* const app,
                    char const * const project_id);

size_t action_paint_layer_pixel(app_t* const app,
                                char const * const layer_id,
                                int32_t const x,
                                int32_t const y,
                                char const * const style_id,
                                uint8_t const rotation);

/*
** Starts a delta change operation. Most changes are going to be done through
** deltas. Deltas are the differences to the current state, they control the
** list of actions in a project and where new actions should be placed at.
*/
void deltas_start(app_t* const app);

void deltas_stop(app_t* const app);

void deltas_apply(app_t* const app);

void delta_retain(app_t* const app, size_t amount);

void delta_delete(app_t* const app, size_t amount);

size_t change_state(app_t* const app, size_t from);

void revert_state(app_t* const app, size_t to);

void clear_error(app_t* const app);

/*-- MARK: Iterator API ------------------------------------------------------*/

typedef struct pixel_iter_t pixel_iter_t;
typedef struct pixel_iter_value_t pixel_iter_value_t;

pixel_iter_t* create_pixel_iter(void);

size_t pixel_iter_start(pixel_iter_t* const iter,
                        app_t* const app,
                        size_t layer_index,
                        pixel_iter_value_t * const values);

void pixel_iter_stop(pixel_iter_t* const iter);

struct pixel_iter_t* pixel_iter_next(pixel_iter_t* const iter);

/*-- MARK: Iterator Read-Only Functions --------------------------------------*/

int32_t x(pixel_iter_t const * const iter, size_t const pos);

int32_t y(pixel_iter_t const * const iter, size_t const pos);

uint8_t rotation(pixel_iter_t const * const iter, size_t const pos);

typedef enum diff_kind_t {
    // NEW is the 0 value, meaning that if nothing is set, the assumption is
    // that this is a new value.
    DIFF_IS_NEW,
    DIFF_UNCHANGED,
    DIFF_HAS_CHANGED,
    DIFF_WAS_REMOVED
} diff_kind_t;

diff_kind_t
diff_px(pixel_iter_t const * const iter, size_t const pos);
diff_kind_t
diff_background(pixel_iter_t const * const iter, size_t const pos);
diff_kind_t
diff_style(pixel_iter_t const * const iter, size_t const pos);
diff_kind_t
diff_shape(pixel_iter_t const * const iter, size_t const pos);

/*-- MARK: Fill access functions ---------------------------------------------*/
/*
** The `fill_kind_t` represents the possible types of fills.
** For now only a static color is supported. In the future other types are
** going to be available as fills.
*/
typedef enum fill_kind_t {
    FILL_COLOR,
} fill_kind_t;
typedef struct fill_t fill_t;
uint32_t fill_color(fill_t const * const f);
fill_kind_t fill_type(fill_t const * const f);
char const * fill_id(fill_t const * const f);

/*-- MARK: Arrays iteration helper functions ---------------------------------*/
size_t fills_length(app_t const * const app);
fill_t const * fill(app_t const * const app, size_t index);

typedef struct shape_t shape_t;
char const * shape_id(shape_t const * const shape);
size_t shapes_length(app_t const * const app);
shape_t const * shape(app_t const * const app, size_t index);

typedef struct style_t style_t;
char const * style_id(style_t const * const style);
size_t styles_length(app_t const * const app);
style_t const * style(app_t const * const app, size_t index);

typedef struct layer_t layer_t;
char const * layer_id(layer_t const * const layer);
size_t layers_length(app_t const * const app);
layer_t const * layer(app_t const * const app, size_t index);

const fill_t *
background(pixel_iter_t const * const iter, size_t const pos);

/*-- MARK: Style access functions --------------------------------------------*/


typedef struct pixel_t pixel_t;

const pixel_t *
px(pixel_iter_t const * const iter, size_t const pos);


/*-- MARK: Sizing Functions --------------------------------------------------*/

/*
** Sizing functions.
** These are relevant in WebAsm code to navigate through
** the heap memory and access the struct's fields.
*/

/*
** This method returns the result of `sizeof(pixel_iter_value_t)`
*/
size_t pixel_iter_value_size(void);

/*
** This method returns the result of `sizeof(pixel_t)`
*/
size_t pixel_size(void);

/*
** This method returns the size in bytes of a pointer.
*/
size_t pointer_size(void);

/*
** This method returns the result of `sizeof(fill_kind_t)`
*/
size_t fill_type_size(void);

#endif /* SHAPEAUX_H */
