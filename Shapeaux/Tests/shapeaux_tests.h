#ifndef SHAPEAUX_TESTS_H
#include "./munit.h" // The external unit tests framework - µnit

/*
** Test that the `shapeaux_id_t` exists
*/
static MunitResult
test_id_exists(const MunitParameter params[], void* data) {
    shapeaux_id_t id = {0};
    
    munit_assert_not_null(&id);
    return MUNIT_OK;
}

/*
** Test that the `shapeaux_id_t` value starts with a default value
*/
static MunitResult
test_id_has_default_value(const MunitParameter params[], void* data) {
    shapeaux_id_t id = {0};
    
    munit_assert_string_equal(id.value, "");
    return MUNIT_OK;
}

/*
** Test that the shapeaux_id_t initialization is successul
*/
static MunitResult
test_id_can_start_with_value(const MunitParameter params[], void* data) {
    shapeaux_id_t id = {
        .value = "This is an Id"
    };
    
    munit_assert_string_equal(id.value, "This is an Id");
    return MUNIT_OK;
}

/*
** Test that the shapeaux_id_t has a constructor function
*/
static MunitResult
test_id_has_constructor(const MunitParameter params[], void* data) {
    shapeaux_id_t id = shapeaux_create_id("Some id");
    
    munit_assert_string_equal(id.value, "Some id");
    return MUNIT_OK;
}

/*
** Test that the shapeaux_id_equal() function exists
*/
static MunitResult
test_id_has_equal_method(const MunitParameter params[], void* data) {
    
    munit_assert((bool)shapeaux_id_equal);
    return MUNIT_OK;
}

/*
** Test that two ids can be compared with the shapeaux_id_equal() function
*/
static MunitResult
test_id_can_be_compared(const MunitParameter params[], void* data) {
    shapeaux_id_t id1 = { .value = "This is Id1" };
    shapeaux_id_t id2 = { .value = "This is Id2" };
    munit_assert_false(shapeaux_id_equal(&id1, &id2));
    
    shapeaux_id_t id1_again = { .value = "This is Id1" };
    munit_assert_true(shapeaux_id_equal(&id1, &id1_again));
    
    shapeaux_id_t id_empty1 = {0};
    shapeaux_id_t id_empty2 = {0};
    
    munit_assert_true(shapeaux_id_equal(&id_empty1, &id_empty2));
    munit_assert_false(shapeaux_id_equal(&id1, &id_empty1));
    
    return MUNIT_OK;
}

/*
** Test that the `color_t` exists
*/
static MunitResult
test_color_exists(const MunitParameter params[], void* data) {
    color_t color = {0};
    
    munit_assert_not_null(&color);
    return MUNIT_OK;
}
/*
** Test that the color_t initialization is successul
*/
static MunitResult
test_color_can_be_initialized(const MunitParameter params[], void* data) {
    color_t color = {
        .r = 100,
        .g = 200,
        .b = 250,
        .a = 0.5f,
    };
    
    munit_assert_uint8(color.r, ==, 100);
    munit_assert_uint8(color.g, ==, 200);
    munit_assert_uint8(color.b, ==, 250);
    munit_assert_true(color.a == 0.5f);
    return MUNIT_OK;
}

/*
** Test that the color_t shapeaux_create_color() exists
*/
static MunitResult
test_color_has_create_function(const MunitParameter params[], void* data) {
    color_t color = shapeaux_create_color(100, 200, 250);
    
    munit_assert_uint8(color.r, ==, 100);
    munit_assert_uint8(color.g, ==, 200);
    munit_assert_uint8(color.b, ==, 250);
    munit_assert_true(color.a == 1.0f);
    return MUNIT_OK;
}

/*
** Test that the color_t shapeaux_color_hex() produces the expected output
*/
static MunitResult
test_color_has_hex_function(const MunitParameter params[], void* data) {
    color_t color1 = { .r = 255, .g = 0, .b = 0, .a = 0.0f };
    munit_assert_uint32(shapeaux_color_hex(&color1), ==, 0xff000000);
    
    color_t color2 = shapeaux_create_color(255, 0, 0);
    munit_assert_uint32(shapeaux_color_hex(&color2), ==, 0xff0000ff);
    
    color_t color3 = shapeaux_create_color(255, 0, 255);
    munit_assert_uint32(shapeaux_color_hex(&color3), ==, 0xff00ffff);
    
    color_t color4 = { .r = 240, .g = 13, .b = 30, .a = 0.0f };
    munit_assert_uint32(shapeaux_color_hex(&color4), ==, 0xF00D1E00);
    return MUNIT_OK;
}

/*
** Test that the int2_t exists
*/
static MunitResult
test_int2_exists(const MunitParameter params[], void* data) {
    int2_t vec = {0};
    
    munit_assert_not_null(&vec);
    return MUNIT_OK;
}

/*
** Test that the int2_t can hold integer values
*/
static MunitResult
test_int2_can_be_initialized(const MunitParameter params[], void* data) {
    int2_t vec = { .x = 123, .y = 321 };
    
    munit_assert_int32(vec.x, ==, 123);
    munit_assert_int32(vec.y, ==, 321);
    return MUNIT_OK;
}

/*
** Test that the int2_t can hold negative values
*/
static MunitResult
test_int2_can_be_negative(const MunitParameter params[], void* data) {
    int2_t vec = { .x = -123, .y = -321 };
    
    munit_assert_int32(vec.x, ==, -123);
    munit_assert_int32(vec.y, ==, -321);
    return MUNIT_OK;
}

/*
** Test that the fill_t exists
*/
static MunitResult
test_fill_exists(const MunitParameter params[], void* data) {
    fill_t f = {0};
    
    munit_assert_not_null(&f);
    return MUNIT_OK;
}

/*
** Test that the fill_t can be initialized
*/
static MunitResult
test_fill_can_be_initialized(const MunitParameter params[], void* data) {
    fill_t f = {
        .id = { .value = "Some fill Id" },
        .type = FILL_COLOR,
        .color = { .r = 255, .g = 200, .b = 100, .a = 1.0f },
    };
    
    munit_assert_string_equal(f.id.value, "Some fill Id");
    munit_assert_true(f.type == FILL_COLOR);
    munit_assert_uint8(f.color.r, ==, 255);
    munit_assert_uint8(f.color.g, ==, 200);
    munit_assert_uint8(f.color.b, ==, 100);
    munit_assert_true(f.color.a == 1.0f);
    return MUNIT_OK;
}

/*
** Test that the fill_t has a constructor function
*/
static MunitResult
test_fill_has_constructor(const MunitParameter params[], void* data) {
    // Create the space for the fill
    fill_t f = {0};
    // Pass the space to the function that creates the fill
    shapeaux_create_fill(&f, "Some fill id", FILL_COLOR,
                        ((color_t){ .r = 100, .g = 200, .b = 50, .a = 0.5f }));
    
    munit_assert_string_equal(f.id.value, "Some fill id");
    munit_assert_true(f.type == FILL_COLOR);
    munit_assert_uint8(f.color.r, ==, 100);
    munit_assert_uint8(f.color.g, ==, 200);
    munit_assert_uint8(f.color.b, ==, 50);
    munit_assert_true(f.color.a == 0.5f);
    return MUNIT_OK;
}

/*
** Test that there is an is_empty method for fill_t.
*/
static MunitResult
test_fill_has_is_empty_method(const MunitParameter params[], void* data) {
    // Create the space for the fill
    fill_t non_empty = {0};
    // Pass it to the fill creation function
    shapeaux_create_fill(&non_empty, "Some fill id", FILL_COLOR,
                        ((color_t){ .r = 100, .g = 200, .b = 50, .a = 0.5f }));
    
    munit_assert_false(shapeaux_fill_is_empty(non_empty));
    fill_t empty_fill = {0};
    munit_assert_true(shapeaux_fill_is_empty(empty_fill));
    fill_t also_empty = {0};
    shapeaux_create_fill(&also_empty, "", FILL_COLOR,
                        ((color_t){ .r = 100, .g = 200, .b = 50, .a = 0.5f }));
    munit_assert_true(shapeaux_fill_is_empty(also_empty));
    return MUNIT_OK;
}

/*
** Test that the shape_t exists
*/
static MunitResult
test_shape_exists(const MunitParameter params[], void* data) {
    shape_t s = {0};
    
    munit_assert_not_null(&s);
    return MUNIT_OK;
}

/*
** Test that the shape_t can be constructed
*/
static MunitResult
test_shape_can_be_constructed(const MunitParameter params[], void* data) {
    // Create space for the shape
    shape_t s = {0};
    // Pass it to the creation function
    shapeaux_create_shape(&s, "Some Id");
    
    munit_assert_string_equal(s.id.value, "Some Id");
    return MUNIT_OK;
}

/*
** Test that the style_t exists
*/
static MunitResult
test_style_exists(const MunitParameter params[], void* data) {
    style_t s = {0};
    
    munit_assert_not_null(&s);
    return MUNIT_OK;
}

/*
** Test that the style_t can be constructed
*/
static MunitResult
test_style_can_be_constructed(const MunitParameter params[], void* data) {
    // Create the space for the style
    style_t s = {0};
    // Pass the reference of the space where the style will be created
    shapeaux_create_style(&s, "Some Style Id", "Shape Id", "Bg Id");
    
    munit_assert_string_equal(s.id.value, "Some Style Id");
    munit_assert_string_equal(s.shape_id.value, "Shape Id");
    munit_assert_string_equal(s.background_id.value, "Bg Id");
    return MUNIT_OK;
}


/*
** Test that the layer_t exists
*/
static MunitResult
test_layer_exists(const MunitParameter params[], void* data) {
    layer_t l = {0};
    
    munit_assert_not_null(&l);
    return MUNIT_OK;
}

/*
** Test that the layer_t defaults to the SQUARE_LAYER enum value
*/
static MunitResult
test_layer_has_square_type_default(const MunitParameter params[], void* data) {
    layer_t l = {0};
    
    munit_assert_int((int)l.type, ==, SQUARE_LAYER);
    return MUNIT_OK;
}

/*
** Test that the layer_t starts with the empty id
*/
static MunitResult
test_layer_starts_with_empty_id(const MunitParameter params[], void* data) {
    layer_t l = {0};
    
    munit_assert_char(l.id.value[0], ==, '\0');
    return MUNIT_OK;
}

/*
** Test that the layer_t can be initialized
*/
static MunitResult
test_layer_can_be_initialized(const MunitParameter params[], void* data) {
    // Create the space for the new layer
    layer_t l = {0};
    // Pass the reference to the space where the new layer is to be created
    shapeaux_create_layer(&l, "Some Id");
    
    munit_assert_string_equal(l.id.value, "Some Id");
    munit_assert_int((int)l.type, ==, SQUARE_LAYER);
    return MUNIT_OK;
}

/*
** Test that the layer_t allows pixels to be read
*/
static MunitResult
test_layer_can_read_pixel(const MunitParameter params[], void* data) {
    // Create the space to be filled with the new layer
    layer_t l = {0};
    // Pass a reference to the layer space where the new layer will be
    shapeaux_create_layer(&l, "Some Id");
    pixel_t const * const p = shapeaux_layer_pixel(&l,
                                                   (int2_t){ .x = 0, .y = 0});
    // Should return NULL when no pixel was found:
    munit_assert_null(p);
    return MUNIT_OK;
}

/*
** Test that the layer_t allows pixels to be written
*/
static MunitResult
test_layer_can_write_pixel(const MunitParameter params[], void* data) {
    layer_t l = {0};
    shapeaux_create_layer(&l, "Some Id");
    shapeaux_id_t style1 = shapeaux_create_id("Some pixel style");
    shapeaux_layer_paint(&l, &style1, (int2_t){ .x = 0, .y = 0}, 0);
    pixel_t const * const p1 = shapeaux_layer_pixel(&l,
                                                    (int2_t){ .x = 0, .y = 0});
    munit_assert_int32(p1->at.x, ==, 0);
    munit_assert_int32(p1->at.y, ==, 0);
    munit_assert_string_equal(p1->style.value, "Some pixel style");
    
    shapeaux_id_t style2 = shapeaux_create_id("Some pixel style 2");
    shapeaux_layer_paint(&l, &style2, (int2_t){ .x = -1, .y = -2 }, 0);
    pixel_t const * const p2 = shapeaux_layer_pixel(
                                                &l,
                                                (int2_t){ .x = -1, .y = -2});
    munit_assert_int32(p2->at.x, ==, -1);
    munit_assert_int32(p2->at.y, ==, -2);
    munit_assert_string_equal(p2->style.value, "Some pixel style 2");
    // Check that it did not delete the previous pixel:
    pixel_t const * const p3 = shapeaux_layer_pixel(&l,
                                                    (int2_t){ .x = 0, .y = 0});
    munit_assert_int32(p3->at.x, ==, 0);
    munit_assert_int32(p3->at.y, ==, 0);
    munit_assert_string_equal(p3->style.value, "Some pixel style");
    return MUNIT_OK;
}

/*
** Test that the layer_t allows pixels to be updated
*/
static MunitResult
test_layer_can_update_pixel(const MunitParameter params[], void* data) {
    // Create the space for the new layer
    layer_t l = {0};
    // Pass the layer space to the layer creation function
    shapeaux_create_layer(&l, "Some Id");
    shapeaux_id_t style1 = shapeaux_create_id("Some pixel style");
    shapeaux_layer_paint(&l, &style1, (int2_t){ .x = 0, .y = 0}, 0);
    pixel_t const * const p = shapeaux_layer_pixel(&l,
                                                   (int2_t){ .x = 0, .y = 0});
    munit_assert_int32(p->at.x, ==, 0);
    munit_assert_int32(p->at.y, ==, 0);
    munit_assert_string_equal(p->style.value, "Some pixel style");
    // Write to the same pixel position:
    shapeaux_id_t style2 = shapeaux_create_id("Some pixel style 2");
    shapeaux_layer_paint(&l, &style2, (int2_t){ .x = 0, .y = 0 },
                         3);
    pixel_t const * const p2 = shapeaux_layer_pixel(&l,
                                                    (int2_t){ .x = 0, .y = 0});
    munit_assert_string_equal(p2->style.value, "Some pixel style 2");
    munit_assert_uint8(p2->rotation, ==, 3);
    return MUNIT_OK;
}

/*
** Test that the layer_t can expand
*/
static MunitResult
test_layer_can_expand(const MunitParameter params[], void* data) {
    // Create the space for the new layer
    layer_t l = {0};
    // Pass the space to the layer creation function
    shapeaux_create_layer(&l, "Some Id");
    shapeaux_id_t px_style = shapeaux_create_id("Some pixel style");
    
    for (int32_t i = 0; i < LAYER_BLOCK_SIZE*10; i++) {
        shapeaux_layer_paint(&l, &px_style, (int2_t){ .x = i, .y = i},0);
    }
    
    for (int32_t i = 0; i < LAYER_BLOCK_SIZE*10; i++) {
        pixel_t const * const px = shapeaux_layer_pixel(&l,
                                                    (int2_t){ .x = i, .y = i});
        munit_assert_int32(px->at.x, ==, i);
        munit_assert_int32(px->at.y, ==, i);
        munit_assert_string_equal(px->style.value, "Some pixel style");
    }
    
    return MUNIT_OK;
}

/*
** Test that the scene_t exists
*/
static MunitResult
test_scene_exists(const MunitParameter params[], void* data) {
    scene_t s = {0};
    
    munit_assert_not_null(&s);
    return MUNIT_OK;
}

/*
** Test that the project_t exists
*/
static MunitResult
test_project_exists(const MunitParameter params[], void* data) {
    project_t p = {0};
    
    munit_assert_not_null(&p);
    return MUNIT_OK;
}

/*
** Test that the action_t exists
*/
static MunitResult
test_action_exists(const MunitParameter params[], void* data) {
    action_t a = {0};
    
    munit_assert_not_null(&a);
    return MUNIT_OK;
}

/*
** Test that the action_t has the type attribute
*/
static MunitResult
test_action_has_type(const MunitParameter params[], void* data) {
    action_t a = {0};
    
    munit_assert_not_null(&(a.type));
    return MUNIT_OK;
}

/*
** Test that the action_kind_t exists
*/
static MunitResult
test_action_kind_exists(const MunitParameter params[], void* data) {
    enum action_kind_t a = 0;
    
    munit_assert_not_null(&a);
    return MUNIT_OK;
}

/*
** Test that the action_kind_t PAINT_LAYER_PIXEL exists
*/
static MunitResult
test_action_kind_paint_layer_exists(const MunitParameter params[], void* data) {
    enum action_kind_t a = PAINT_LAYER_PIXEL;
    
    munit_assert_int((int)a, ==, PAINT_LAYER_PIXEL);
    return MUNIT_OK;
}

/*
** Test that the action_t has the ids array
*/
static MunitResult
test_action_has_an_array_of_ids(const MunitParameter params[], void* data) {
    action_t a = {0};
    
    munit_assert_not_null(&(a.ids));
    munit_assert_int((int)sizeof(a.ids), ==, MAX_IDS_ACTION * SHAPEAUX_ID_SIZE);
    return MUNIT_OK;
}

/*
** Test that the action_t has the values array
*/
static MunitResult
test_action_has_an_array_of_values(const MunitParameter params[], void* data) {
    action_t a = {0};
    
    munit_assert_not_null(&(a.values));
    munit_assert_int((int)sizeof(a.values), ==,
                     MAX_VALUES_ACTION * sizeof(double));
    return MUNIT_OK;
}

/*
** Test that the action_t has an array of int vectors
*/
static MunitResult
test_action_has_an_array_of_int_vectors(const MunitParameter params[],
                                        void* data) {
    action_t a = {0};
    
    munit_assert_not_null(&(a.int_vectors));
    munit_assert_int((int)sizeof(a.int_vectors), ==,
                     MAX_INT_VECTORS_ACTION * sizeof(int2_t));
    return MUNIT_OK;
}

/*
** Test that the project_t has a block of actions
*/
static MunitResult
test_project_has_actions_block(const MunitParameter params[], void* data) {
    project_t p = {0};
    
    munit_assert_not_null(&(p.blocks));
    return MUNIT_OK;
}

/*
** Test that the project_t has state
*/
static MunitResult
test_project_has_state(const MunitParameter params[], void* data) {
    project_t p = {0};
    
    munit_assert_not_null(&(p.state));
    return MUNIT_OK;
}

/*
** Test that there is a function to create a new project
*/
static MunitResult
test_project_has_create_function(const MunitParameter params[], void* data) {
    // Create the space for the new project
    project_t p = {0};
    // Pass the reference to the project space where the new project will reside
    shapeaux_create_project(&p, shapeaux_create_id(""));
    
    munit_assert_not_null(&(p));
    return MUNIT_OK;
}

/*
** Test that the project creator function sets the project id
*/
static MunitResult
test_project_can_create_with_id(const MunitParameter params[], void* data) {
    // Create the space for the new project
    project_t p = {0};
    // Pass the reference to the space where the new project will be created
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    
    munit_assert_string_equal(p.id.value, "Some Id");
    return MUNIT_OK;
}

/*
** Test that the project creator function sets an initial state
*/
static MunitResult
test_project_can_create_initial_state(const MunitParameter params[],
                                      void* data) {
    // Create the space for the new project
    project_t p = {0};
    // Pass the reference to the space where the new project will be created
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    // Initial state must contain:
    // - Two fills (one transparent and one color)
    // - A shape to paint (simple filled square)
    // - One empty layer
    // - A style made of the shape and the color fill
    scene_t initial_state = p.state;
    // Check if the first fill is a transparent fill:
    fill_t fill0 = initial_state.fills[0];
    munit_assert_string_equal(fill0.id.value, "");
    munit_assert(fill0.color.a == 0.0f);
    munit_assert_int((int)fill0.type, ==, FILL_COLOR);
    // Check if the second fill is colored:
    fill_t fill1 = initial_state.fills[1];
    munit_assert_string_equal(fill1.id.value, "Fill1");
    munit_assert(fill1.color.a == 1.0f);
    munit_assert_int((int)fill1.type, ==, FILL_COLOR);
    // There must be a shape in the initial state:
    shape_t shape1 = initial_state.shapes[1];
    munit_assert_string_equal(shape1.id.value, "Shape1");
    // There must be an empty shape:
    shape_t emptyShape = initial_state.shapes[0];
    munit_assert_string_equal(emptyShape.id.value, "");
    // There must be one layer in the initial state:
    layer_t layer0 = initial_state.layers[0];
    munit_assert_string_equal(layer0.id.value, "Layer0");
    // There must be a pixel style in the initial state:
    style_t style1 = initial_state.styles[1];
    munit_assert_string_equal(style1.id.value, "PixelStyle1");
    munit_assert_string_equal(style1.shape_id.value, "Shape1");
    munit_assert_string_equal(style1.background_id.value, "Fill1");
    // There must be an empty style:
    style_t emptyStyle = initial_state.styles[0];
    munit_assert_string_equal(emptyStyle.id.value, "");
    munit_assert_string_equal(emptyStyle.shape_id.value, "");
    munit_assert_string_equal(emptyStyle.background_id.value, "");
    // Lengths must correspond to the number of items in the arrays
    munit_assert_ulong(initial_state.fills_length, ==, 2UL);
    munit_assert_ulong(initial_state.shapes_length, ==, 2UL);
    munit_assert_ulong(initial_state.styles_length, ==, 2UL);
    munit_assert_ulong(initial_state.layers_length, ==, 1UL);
    return MUNIT_OK;
}

/*
** Test that the project creator function starts with 0 actions
*/
static MunitResult
test_project_creator_empty_actions(const MunitParameter params[],
                                            void* data) {
    // Create the space for the new project
    project_t p = {0};
    // Pass the reference to the space where the new project will be created
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    
    munit_assert_int((int) (p.blocks->actions[0].type), ==, NOP);
    
    return MUNIT_OK;
}

/*
** Test that there is a shape_the_pixel app global var
*/
static MunitResult
test_app_exists(const MunitParameter params[],
                                            void* data) {
    munit_assert_not_null((void*)&create_shape_the_pixel);
    
    return MUNIT_OK;
}

/*
** Test that there is a function to start the app
*/
static MunitResult
test_app_start_function_exists(const MunitParameter params[],
                                            void* data) {
    // Start the app by calling `create_shape_the_pixel()`, this function
    // returns a pointer to the app, this pointer is then used to manipulate
    // this app instance throughout the public api
    app_t * app = create_shape_the_pixel();

    // Assert that the working project is an empty project at the start
    munit_assert_string_equal(app->working_project.id.value, "");

    return MUNIT_OK;
}

/*
** Test that there is a function to create a project in the global app var
*/
static MunitResult
test_app_create_new_project(const MunitParameter params[],
                                            void* data) {
    
    // Start the app by calling `create_shape_the_pixel()`, this function
    // returns a pointer to the app, this pointer is then used to manipulate
    // this app instance throughout the public api
    app_t * app = create_shape_the_pixel();
    // Assert that the working project is empty, before creating a new one
    munit_assert_string_equal(app->working_project.id.value, "");
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    // Assert that the working project now has the id that it was created with
    munit_assert_string_equal(app->working_project.id.value, "Cool Project");
    
    return MUNIT_OK;
}

/*
** Test that it is possible to iterate through the fills in the app current
** working project
*/
static MunitResult
test_app_fills_iterate(const MunitParameter params[],
                                            void* data) {
    
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it
    create_project(app, "Fills Iterate");
    // Assert that there is a function to return the number of fills
    size_t number_of_fills = fills_length(app);
    // There must be two fills in the starting project (the "" and "Fill1");
    munit_assert_ulong(number_of_fills, ==, 2UL);
    // It has to be possible to retrieve a fill by its index
    fill_t const * fill0 = fill(app, 0);
    fill_t const * fill1 = fill(app, 1);
    fill_t const * fill2 = fill(app, 2);
    
    munit_assert_string_equal(fill_id(fill0), "");
    munit_assert_string_equal(fill_id(fill1), "Fill1");
    // Out of bounds fills should return 0x0
    munit_assert_null(fill2);
    return MUNIT_OK;
}

/*
** Test that it is possible to iterate through the shapes in the app current
** working project
*/
static MunitResult
test_app_shapes_iterate(const MunitParameter params[],
                                            void* data) {
    
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it
    create_project(app, "Shapes Iterate");
    // Assert that there is a function to return the number of shapes
    size_t number_of_shapes = shapes_length(app);
    // There must be two shapes in the starting project (the "" and "Shape1");
    munit_assert_ulong(number_of_shapes, ==, 2UL);
    // It has to be possible to retrieve a shape by its index
    shape_t const * shape0 = shape(app, 0);
    shape_t const * shape1 = shape(app, 1);
    shape_t const * shape2 = shape(app, 2);
    
    munit_assert_string_equal(shape_id(shape0), "");
    munit_assert_string_equal(shape_id(shape1), "Shape1");
    // Out of bounds shapes should return 0x0
    munit_assert_null(shape2);
    return MUNIT_OK;
}

/*
** Test that it is possible to iterate through the styles in the app current
** working project
*/
static MunitResult
test_app_styles_iterate(const MunitParameter params[],
                                            void* data) {
    
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it
    create_project(app, "Styles Iterate");
    // Assert that there is a function to return the number of styles
    size_t number_of_styles = styles_length(app);
    // There must be two styles in the starting project
    // (the "" and "PixelStyle1")
    munit_assert_ulong(number_of_styles, ==, 2UL);
    // It has to be possible to retrieve a style by its index
    style_t const * style0 = style(app, 0);
    style_t const * style1 = style(app, 1);
    style_t const * style2 = style(app, 2);
    
    munit_assert_string_equal(style_id(style0), "");
    munit_assert_string_equal(style_id(style1), "PixelStyle1");
    // Out of bounds styles should return 0x0
    munit_assert_null(style2);
    return MUNIT_OK;
}

/*
** Test that it is possible to iterate through the layers in the app current
** working project
*/
static MunitResult
test_app_layers_iterate(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it
    create_project(app, "Layers Iterate");
    // Assert that there is a function to return the number of layers
    size_t number_of_layers = layers_length(app);
    // There must be one layer in the starting project
    // ("Layer0")
    munit_assert_ulong(number_of_layers, ==, 1UL);
    // It has to be possible to retrieve a layer by its index
    layer_t const * layer0 = layer(app, 0);
    layer_t const * layer1 = layer(app, 1);
    
    munit_assert_string_equal(layer_id(layer0), "Layer0");
    // Out of bounds layers should return 0x0
    munit_assert_null(layer1);
    return MUNIT_OK;
}

/*
** Test that the paint layer pixel action exists and works as expected
*/
static MunitResult
test_action_paint_layer_pixel(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    // Apply the action - Using valid id strings from the default state
    action_paint_layer_pixel(app, "Layer0", 2, 3, "PixelStyle1", 200);
    // Test that it was successful
    // 1. An action was created with the correct type and args in the project
    action_t action = app->working_project.blocks->actions[0];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 2);
    munit_assert_int32(action.int_vectors[0].y, ==, 3);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 200.0);
    // 2. Can apply all actions to the state
    change_state(app, 0);
    // 3. The state layer was painted at the correct position with the
    // correct style
    layer_t layer = app->working_project.state.layers[0];
    pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                   (int2_t){ .x = 2, .y = 3});
    munit_assert_int32(p->at.x, ==, 2);
    munit_assert_int32(p->at.y, ==, 3);
    munit_assert_string_equal(p->style.value, "PixelStyle1");
    munit_assert_uint8(p->rotation, ==, 200);
    
    return MUNIT_OK;
}

/*
** Test that the paint layer pixel action can be applied twice in different
** positions.
*/
static MunitResult
test_action_paint_layer_3pixels(const MunitParameter params[],
                                       void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    // Apply the action - Using valid id strings from the default state
    action_paint_layer_pixel(app, "Layer0", 2, 3, "PixelStyle1", 1);
    action_paint_layer_pixel(app, "Layer0", 1, 2, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", -4, -2, "PixelStyle1", 3);
    // Test that it was successful
    // 1. the actions were created with the correct type and args in the project
    action_t action1 = app->working_project.blocks->actions[0];
    action_t action2 = app->working_project.blocks->actions[1];
    action_t action3 = app->working_project.blocks->actions[2];
    // Created actions must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action1.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int((int)action2.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int((int)action3.type, ==, PAINT_LAYER_PIXEL);
    // Created actions must have the provided layer string as one of its ids:
    munit_assert_string_equal(action1.ids[0].value, "Layer0");
    munit_assert_string_equal(action2.ids[0].value, "Layer0");
    munit_assert_string_equal(action3.ids[0].value, "Layer0");
    // Created actions must have the provided id string as one of its ids:
    munit_assert_string_equal(action1.ids[1].value, "PixelStyle1");
    munit_assert_string_equal(action2.ids[1].value, "PixelStyle1");
    munit_assert_string_equal(action3.ids[1].value, "PixelStyle1");
    munit_assert_int32(action1.int_vectors[0].x, ==, 2);
    munit_assert_int32(action1.int_vectors[0].y, ==, 3);
    munit_assert_int32(action2.int_vectors[0].x, ==, 1);
    munit_assert_int32(action2.int_vectors[0].y, ==, 2);
    munit_assert_int32(action3.int_vectors[0].x, ==, -4);
    munit_assert_int32(action3.int_vectors[0].y, ==, -2);
    // Created actions rotation must have the provided values
    munit_assert(action1.values[0] == 1.0);
    munit_assert(action2.values[0] == 2.0);
    munit_assert(action3.values[0] == 3.0);
    // 2. Can apply all actions to the state
    change_state(app, 0);
    // 3. The state layer was painted at the correct position with the
    // correct style
    layer_t layer = app->working_project.state.layers[0];
    pixel_t const * const p1 = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = 2, .y = 3});
    pixel_t const * const p2 = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = 1, .y = 2});
    pixel_t const * const p3 = shapeaux_layer_pixel(&layer,
                                                (int2_t){ .x = -4, .y = -2});
    munit_assert_int32(p1->at.x, ==, 2);
    munit_assert_int32(p1->at.y, ==, 3);
    munit_assert_int32(p2->at.x, ==, 1);
    munit_assert_int32(p2->at.y, ==, 2);
    munit_assert_int32(p3->at.x, ==, -4);
    munit_assert_int32(p3->at.y, ==, -2);
    munit_assert_string_equal(p1->style.value, "PixelStyle1");
    munit_assert_string_equal(p2->style.value, "PixelStyle1");
    munit_assert_string_equal(p3->style.value, "PixelStyle1");
    munit_assert_uint8(p1->rotation, ==, 1);
    munit_assert_uint8(p2->rotation, ==, 2);
    munit_assert_uint8(p3->rotation, ==, 3);
    
    return MUNIT_OK;
}

/*
** Test that the paint layer pixel action can be applied 2000x times in
** different positions.
*/
static MunitResult
test_action_paint_layer_2000x(const MunitParameter params[],
                              void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    int32_t const iterations = 2000;
    // Create the actions:
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 1);
    }
    // Apply them to the state:
    change_state(app, 0);
    
    // Confirm that they were properly done:
    layer_t layer = app->working_project.state.layers[0];
    for (int32_t i = 0; i < iterations; i++) {
        pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = i, .y = -i});
        munit_assert_int32(p->at.x, ==, i);
        munit_assert_int32(p->at.y, ==, -i);
        munit_assert_string_equal(p->style.value, "PixelStyle1");
        munit_assert_uint8(p->rotation, ==, 1);
    }
    
    return MUNIT_OK;
}

/*
** Test that the state can be changed with the actions starting
** at a given specific action index up until the end of the actions blocks
*/
static MunitResult
test_change_state_from_index(const MunitParameter params[],
                             void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    
    int32_t const iterations = BLOCK_SIZE_PROJECT_ACTION * 2 + 4;
    // Create the actions:
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 1);
    }
    // Apply only some of them to the state:
    change_state(app, iterations / 2);
    
    // Confirm that they were properly done:
    layer_t layer = app->working_project.state.layers[0];
    for (int32_t i = 0; i < iterations; i++) {
        pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = i, .y = -i});
        if (i < (iterations / 2)) {
            // The pixel must be empty, no action was applied to this pixel
            munit_assert_null(p);
        } else {
            // The pixel must be set
            munit_assert_int32(p->at.x, ==, i);
            munit_assert_int32(p->at.y, ==, -i);
            munit_assert_string_equal(p->style.value, "PixelStyle1");
            munit_assert_uint8(p->rotation, ==, 1);
        }
    }
    
    return MUNIT_OK;
}

/*
** Test that there is an "error" attribute in the main app
*/
static MunitResult
test_app_has_error_state(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();

    munit_assert_string_equal(app->error, "");
    return MUNIT_OK;
}


/*
** Test that there is an error clearing function
*/
static MunitResult
test_app_can_clear_error_state(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    munit_assert_string_equal(app->error, "");
    app->error = shapeaux_error_s("Some error", "ERROR ID");
    munit_assert_string_equal(app->error, "Some error ERROR ID");
    clear_error(app);
    munit_assert_string_equal(app->error, "");
    
    return MUNIT_OK;
}

/*
** Test that an error is set when painting with an unknown layer id
*/
static MunitResult
test_app_paint_unknown_layerid_error(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    
    action_paint_layer_pixel(app, "This layer does not exist",
                             0, 0, "PixelStyle1", 1);
    // No error is set:
    munit_assert_string_equal(app->error, "");
    // Apply them to the state:
    change_state(app, 0);
    // The error was set:
    munit_assert_string_equal(app->error,
                              "paint_layer_pixel(): Layer id was not found "
                              "This layer does not exist");
    // Confirm that it was not applied:
    layer_t layer = app->working_project.state.layers[0];
    pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                   (int2_t){ .x = 0, .y = 0});

    // Should be empty
    munit_assert_null(p);

    return MUNIT_OK;
}

/*
** Test that an error is set when painting with an unknown style id
*/
static MunitResult
test_app_paint_unknown_style_error(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Cool Project");
    action_paint_layer_pixel(app, "Layer0",
                             0, 0, "This style does not exist", 1);
    // No error is set:
    munit_assert_string_equal(app->error, "");
    // Apply them to the state:
    change_state(app, 0);
    // The error was set:
    munit_assert_string_equal(app->error,
                              "paint_layer_pixel(): Style id was not found "
                              "This style does not exist");
    // Confirm that it was not applied:
    layer_t layer = app->working_project.state.layers[0];
    pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                   (int2_t){ .x = 0, .y = 0});
    // Should be empty
    munit_assert_null(p);

    return MUNIT_OK;
}

/*
** Test that a scene can be cloned
*/
static MunitResult
test_can_clone_scene(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);

    fill_t fill0 = cloned1.fills[0];
    munit_assert_string_equal(fill0.id.value, "");
    munit_assert(fill0.color.a == 0.0f);
    munit_assert_int((int)fill0.type, ==, FILL_COLOR);
    // Check if the second fill is colored:
    fill_t fill1 = cloned1.fills[1];
    munit_assert_string_equal(fill1.id.value, "Fill1");
    munit_assert(fill1.color.a == 1.0f);
    munit_assert_int((int)fill1.type, ==, FILL_COLOR);
    // There must be a shape in the initial state:
    shape_t shape1 = cloned1.shapes[1];
    munit_assert_string_equal(shape1.id.value, "Shape1");
    // There must be an empty shape:
    shape_t emptyShape = cloned1.shapes[0];
    munit_assert_string_equal(emptyShape.id.value, "");
    // There must be one layer in the initial state:
    layer_t layer0 = cloned1.layers[0];
    munit_assert_string_equal(layer0.id.value, "Layer0");
    // There must be a pixel style in the initial state:
    style_t style1 = cloned1.styles[1];
    munit_assert_string_equal(style1.id.value, "PixelStyle1");
    munit_assert_string_equal(style1.shape_id.value, "Shape1");
    munit_assert_string_equal(style1.background_id.value, "Fill1");
    // There must be an empty style:
    style_t emptyStyle = cloned1.styles[0];
    munit_assert_string_equal(emptyStyle.id.value, "");
    munit_assert_string_equal(emptyStyle.shape_id.value, "");
    munit_assert_string_equal(emptyStyle.background_id.value, "");
    // Lengths should be equal in both styles
    munit_assert_ulong(initial.fills_length, ==, cloned1.fills_length);
    munit_assert_ulong(initial.shapes_length, ==, cloned1.shapes_length);
    munit_assert_ulong(initial.styles_length, ==, cloned1.styles_length);
    munit_assert_ulong(initial.layers_length, ==, cloned1.layers_length);
    return MUNIT_OK;
}

/*
** Test that painting a cloned scene do not affect the original scene
*/
static MunitResult
test_paint_only_affects_clone(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);

    // Paint a pixel in cloned1:
    shapeaux_id_t style1 = shapeaux_create_id("Some pixel style");
    shapeaux_layer_paint(&(cloned1.layers[0]),
                         &style1,
                         (int2_t){ .x = 0, .y = 0}, 1);
    
    pixel_t const * const p1 = shapeaux_layer_pixel(&(cloned1.layers[0]),
                                       (int2_t){ .x = 0, .y = 0});
    munit_assert_string_equal(p1->style.value, "Some pixel style");
    pixel_t const * const p0 = shapeaux_layer_pixel(&(initial.layers[0]),
                                       (int2_t){ .x = 0, .y = 0});
    // No change was made in the original state
    munit_assert_null(p0);
    return MUNIT_OK;
}

/*
** Test that painting a cloned scene do not affect the original scene
*/
static MunitResult
test_clone_paint2000x(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);
    
    shapeaux_id_t style1 = shapeaux_create_id("Some pixel style");
    int32_t const iterations = 2000;
    for(int32_t i = 0; i < iterations; i++) {
        // Paint a pixel in cloned1:
        shapeaux_layer_paint(&(cloned1.layers[0]),
                             &style1,
                             (int2_t){ .x = i, .y = -i}, 2);
    }

    // Confirm that the painting did not affect the original state
    for(int32_t i = 0; i < iterations; i++) {
        pixel_t const * const px = shapeaux_layer_pixel(&(initial.layers[0]),
                                          (int2_t){ .x = i, .y = -i});
        // Should be empty
        munit_assert_null(px);
    }
    
    // Paint the original state
    shapeaux_id_t style2 = shapeaux_create_id("Initial pixel style");
    for(int32_t i = 0; i < iterations; i++) {
        // Paint a pixel in initial:
        shapeaux_layer_paint(&(initial.layers[0]),
                             &style2,
                             (int2_t){ .x = i, .y = -i}, 3);
    }
    // Confirm that the painting did not affect the cloned state
    for(int32_t i = 0; i < iterations; i++) {
        pixel_t const * const px = shapeaux_layer_pixel(&(cloned1.layers[0]),
                                           (int2_t){ .x = i, .y = -i});
        munit_assert_string_equal(px->style.value, "Some pixel style");
        munit_assert_uint8(px->rotation, ==, 2);
    }
    return MUNIT_OK;
}

/*
** Test that changing a color of a cloned scene do not affect the original scene
*/
static MunitResult
test_color_change_only_in_clone(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);

    // Change the color in cloned1:
    cloned1.fills[0].color.r = 1;
    cloned1.fills[0].color.g = 2;
    cloned1.fills[0].color.b = 3;
    cloned1.fills[1].color.r = 4;
    cloned1.fills[1].color.g = 5;
    cloned1.fills[1].color.b = 6;
    
    // No change was made in the original state
    munit_assert_uint8(initial.fills[0].color.r, !=, 1);
    munit_assert_uint8(initial.fills[0].color.g, !=, 2);
    munit_assert_uint8(initial.fills[0].color.b, !=, 3);
    munit_assert_uint8(initial.fills[1].color.r, !=, 4);
    munit_assert_uint8(initial.fills[1].color.g, !=, 5);
    munit_assert_uint8(initial.fills[1].color.b, !=, 6);
    
    // Change the color in original:
    initial.fills[0].color.r = 10;
    initial.fills[0].color.g = 20;
    initial.fills[0].color.b = 30;
    initial.fills[1].color.r = 40;
    initial.fills[1].color.g = 50;
    initial.fills[1].color.b = 60;
    
    // No change was made in the cloned state
    munit_assert_uint8(cloned1.fills[0].color.r, ==, 1);
    munit_assert_uint8(cloned1.fills[0].color.g, ==, 2);
    munit_assert_uint8(cloned1.fills[0].color.b, ==, 3);
    munit_assert_uint8(cloned1.fills[1].color.r, ==, 4);
    munit_assert_uint8(cloned1.fills[1].color.g, ==, 5);
    munit_assert_uint8(cloned1.fills[1].color.b, ==, 6);
    
    return MUNIT_OK;
}

/*
** Test that changing a style id of a cloned scene do not affect the original
** scene
*/
static MunitResult
test_style_id_only_in_clone(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);

    // Change the style in cloned1:
    cloned1.styles[1].id.value[0] = 'X';
    cloned1.styles[0].id = shapeaux_create_id("NewStyleId");
    
    // No change was made in the original state
    munit_assert_string_equal(initial.styles[1].id.value, "PixelStyle1");
    munit_assert_string_equal(initial.styles[0].id.value, "");
    
    // Change the style in original:
    initial.styles[1].id.value[0] = 'O';
    initial.styles[0].id = shapeaux_create_id("OldStyleId");
    
    munit_assert_string_equal(cloned1.styles[0].id.value, "NewStyleId");
    munit_assert_string_equal(cloned1.styles[1].id.value, "XixelStyle1");
    return MUNIT_OK;
}

/*
** Test that changing a shape of a cloned scene do not affect the original
** scene
*/
static MunitResult
test_shape_only_in_clone(const MunitParameter params[],
                                            void* data) {
    // Start with the default initial scene of a project.
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    scene_t initial = p.state;
    
    scene_t cloned1 = {0};
    shapeaux_clone_scene(&cloned1, &initial);

    // Change the shapes in cloned1:
    cloned1.shapes[1].id.value[0] = 'X';
    shapeaux_create_shape(&cloned1.shapes[1], "Shape2");
    cloned1.shapes[0].id = shapeaux_create_id("NewShapeId");
    
    // No change was made in the original state
    munit_assert_string_equal(initial.shapes[1].id.value, "Shape1");
    munit_assert_string_equal(initial.shapes[0].id.value, "");
    
    // Change the shapes in original:
    initial.shapes[1].id.value[0] = 'O';
    shapeaux_create_shape(&initial.shapes[1], "Ohape3");
    initial.shapes[0].id = shapeaux_create_id("OldShapeId");
    
    // No change was made in the cloned state
    munit_assert_string_equal(cloned1.shapes[0].id.value, "NewShapeId");
    munit_assert_string_equal(cloned1.shapes[1].id.value, "Shape2");
    return MUNIT_OK;
}

/*
** Test that each actions block has a state checkpoint
*/
static MunitResult
test_has_state_checkpoints(const MunitParameter params[],
                                            void* data) {
    // There is a checkpoint in the first block with the same state as the
    // initial state
    project_t p = {0};
    shapeaux_create_project(&p, shapeaux_create_id("Some Id"));
    munit_assert_string_equal(p.state.layers[0].id.value,
                              p.blocks->checkpoint.layers[0].id.value
                              );
    // TODO: Add more validations, check the whole initial state
    return MUNIT_OK;
}

/*
** Test that each actions block has an updated state checkpoint. This should
** happend when a new action block is created. It must be created with a state
** checkpoint that matches the one where all the actions of the previous block
** are applied.
*/
static MunitResult
test_updates_state_checkpoints(const MunitParameter params[],
                                            void* data) {
    // Apply BLOCK_SIZE_PROJECT_ACTION + 1 actions, in order to make at least
    // 2 blocks of actions in a project.
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Checkpoint Project");
    int32_t const iterations = BLOCK_SIZE_PROJECT_ACTION + 1;
    // Create the actions:
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 1);
    }
    // Apply them to the state:
    change_state(app, 0);
    
    // Confirm that they were properly done:
    layer_t layer = app->working_project.state.layers[0];
    for (int32_t i = 0; i < iterations; i++) {
        pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = i, .y = -i});
        munit_assert_int32(p->at.x, ==, i);
        munit_assert_int32(p->at.y, ==, -i);
        munit_assert_string_equal(p->style.value, "PixelStyle1");
        munit_assert_uint8(p->rotation, ==, 1);
    }
    // Check that the new actions block has a checkpoint that is different from
    // the previous block checkpoint.
    // Check that the new actions block has a checkpoint that corresponds to
    // the actions of the previous block applied to it.
    struct actions_block_t* block1 = app->working_project.blocks;
    struct actions_block_t* block2 = block1->next;
    layer_t block1_layer = block1->checkpoint.layers[0];
    layer_t block2_layer = block2->checkpoint.layers[0];
    // block 1 layer checkpoint must not have pixels painted - it is the
    // initial state
    pixel_t const * const p1 = shapeaux_layer_pixel(&block1_layer,
                    (int2_t){
        .x = iterations - 2,
        .y = -(iterations - 2)});
    // Should be empty:
    munit_assert_null(p1);
    
    // block 2 layer checkpoint must have the BLOCK_SIZE_PROJECT_ACTION action
    // applied to it (which corresponds to the pixel painted at (i, -i)
    pixel_t const * const p2 = shapeaux_layer_pixel(&block2_layer,
                    (int2_t){
        .x = iterations - 2,
        .y = -(iterations - 2)});
    munit_assert_string_equal(p2->style.value, "PixelStyle1");
    
    // however, block 2 layer checkpoint must NOT have the
    // BLOCK_SIZE_PROJECT_ACTION+1 action applied to it, since this action
    // should belong to the next block(3) checkpoint. Block checkpoints
    // correspond to the state of all the previous block actions applied to it.
    pixel_t const * const p3 = shapeaux_layer_pixel(&block2_layer,
                    (int2_t){
        .x = iterations - 1,
        .y = -(iterations - 1)});
    // Should be empty:
    munit_assert_null(p3);
    // But it should exist in the project state:
    pixel_t const * const p4 = shapeaux_layer_pixel(&layer,
                    (int2_t){
        .x = iterations - 1,
        .y = -(iterations - 1)});
    munit_assert_string_equal(p4->style.value, "PixelStyle1");
    
    return MUNIT_OK;
}

/*
** Test that the project state can be reversed
*/
static MunitResult
test_can_reverse_state(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Can Reverse");
    
    // Paint a pixel
    action_paint_layer_pixel(app, "Layer0", 0, 0, "PixelStyle1", 1);
    change_state(app, 0);
    layer_t layer = app->working_project.state.layers[0];
    // Check that it was painted:
    pixel_t const * const p1 = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = 0, .y = 0 });
    munit_assert_string_equal(p1->style.value, "PixelStyle1");
    // Revert the state
    revert_state(app, 0);
    layer = app->working_project.state.layers[0];
    // Check that it is no longer painted:
    pixel_t const * const p2 = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = 0, .y = 0 });
    munit_assert_null(p2);
    return MUNIT_OK;
}

/*
** Test that the project state can be reversed back several actions
*/
static MunitResult
test_can_reverse_multiple_actions(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "Can Reverse Multiple");

    // Add the actions to paint many pixels
    int32_t const iterations = BLOCK_SIZE_PROJECT_ACTION + 2;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 111);
    }
    // Apply all the actions
    change_state(app, 0);
    layer_t layer = app->working_project.state.layers[0];
    // Check that it was painted:
    pixel_t const * const p1 = shapeaux_layer_pixel(&layer,
                                                    (int2_t){ .x = 0, .y = 0 });
    munit_assert_string_equal(p1->style.value, "PixelStyle1");
    munit_assert_uint8(p1->rotation, ==, 111);
    // Revert the state up to the 3rd action:
    revert_state(app, 2);
    layer = app->working_project.state.layers[0];
    // Check that it is not painted (revert goes up to, but not including
    // the index)
    pixel_t const * const p2 = shapeaux_layer_pixel(&layer,
                                                (int2_t){ .x = 2, .y = -2 });
    // Should be empty:
    munit_assert_null(p2);
    // Revert the state up to block limit:
    revert_state(app, BLOCK_SIZE_PROJECT_ACTION + 1);
    layer = app->working_project.state.layers[0];
    // Check that the previous action, at the block limit, was painted
    pixel_t const * const p3 = shapeaux_layer_pixel(&layer, (int2_t){
        .x = BLOCK_SIZE_PROJECT_ACTION,
        .y = -(BLOCK_SIZE_PROJECT_ACTION)
    });
    munit_assert_string_equal(p3->style.value, "PixelStyle1");
    munit_assert_uint8(p3->rotation, ==, 111);

    return MUNIT_OK;
}

/*
** Test that the project attribute `state_at` is pointing to the current
** action applied to the state.
*/
static MunitResult
test_project_updates_state_at(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "state_at is looking good");

    // Add the actions to paint many pixels
    int32_t const iterations = BLOCK_SIZE_PROJECT_ACTION + 2;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 111);
    }
    // Apply all the actions
    change_state(app, 0);
    // When all actions are applied, the `state_at` must point to the project.at
    munit_assert_ulong(app->working_project.at, ==,
                       app->working_project.state_at);
    // Revert to the third action
    revert_state(app, 2);
    munit_assert_ulong(app->working_project.state_at, ==, 2);
    munit_assert_ulong(app->working_project.at, ==, iterations);
    return MUNIT_OK;
}

/*
** Test that the actions delta list starts inactive and is set to active when
** the start_deltas(); function is called.
*/
static MunitResult
test_can_start_deltas(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas start");
    
    munit_assert(app->deltas.active == false);
    deltas_start(app);
    munit_assert(app->deltas.active == true);
    return MUNIT_OK;
}

/*
** Test that actions go into the delta list when deltas are started, instead
** of going into the working project actions block list.
*/
static MunitResult
test_actions_go_into_deltas(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "actions go into deltas");

    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    // actions should not go into the working project actions list when the
    // deltas are active
    action_t action = app->working_project.blocks->actions[0];
    // Created action must not be present at the actions list of the project
    munit_assert_int((int)action.type, ==, NOP);
    // Instead the action must be present at the deltas changes list
    action = app->deltas.changes[0].action;
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // The action is placed at the deltas with a DELTA_INSERT type
    munit_assert_int((int)app->deltas.changes[0].type, ==, DELTA_INSERT);
    return MUNIT_OK;
}

/*
** Test that actions go into the actions list when deltas are stopped, instead
** of going into the deltas list.
*/
static MunitResult
test_delta_stop(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "actions go into deltas");

    deltas_start(app);
    // Apply one action that should go into the deltas
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    deltas_stop(app);
    action_paint_layer_pixel(app, "Layer0", 32, -32, "PixelStyle1", 222);
    // actions should now go into the working project actions list when the
    // deltas are stopped
    action_t action = app->working_project.blocks->actions[0];
    // Created action must not be present at the actions list of the project
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Create action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 32);
    munit_assert_int32(action.int_vectors[0].y, ==, -32);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 222.0);

    return MUNIT_OK;
}

/*
** Test that the actions delta list starts empty.
*/
static MunitResult
test_deltas_start_empty(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas start empty");
    
    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    munit_assert_ulong(app->deltas.at, ==, 1);
    deltas_start(app);
    munit_assert_ulong(app->deltas.at, ==, 0);
    return MUNIT_OK;
}

/*
** Test that the delta_retain() produces a retain delta in the delta changes
** array.
*/
static MunitResult
test_deltas_retain(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas retain exists");
    
    deltas_start(app);
    delta_retain(app, 3);
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    munit_assert_ulong(app->deltas.at, ==, 2);
    munit_assert_int((int)app->deltas.changes[0].type, ==, DELTA_RETAIN);
    munit_assert_ulong(app->deltas.changes[0].amount, ==, 3);
    return MUNIT_OK;
}

/*
** Test that the delta_delete() produces a delete delta in the delta changes
** array.
*/
static MunitResult
test_deltas_delete(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas delete exists");
    
    deltas_start(app);
    delta_delete(app, 3);
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    munit_assert_ulong(app->deltas.at, ==, 2);
    munit_assert_int((int)app->deltas.changes[0].type, ==, DELTA_DELETE);
    munit_assert_ulong(app->deltas.changes[0].amount, ==, 3);
    return MUNIT_OK;
}

/*
** Test that the apply_deltas() can apply a single action in it to the current
** working project.
*/
static MunitResult
test_deltas_apply_one_action(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas apply 1 action");
    
    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 16, -16, "PixelStyle1", 111);
    deltas_apply(app);
    // The action should now be sent to the working project list of actions
    // to be applied:
    
    // An action was created with the correct type and args in the project
    action_t action = app->working_project.blocks->actions[0];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 16);
    munit_assert_int32(action.int_vectors[0].y, ==, -16);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 111.0);
    
    // Finally assert that the deltas are empty:
    munit_assert_ulong(app->deltas.at, ==, 0);
    
    return MUNIT_OK;
}

/*
** Test that the apply_deltas() can apply a single delete delta to the current
** working project.
*/
static MunitResult
test_deltas_apply_one_delete(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas can delete");
    
    // Add a couple of actions to the project, before starting the deltas
    action_paint_layer_pixel(app, "Layer0", 1, 1, "PixelStyle1", 10);
    action_paint_layer_pixel(app, "Layer0", 2, 2, "PixelStyle1", 20);
    action_paint_layer_pixel(app, "Layer0", 3, 3, "PixelStyle1", 30);
    deltas_start(app);
    // delete 1 action at the start:
    delta_delete(app, 1);
    deltas_apply(app);
    // The delete delta should now be applied to the working project list of
    // actions, deleting 1 action from the index 0
    
    // The first action should be a NOP
    action_t action = app->working_project.blocks->actions[0];
    munit_assert_int((int)action.type, ==, NOP);
    
    // The second action should be the PAINT_LAYER_PIXEL
    action = app->working_project.blocks->actions[1];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 2);
    munit_assert_int32(action.int_vectors[0].y, ==, 2);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 20.0);

    // The 3rd action should be the PAINT_LAYER_PIXEL
    action = app->working_project.blocks->actions[2];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 3);
    munit_assert_int32(action.int_vectors[0].y, ==, 3);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 30.0);
    
    return MUNIT_OK;
}

/*
** Test that the apply_deltas() can apply a single retain delta before doing
** the next delta.
*/
static MunitResult
test_deltas_apply_one_retain(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas can retain");
    
    // Add a couple of actions to the project, before starting the deltas
    action_paint_layer_pixel(app, "Layer0", 1, 1, "PixelStyle1", 10);
    action_paint_layer_pixel(app, "Layer0", 2, 2, "PixelStyle1", 20);
    action_paint_layer_pixel(app, "Layer0", 3, 3, "PixelStyle1", 30);
    deltas_start(app);
    // retain 2
    delta_retain(app, 2);
    // delete 1 action at the end:
    delta_delete(app, 1);
    deltas_apply(app);
    // The delete delta should now be applied to the working project list of
    // actions, deleting 1 action from the index 0
    
    // The first action should be NOT be a NOP
    action_t action = app->working_project.blocks->actions[0];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 1);
    munit_assert_int32(action.int_vectors[0].y, ==, 1);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 10.0);
    
    // The second action should be the PAINT_LAYER_PIXEL
    action = app->working_project.blocks->actions[1];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 2);
    munit_assert_int32(action.int_vectors[0].y, ==, 2);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 20.0);

    // The 3rd action should be a NOP
    action = app->working_project.blocks->actions[2];
    munit_assert_int((int)action.type, ==, NOP);

    return MUNIT_OK;
}


/*
** Test that the apply_deltas() can apply multiple actions to the current
** working project.
*/
static MunitResult
test_deltas_apply_many_actions(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas apply 3 actions");
    
    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 10);
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 20);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 30);
    deltas_apply(app);
    // The action should now be sent to the working project list of actions
    // to be applied:
    
    // The first action is created with the correct type and args in the project
    action_t action = app->working_project.blocks->actions[0];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 1);
    munit_assert_int32(action.int_vectors[0].y, ==, -1);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 10.0);
    
    // The 2nd action is created with the correct type and args in the project
    action = app->working_project.blocks->actions[1];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 2);
    munit_assert_int32(action.int_vectors[0].y, ==, -2);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 20.0);
    
    // The 3rd action is created with the correct type and args in the project
    action = app->working_project.blocks->actions[2];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 3);
    munit_assert_int32(action.int_vectors[0].y, ==, -3);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 30.0);
    
    // Finally assert that the deltas are empty:
    munit_assert_ulong(app->deltas.at, ==, 0);
    
    return MUNIT_OK;
}

/*
** Test that the apply_deltas() can apply multiple actions to the current
** working project.
*/
static MunitResult
test_deltas_multiple_applies(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas multiple applies");
    
    // Start the first deltas
    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 10);
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 20);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 30);
    deltas_apply(app); // [1, 2, 3]
    // Now start new deltas:
    deltas_start(app);
    action_paint_layer_pixel(app, "Layer0", 4, -4, "PixelStyle1", 40);
    deltas_apply(app); // [4, 1, 2, 3]
    // And do it again, this time with a retain:
    deltas_start(app);
    delta_retain(app, 4);
    action_paint_layer_pixel(app, "Layer0", 5, -5, "PixelStyle1", 50);
    deltas_apply(app); // [4, 1, 2, 3, 5]
    
    //
    // Action 1 (should be the x=4, rot=40)
    //
    action_t action = app->working_project.blocks->actions[0];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 4);
    munit_assert_int32(action.int_vectors[0].y, ==, -4);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 40.0);
    
    //
    // Action 2 (should be x=1, rot=10)
    //
    action = app->working_project.blocks->actions[1];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==, 1);
    munit_assert_int32(action.int_vectors[0].y, ==, -1);
    munit_assert(action.values[0] == 10.0);
    
    //
    // Action 3 (should be x=2, rot=20)
    //
    action = app->working_project.blocks->actions[2];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==, 2);
    munit_assert_int32(action.int_vectors[0].y, ==, -2);
    munit_assert(action.values[0] == 20.0);
    
    //
    // Action 4 (should be x=3, rot=30)
    //
    action = app->working_project.blocks->actions[3];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==, 3);
    munit_assert_int32(action.int_vectors[0].y, ==, -3);
    munit_assert(action.values[0] == 30.0);
    
    //
    // Action 5 (should be x=5, rot=50)
    //
    action = app->working_project.blocks->actions[4];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==, 5);
    munit_assert_int32(action.int_vectors[0].y, ==, -5);
    munit_assert(action.values[0] == 50.0);

    // Finally assert that the deltas are empty:
    munit_assert_ulong(app->deltas.at, ==, 0);
    
    return MUNIT_OK;
}

/*
** Test that the apply_deltas() can apply actions when the project has many
** action blocks already filled.
*/
static MunitResult
test_deltas_expand_blocks(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "deltas expand block");
    int32_t const iterations = BLOCK_SIZE_PROJECT_ACTION * 3;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", ((uint8_t)i) % 255);
    }
    
    // Start the delta changes
    deltas_start(app);
    // Add a single action at the start, to force the shift of all the project
    // actions in order to create space for this action at the start:
    action_paint_layer_pixel(app, "Layer0", 1337, 1337, "PixelStyle1", 137);
    deltas_apply(app);
    
    //
    // Action 1 (should be the x=1337, rot=137)
    //
    action_t action = app->working_project.blocks->actions[0];
    // Created action must have the PAINT_LAYER_PIXEL type
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    // Created action must have the provided layer string as one of its ids:
    munit_assert_string_equal(action.ids[0].value, "Layer0");
    // Created action must have the provided id string as one of its ids:
    munit_assert_string_equal(action.ids[1].value, "PixelStyle1");
    munit_assert_int32(action.int_vectors[0].x, ==, 1337);
    munit_assert_int32(action.int_vectors[0].y, ==, 1337);
    // Created action rotation must have the provided value
    munit_assert(action.values[0] == 137.0);
    
    // Get the third block
    struct actions_block_t* third =
        app->working_project.blocks->next->next;
    action = third->actions[BLOCK_SIZE_PROJECT_ACTION - 1];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==,
                       (BLOCK_SIZE_PROJECT_ACTION * 3) - 2);
    munit_assert_int32(action.int_vectors[0].y, ==,
                       -((BLOCK_SIZE_PROJECT_ACTION * 3) - 2));
    // There should be a fourth block, from the expansion:
    struct actions_block_t* fourth = third->next;
    action = fourth->actions[0];
    munit_assert_int((int)action.type, ==, PAINT_LAYER_PIXEL);
    munit_assert_int32(action.int_vectors[0].x, ==,
                       (BLOCK_SIZE_PROJECT_ACTION * 3) - 1);
    munit_assert_int32(action.int_vectors[0].y, ==,
                       - ((BLOCK_SIZE_PROJECT_ACTION * 3) - 1));
    
    
    return MUNIT_OK;
}

/*
** Test that there is an iterator for the scene values.
*/
static MunitResult
test_iterator_start_exists(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator start");
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    
    munit_assert_not_null(&iter);
    return MUNIT_OK;
}

/*
** Test that there a new iterator starts with the proper value set.
** The proper value is the one from position 0.
*/
static MunitResult
test_iterator_empty_next(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator start empty");
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    munit_assert_false(iter->is_done);
    // Move the iterator to the first non-empty pixel, which does not exist and
    // this should return a NULL pointer
    pixel_iter_t* next_iter = pixel_iter_next(iter);
    munit_assert_null(next_iter);
    munit_assert(iter->is_done);
    
    return MUNIT_OK;
}

/*
** Test that a new iterator can read VALUES_PER_ITER pixels at each
** next() call.
*/
static MunitResult
test_iterator_non_empty_next(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator start value");
    
    // Create enough actions to fill the iterator values array
    for (int32_t i = 0; i < VALUES_PER_ITER; i++) {
        action_paint_layer_pixel(app, "Layer0",
                                 10 + i, 10 - i, "PixelStyle1",
                                 120 + (uint8_t)i);
    }
    
    // Apply all the actions
    change_state(app, 0);
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    munit_assert_false(iter->is_done);
    
    // Move the iterator to the first non-empty VALUES_PER_ITER pixels
    iter = pixel_iter_next(iter);
    munit_assert_not_null(iter);
    
    pixel_iter_value_t* value = iter->value;
    // Check that the values of the iter correspond to the pixels at the scene
    for (int32_t i = 0; i < VALUES_PER_ITER; i++) {
        munit_assert_int32(value[i].px->at.x, ==, 10 + i);
        munit_assert_int32(value[i].px->at.y, ==, 10 - i);
        munit_assert_uint8(value[i].px->rotation, ==, 120 + (uint8_t)i);
        munit_assert_string_equal(value[i].style->id.value, "PixelStyle1");
        munit_assert_string_equal(value[i].style->shape_id.value, "Shape1");
        munit_assert_string_equal(value[i].style->background_id.value, "Fill1");
    }
    
    // Move the iterator to the next pixel, should be empty
    iter = pixel_iter_next(iter);
    munit_assert_null(iter);
    
    return MUNIT_OK;
}

/*
** Test that an iterator that only finds one pixel value (thus not enough to
** fill the total capacity of VALUES_PER_ITER pixels that it can read) does not
** return a NULL (0x0) value on the next() call - allowing users to read the
** pixel values from the iterator.
**
** This test also makes sure that the next next() call returns null. This is:
** When the iterator ends, but still has values, only the next next() should
** return null.
*/
static MunitResult
test_iterator_values_not_filled(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator not enough values to fill it up");
    
    // Create not enough actions to fill the iterator values array
    for (int32_t i = 0; i < VALUES_PER_ITER - 1; i++) {
        action_paint_layer_pixel(app, "Layer0",
                                 10 + i, 10 - i, "PixelStyle1",
                                 120 + (uint8_t)i);
    }
    
    // Apply all the actions
    change_state(app, 0);
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    munit_assert_false(iter->is_done);
    
    // Calling next should return an iterator that is not null but with the
    // values array not filled up
    iter = pixel_iter_next(iter);
    // This is where the test makes sure that the next() call did not return
    // a null iterator (but it the iterator is in fact finished - it just has
    // values to process still).
    munit_assert_not_null(iter);
    
    pixel_iter_value_t* value = iter->value;
    // Check that the values of the iter correspond to the pixels at the scene
    for (int32_t i = 0; i < VALUES_PER_ITER - 1; i++) {
        munit_assert_int32(value[i].px->at.x, ==, 10 + i);
        munit_assert_int32(value[i].px->at.y, ==, 10 - i);
        munit_assert_uint8(value[i].px->rotation, ==, 120 + (uint8_t)i);
        munit_assert_string_equal(value[i].style->id.value, "PixelStyle1");
        munit_assert_string_equal(value[i].style->shape_id.value, "Shape1");
        munit_assert_string_equal(value[i].style->background_id.value, "Fill1");
    }
    
    // Move the iterator to the next pixel bucket, should be empty
    iter = pixel_iter_next(iter);
    munit_assert_null(iter);
    
    return MUNIT_OK;
}

/*
** Test that there is a fill for the background of a pixel in the iterator.
** This test looks for a fill_t struct in the field `background` of the iterator
** value.
*/
static MunitResult
test_iterator_has_bg_fill(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator has fill");
    
    action_paint_layer_pixel(app, "Layer0", 0, 0, "PixelStyle1", 0);
    // Apply all the actions
    change_state(app, 0);
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    
    // Move the iterator to the first non-empty pixel
    iter = pixel_iter_next(iter);
    
    pixel_iter_value_t* value = iter->value;
    // Background is the default initial background for the "PixelStyle1" fill
    fill_t const expected = app->working_project.state.fills[1];
    // Test that the fill in the pixel value is the same as the initial fill
    munit_assert_true(value[0].background->type == expected.type);
    munit_assert_true(value[0].background->color.r == expected.color.r);
    munit_assert_true(value[0].background->color.g == expected.color.g);
    munit_assert_true(value[0].background->color.b == expected.color.b);
    munit_assert_string_equal(value[0].background->id.value, expected.id.value);

    return MUNIT_OK;
}

/*
** Test that there is a background() method that returns the current iterator
** pixel background.
** This test makes use of the read only functions:
**   - `fill_color()`
**   - `fill_type()`
**   - `fill_id()`
*/
static MunitResult
test_iterator_can_get_bg(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator has background()");
    
    action_paint_layer_pixel(app, "Layer0", 0, 0, "PixelStyle1", 0);
    // Apply all the actions
    change_state(app, 0);
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    
    // Move the iterator to the first non-empty pixel
    iter = pixel_iter_next(iter);
    munit_assert_not_null(iter);
    // Return the current pixel background
    fill_t const * bg = background(iter, 0);
    // Background is the default initial background for the "PixelStyle1" fill
    fill_t const expected = app->working_project.state.fills[1];
    uint32_t const expected_color = shapeaux_color_hex(&expected.color);
    // Test that the fill in the pixel value is the same as the initial fill
    munit_assert_true(fill_type(bg) == expected.type);
    munit_assert_uint32(fill_color(bg), ==, expected_color);
    munit_assert_string_equal(fill_id(bg), expected.id.value);

    return MUNIT_OK;
}

/*
** Test that the iterator can go through all the pixels in a project with many
** pixels. This test is important to evaluate the capacity of iterators to cross
** pixel block boundaries (which happen at every LAYER_BLOCK_SIZE).
** This test assumes that the iterator traverses the pixels in insertion order.
*/
static MunitResult
test_iterator_all_pixels(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator many pixels");
    
    // Add a bunch of paint pixel actions to the project
    int32_t iterations = 1024;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 7);
    }
    
    // Apply all the actions
    change_state(app, 0);
    // Create a new iterator and start it
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    int32_t i = 0;
    for (iter = pixel_iter_next(iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            munit_assert_int32(x(iter, value_index), ==, i);
            munit_assert_int32(y(iter, value_index), ==, -i);
            munit_assert_int32(rotation(iter, value_index), ==, 7);
        }
    }

    return MUNIT_OK;
}

/*
** Test that a scene clone is made when the iterator is stopped
*/
static MunitResult
test_iterator_stop_clone(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator stop");
    
    // Add a paint pixel action to the project
    action_paint_layer_pixel(app, "Layer0", 123, 123, "PixelStyle1", 7);
    // Apply all the actions
    change_state(app, 0);
    
    // Create a new iterator and start it
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    iter = pixel_iter_next(iter);
    pixel_iter_next(iter);
    // Stop the iterator, should clone the scene
    pixel_iter_stop(iter);
    // Check that the scene is cloned
    scene_t* iterator_scene = &(iter->previous_scene);
    
    // Look for the same fills and shapes
    fill_t fill0 = iterator_scene->fills[0];
    munit_assert_string_equal(fill0.id.value, "");
    munit_assert(fill0.color.a == 0.0f);
    munit_assert_int((int)fill0.type, ==, FILL_COLOR);
    // Check if the second fill is colored:
    fill_t fill1 = iterator_scene->fills[1];
    munit_assert_string_equal(fill1.id.value, "Fill1");
    munit_assert(fill1.color.a == 1.0f);
    munit_assert_int((int)fill1.type, ==, FILL_COLOR);
    // There must be a shape in the initial state:
    shape_t shape1 = iterator_scene->shapes[1];
    munit_assert_string_equal(shape1.id.value, "Shape1");
    // There must be an empty shape:
    shape_t emptyShape = iterator_scene->shapes[0];
    munit_assert_string_equal(emptyShape.id.value, "");
    
    // Check that the pixel painted on the original scene is available at the
    // Cloned scene
    layer_t layer = iterator_scene->layers[0];
    pixel_t const * const p = shapeaux_layer_pixel(&layer,
                                                (int2_t){ .x = 123, .y = 123});
    munit_assert_int32(p->at.x, ==, 123);
    munit_assert_int32(p->at.y, ==, 123);
    munit_assert_string_equal(p->style.value, "PixelStyle1");
    munit_assert_uint8(p->rotation, ==, 7);
    
    return MUNIT_OK;
}

/*
** Test that the iterator value has a `.diff` struct
*/
static MunitResult
test_iterator_value_has_diff(const MunitParameter params[],
                                            void* data) {
    
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator has diff");
    
    // Add a paint pixel action to the project
    action_paint_layer_pixel(app, "Layer0", 123, 123, "PixelStyle1", 7);
    // Apply all the actions
    change_state(app, 0);
    
    // Create a new iterator and start it
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    iter = pixel_iter_next(iter);
    
    // Check that there is a diff attribute in the iter
    munit_assert_not_null(&(iter->value->diff));
    // Check that there is a `px` attribute in the iter value diff
    munit_assert_not_null(&(iter->value->diff.px));
    // Check that there is a `background` attribute in the iter value diff
    munit_assert_not_null(&(iter->value->diff.background));
    // Check that there is a `style` attribute in the iter value diff
    munit_assert_not_null(&(iter->value->diff.style));
    // Check that there is a `shape` attribute in the iter value diff
    munit_assert_not_null(&(iter->value->diff.shape));
    
    return MUNIT_OK;
}

/*
** Test that the iterator diff values default to NEW when no clone is made
** (a clone is only made when the `pixel_iter_stop()` is called).
*/
static MunitResult
test_iterator_diff_default_new(const MunitParameter params[],
                                            void* data) {
    
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator diffs are NEW");
    
    // Add a few paint pixel actions to the project
    int32_t iterations = 200;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 7);
    }
    // Apply all the actions
    change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* iter = create_pixel_iter();
    pixel_iter_start(iter, app, 0, 0x0);
    int32_t i = 0;
    for (iter = pixel_iter_next(iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }

    return MUNIT_OK;
}

/*
** Test that the iterator can properly signal the unchanged pixels in a 2nd
** iteration execution.
*/
static MunitResult
test_iterator_diff_unchanged(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator diff UNCHANGED");
    
    // Add a few paint pixel actions to the project
    int32_t iterations = 200;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 7);
    }
    // Apply all the actions
    change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* pixel_iter = create_pixel_iter();
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    int32_t i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }
    // Create a clone
    pixel_iter_stop(pixel_iter);
    // Re-run the iterator, the diff values should now be UNCHANGED, since
    // they were not changed between iterations
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
        }
    }

    return MUNIT_OK;
}

/*
** Test that the iterator can properly signal the new pixels in a 2nd
** iteration execution.
*/
static MunitResult
test_iterator_diff_new(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator diff NEW 2nd run");
    
    // Add a few paint pixel actions to the project
    int32_t iterations = 1;
    for (int32_t i = 0; i < iterations; i++) {
        action_paint_layer_pixel(app, "Layer0", i, -i, "PixelStyle1", 7);
    }
    // Apply all the actions
    size_t at = change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* pixel_iter = create_pixel_iter();
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    int32_t i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }
    // Create a clone
    pixel_iter_stop(pixel_iter);
    
    // Create and apply new actions
    action_paint_layer_pixel(app, "Layer0", 1337, -1337, "PixelStyle1", 1);
    action_paint_layer_pixel(app, "Layer0", 31337, -31337, "PixelStyle1", 2);
    change_state(app, at);
    
    // Re-run the iterator, the diff `px` values should be UNCHANGED except
    // for the two pixels painted above
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            // Check if these are the newly painted pixels above
            if (x(iter, value_index) == 1337 || x(iter, value_index) == 31337) {
                // These are the new values
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_IS_NEW);
            } else {
                // These values are the same as before
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_UNCHANGED);
            }
            // Regardless of the `px` position, all the other diff values
            // should be unchanged because the pixels were painted with
            // existing styles, shapes and fills
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
        }
    }

    return MUNIT_OK;
}

/*
** Test that the iterator can properly signal the changed pixels in a 2nd
** iteration execution.
*/
static MunitResult
test_iterator_diff_has_changed(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator diff HAS_CHANGED 2nd run");
    
    // Add a few paint pixel actions to the project
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 4, -4, "PixelStyle1", 2);
    // Apply all the actions
    size_t at = change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* pixel_iter = create_pixel_iter();
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    int32_t i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }
    // Create a clone
    pixel_iter_stop(pixel_iter);
    
    // Update previous actions, change the rotation
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 1);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 3);
    action_paint_layer_pixel(app, "Layer0", 4, -4, "PixelStyle1", 4);
    // Does not change
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 2);
    change_state(app, at);
    
    // Re-run the iterator, the diff `px` values should be HAS_CHANGED except
    // for the pixel at (2,-2)
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            // Check if these are the newly painted pixels above
            if (x(iter, value_index) == 2) {
                // This pixel (2,-2) should be unchanged
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_UNCHANGED);
            } else {
                // These values are the same as before
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_HAS_CHANGED);
            }
            // Regardless of the `px` position, all the other diff values
            // should be unchanged because the pixels were painted with
            // existing styles, shapes and fills
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
        }
    }

    return MUNIT_OK;
}

/*
** Test that the iterator can properly signal a pixel with a changed background
*/
static MunitResult
test_iterator_diff_bg_changed(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterator diff background HAS_CHANGED 2nd run");
    
    // Add a few paint pixel actions to the project
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 4, -4, "PixelStyle1", 2);
    // Apply all the actions
    size_t at = change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* pixel_iter = create_pixel_iter();
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    int32_t i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;

            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }
    // Create a clone
    pixel_iter_stop(pixel_iter);
    
    // Update the background fill in the current state
    app->working_project.state.fills[1].color.r = 1;
    // ^^ the background fill being used in the style "PixelStyle1" is the
    // "Fill1" which resides at the fills[1] position of the working project
    // state

    // Re-run the iterator, the diff `background` attribute should be
    // DIFF_HAS_CHANGED for all the pixels (the fill was changed, which impacts
    // all the pixels that were using it as a background)
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            // Check if the current pixel has a changed background
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_HAS_CHANGED);
            // Regardless of the `background` value, all the other diff values
            // should be unchanged because the pixels were not altered
            munit_assert_int((int)(diff_px(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
        }
    }

    return MUNIT_OK;
}

/*
** Test that the iterator runs through the deleted pixels
*/
static MunitResult
test_iterator_diff_removed(const MunitParameter params[],
                                            void* data) {
    // Start the app
    app_t * app = create_shape_the_pixel();
    // Create a project in it, and set it as the working project
    create_project(app, "iterates through removed pixels");
    
    // Add a few paint pixel actions to the project
    action_paint_layer_pixel(app, "Layer0", 0, -0, "PixelStyle1", 0);
    action_paint_layer_pixel(app, "Layer0", 1, -1, "PixelStyle1", 1);
    action_paint_layer_pixel(app, "Layer0", 2, -2, "PixelStyle1", 2);
    action_paint_layer_pixel(app, "Layer0", 3, -3, "PixelStyle1", 3);
    // Apply all the actions
    size_t at = change_state(app, 0);
    
    // Create a new iterator and start it without having a previous clone
    pixel_iter_t* pixel_iter = create_pixel_iter();
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    int32_t i = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            
            // Check that all the pixels are marked as new
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_IS_NEW);
        }
    }
    // Create a clone
    pixel_iter_stop(pixel_iter);
    
    // Delete two of the painted pixels
    // Delete the pixel at position (1,-1)
    shapeaux_layer_delete(&(app->working_project.state.layers[0]), (int2_t){
        .x = 1, .y = -1
    });
    // Delete the pixel at position (2,-2)
    shapeaux_layer_delete(&(app->working_project.state.layers[0]), (int2_t){
        .x = 2, .y = -2
    });

    // Re-run the iterator, the diff `px` values should be WAS_REMOVED
    // for the pixels at (1,-1) and (2, -2)
    pixel_iter_start(pixel_iter, app, 0, 0x0);
    i = 0;
    size_t deleted_pixels_iterated = 0;
    for (pixel_iter_t* iter = pixel_iter_next(pixel_iter);
         iter != 0x0;
         iter = pixel_iter_next(iter)) {
        for (size_t value_index = 0; value_index < VALUES_PER_ITER;
             value_index++, i++) {
            // Don't process NULL values, which can happen when the number of
            // pixels is not a multiple of the VALUES_PER_ITER (leaving empty
            // values in the iterator)
            if (iter->value[value_index].px == 0x0) continue;
            if (x(iter, value_index) == 1 || x(iter, value_index) == 2) {
                deleted_pixels_iterated++;

                // Check if the current pixel has a changed background
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_WAS_REMOVED);
            } else {
                munit_assert_int((int)(diff_px(iter, value_index)), ==,
                                 (int)DIFF_UNCHANGED);
            }
            // Regardless of the `px` value, all the other diff values
            // should be unchanged because the pixels were not altered
            munit_assert_int((int)(diff_background(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_style(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
            munit_assert_int((int)(diff_shape(iter, value_index)), ==,
                             (int)DIFF_UNCHANGED);
        }
        // Should have iterated through two deleted pixels
        munit_assert_ulong(deleted_pixels_iterated, ==, 2UL);
    }

    return MUNIT_OK;
}

/*
** Test that there a new iterator starts with `is_done` as true when there
** are no items on the layer to traverse.

static MunitResult
test_iterator_start_empty(const MunitParameter params[],
                                            void* data) {
    // Start with a clean state
    start_shape_the_pixel();
    // Create a project with a an action already present
    create_new_project("iterator start value");
    
    
    // Start the iterator, it should read the value at the position 0
    // of the layer blocks and return its processed version
    shapeaux_scene_iter_t* iter = iter_start();
    munit_assert_true(iter->is_done);
    
    return MUNIT_OK;
}
 */

/*
TESTS TO BE DONE:
 - Test that the actions delta list starts empty.
 
 - Project has state checkpoints
 - When a new block of actions is created, a proper checkpoint is created in it.
 - change_state uses checkpoints (in test set a different state before calling change_state)
 -
 */
/*
** Shapeaux is intended to be used as a library by Swift and JavaScript WebASM.
** The main() function is included to be able to run its tests directly in
** the CLI. This function is the unit tests entry-point.
*/
int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {
    // The array of tests to run
    static MunitTest all_tests[] = {
        { (char*) "test_id_exists", test_id_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_id_has_default_value", test_id_has_default_value,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_id_has_constructor", test_id_has_constructor,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_id_can_start_with_value", test_id_can_start_with_value,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_id_has_equal_method", test_id_has_equal_method,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_id_can_be_compared", test_id_can_be_compared,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_color_exists", test_color_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_color_can_be_initialized",
            test_color_can_be_initialized,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_color_has_create_function",
            test_color_has_create_function,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_color_has_hex_function", test_color_has_hex_function,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_int2_exists", test_int2_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_int2_can_be_initialized", test_int2_can_be_initialized,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_int2_can_be_negative", test_int2_can_be_negative,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_fill_exists", test_fill_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_fill_can_be_initialized", test_fill_can_be_initialized,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_fill_has_constructor", test_fill_has_constructor,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_fill_has_is_empty_method",
            test_fill_has_is_empty_method,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_shape_exists", test_shape_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_shape_can_be_constructed",
            test_shape_can_be_constructed,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_style_exists", test_style_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_style_can_be_constructed",
            test_style_can_be_constructed,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_exists", test_layer_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_has_square_type_default",
            test_layer_has_square_type_default,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_starts_with_empty_id",
            test_layer_starts_with_empty_id,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_can_be_initialized",
            test_layer_can_be_initialized,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_can_read_pixel", test_layer_can_read_pixel,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_can_write_pixel", test_layer_can_write_pixel,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_can_expand", test_layer_can_expand,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_layer_can_update_pixel", test_layer_can_update_pixel,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_scene_exists", test_scene_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_exists", test_project_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_exists", test_action_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_has_type", test_action_has_type,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_kind_exists", test_action_kind_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_kind_paint_layer_exists",
            test_action_kind_paint_layer_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_has_an_array_of_ids",
            test_action_has_an_array_of_ids,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_has_an_array_of_values",
            test_action_has_an_array_of_values,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_has_an_array_of_intvecs",
            test_action_has_an_array_of_int_vectors,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_has_actions_block",
            test_project_has_actions_block,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_has_state",
            test_project_has_state,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_has_create_function",
            test_project_has_create_function,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_can_create_with_id",
            test_project_can_create_with_id,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_can_create_initial_state",
            test_project_can_create_initial_state,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_creator_empty_actions",
            test_project_creator_empty_actions,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_exists",
            test_app_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_start_function_exists",
            test_app_start_function_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_create_new_project",
            test_app_create_new_project,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_fills_iterate",
            test_app_fills_iterate,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_shapes_iterate",
            test_app_shapes_iterate,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_styles_iterate",
            test_app_styles_iterate,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_layers_iterate",
            test_app_layers_iterate,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        
        
        { (char*) "test_action_paint_layer_pixel",
            test_action_paint_layer_pixel,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_paint_layer_3pixels",
            test_action_paint_layer_3pixels,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_action_paint_layer_2000x",
            test_action_paint_layer_2000x,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_change_state_from_index",
            test_change_state_from_index,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_has_error_state",
            test_app_has_error_state,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_can_clear_error_state",
            test_app_can_clear_error_state,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_paint_unknown_layerid_error",
            test_app_paint_unknown_layerid_error,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_app_paint_unknown_style_error",
            test_app_paint_unknown_style_error,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_can_clone_scene",
            test_can_clone_scene,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_paint_only_affects_clone",
            test_paint_only_affects_clone,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_color_change_only_in_clone",
            test_color_change_only_in_clone,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_style_id_only_in_clone",
            test_style_id_only_in_clone,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_shape_only_in_clone",
            test_shape_only_in_clone,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_clone_paint2000x",
            test_clone_paint2000x,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_has_state_checkpoints",
            test_has_state_checkpoints,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_updates_state_checkpoints",
            test_updates_state_checkpoints,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_can_reverse_state",
            test_can_reverse_state,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_can_reverse_multiple_actions",
            test_can_reverse_multiple_actions,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_project_updates_state_at",
            test_project_updates_state_at,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_can_start_deltas",
            test_can_start_deltas,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_actions_go_into_deltas",
            test_actions_go_into_deltas,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_start_empty",
            test_deltas_start_empty,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_delta_stop",
            test_delta_stop,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_retain",
            test_deltas_retain,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_delete",
            test_deltas_delete,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_apply_one_action",
            test_deltas_apply_one_action,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_apply_one_delete",
            test_deltas_apply_one_delete,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_apply_one_retain",
            test_deltas_apply_one_retain,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_apply_many_actions",
            test_deltas_apply_many_actions,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_multiple_applies",
            test_deltas_multiple_applies,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_deltas_expand_blocks",
            test_deltas_expand_blocks,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_start_exists",
            test_iterator_start_exists,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_empty_next",
            test_iterator_empty_next,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_non_empty_next",
            test_iterator_non_empty_next,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_values_not_filled",
            test_iterator_values_not_filled,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_has_bg_fill",
            test_iterator_has_bg_fill,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_can_get_bg",
            test_iterator_can_get_bg,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_all_pixels",
            test_iterator_all_pixels,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_stop_clone",
            test_iterator_stop_clone,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_value_has_diff",
            test_iterator_value_has_diff,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_default_new",
            test_iterator_diff_default_new,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_unchanged",
            test_iterator_diff_unchanged,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_new",
            test_iterator_diff_new,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_has_changed",
            test_iterator_diff_has_changed,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_bg_changed",
            test_iterator_diff_bg_changed,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { (char*) "test_iterator_diff_removed",
            test_iterator_diff_removed,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        
        
        { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    };
    // Single test:
    static MunitTest single_test[] = {
        { (char*) "test_app_layers_iterate",
            test_app_layers_iterate,
            0, 0, MUNIT_TEST_OPTION_NONE, 0 },
        { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    };
    const MunitSuite test_suite = {
        // This string will be prepended to all test names in this suite
        (char*) "",
        // The array of test suites.
        // single_test,
        all_tests,
        // Suites can contain other test suites. This isn't necessary for
        // shapeaux
        NULL,
        // Number of iterations of each test
        1,
        // Use the munit default settings
        MUNIT_SUITE_OPTION_NONE
    };
    return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
#endif /* SHAPEAUX_TESTS_H */
