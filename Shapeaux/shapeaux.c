/*
 ** shapeaux.c
 **  _______ __                       _______       ___ ___
 ** |   _   |  |--.---.-.-----.-----.|   _   .--.--(   Y   )
 ** |   1___|     |  _  |  _  |  -__||.  1   |  |  |\  1  /
 ** |____   |__|__|___._|   __|_____||.  _   |_____|/  _  \
 ** |:  1   |           |__|         |:  |   |     /:  |   \
 ** |::.. . |                        |::.|:. |    (::. |:.  )
 ** `-------'                        `--- ---'     `--- ---'
 **
 ** Let's build the Shape The Pixel engine together. It's easier than you think.
 ** For starters, everything happens in this file. There are no external
 ** dependencies other than the C standard libraries.
 **
 ** Tests are done in this file as well, under the SHAPEAUX_TESTS #define at the
 ** bottom, to facilitate tests the "µnit" external library is used (for tests
 ** only).
 ** Check out their site if you are interested in their approach to unit tests
 ** in C: https://nemequ.github.io/munit
 **
 ** It all starts by including the public API for Shapeaux, the "shapeaux.h"
 ** file, this file has a good overview of the API written in it.
 */
#include "shapeaux.h"

/*-- MARK: Public API Implementation -----------------------------------------*/

/*-- MARK: Internal Implementation -------------------------------------------*/

/*
** The `shapeaux_id_t` type.
**
** Most objects in Shapeaux are identifiable and make use of this `id_t` type.
** It is composed of a single `.value` attribute, a char[SHAPEAUX_ID_SIZE]
** fixed-sized array (this .value attribute is then mapped to .code in Swift,
** because there is no support for fixed-sized arrays in the language).
**
** Shapeaux does not create id's, instead it expects them to be provided by
** external code. Currently in "Shape The Pixel" the id's are being done using
** the `cuid()` method (see: http://usecuid.org). The `id_t` declared here
** holds enough space to accommodate a cuid() string.
**
*/
#define SHAPEAUX_ID_SIZE (64)
struct shapeaux_id_t {
    union {
        char value[SHAPEAUX_ID_SIZE];
        uint32_t number[SHAPEAUX_ID_SIZE / 4U];
    };
};
typedef struct shapeaux_id_t shapeaux_id_t;

/*
** Method that checks if two ids are equal.
** Uses strncmp, with n set to SHAPEAUX_ID_SIZE.
** This function does not make use of `restrict` pointers because it can
** be used to find an existing id in a collection.
*/
static inline bool
shapeaux_id_equal(shapeaux_id_t const * const id1,
                  shapeaux_id_t const * const id2) {
    // return strncmp(id1->value, id2->value, SHAPEAUX_ID_SIZE) == 0;
    
    // FUCK EMSCRIPTEN
    return ((id1->number[0] == id2->number[0] && id1->number[0] == '\0') || (
            id1->number[0] == id2->number[0] &&
            id1->number[1] == id2->number[1] &&
            id1->number[2] == id2->number[2] &&
            id1->number[3] == id2->number[3] &&
            id1->number[4] == id2->number[4] &&
            id1->number[5] == id2->number[5] &&
            id1->number[6] == id2->number[6] &&
            id1->number[7] == id2->number[7] &&
            id1->number[8] == id2->number[8] &&
            id1->number[9] == id2->number[9] &&
            id1->number[10] == id2->number[10] &&
            id1->number[11] == id2->number[11] &&
            id1->number[12] == id2->number[12] &&
            id1->number[13] == id2->number[13] &&
            id1->number[14] == id2->number[14] &&
            id1->number[15] == id2->number[15]));
}

/*
** Creates a `shapeaux_id_t` type initialized with the id from the argument.
** It uses `strncpy()` to copy the provided string in the parameter.
*/
static shapeaux_id_t
shapeaux_create_id(char const id[static 1]) {
    shapeaux_id_t result = {0};
    if (id == 0x0) {
        return result;
    }
    strncpy(result.value, id, SHAPEAUX_ID_SIZE);
    return result;
}

/*
** A helper function that returns the shapeaux_id_t value char array as a
** const char *.
*/
static inline char const *
shapeaux_get_id_str(shapeaux_id_t const * const id) {
    return id->value;
}

/*-- MARK: color_t -----------------------------------------------------------*/
/*
** The `color_t` represents an RGBA Color.
*/
typedef struct color_t {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    float a; // alpha value - between 0.0f and 1.0f
} color_t;

/*
** Returns the Color as a single uint32_t integer. Useful for hex
** color representations or quick color comparisons. The returned uint32_t has
** the following encoding:
**
** 0xRRGGBBAA
**
** The color.a double is transformed to be within the range of [0,255].
*/
static uint32_t
shapeaux_color_hex(color_t const * const c) {
    uint32_t const hex =
        (uint32_t)((c->r << 24U)
                   + (c->g << 16U)
                   + (c->b << 8U))
        + (uint8_t)(c->a * 255.0f);
    return hex;
}

/*
** Checks if two colors are equal, returns true if they are.
*/
static bool
shapeaux_color_equal(color_t const * const c1,
                     color_t const * const c2) {
    return shapeaux_color_hex(c1) == shapeaux_color_hex(c2);
}

/*-- MARK: int2_t ------------------------------------------------------------*/
/*
** An integer vector with 2 dimensions. The `int2_t` represents a pair of
** int32 numbers (x, y).
*/
struct int2_t {
    int32_t x;
    int32_t y;
};
typedef struct int2_t int2_t;

/*-- MARK: fill_t ------------------------------------------------------------*/

/*
** A fill is what is displayed inside of a shape or a path.
*/
struct fill_t {
    shapeaux_id_t id;
    color_t color;
    fill_kind_t type;
};
typedef struct fill_t fill_t;

/*
** Helper method to create a `fill_t` type.
*/
static void
shapeaux_create_fill(fill_t* const fill,
                     char id_str[64],
                     fill_kind_t type,
                     color_t color) {
    fill->id = shapeaux_create_id(id_str);
    fill->type = type;
    fill->color = color;
}

/*
** Checks if two fills are equal and returns true if they are
*/
static bool
shapeaux_fill_equal(fill_t const * const restrict fill1,
                    fill_t const * const restrict fill2) {
    return (shapeaux_id_equal(&(fill1->id), &(fill2->id))
            && shapeaux_color_equal(&(fill1->color), &(fill2->color))
            && fill1->type == fill2->type);
}

/*
** Helper function to get the color of a fill_t
*/
uint32_t
fill_color(fill_t const * const f) {
    return shapeaux_color_hex(&(f->color));
}

/*
** Helper function to get the kind_t of a fill_t
*/
fill_kind_t
fill_type(fill_t const * const f) {
    return f->type;
}

/*
** Helper function to get the fill id string from a fill_t
*/
char const *
fill_id(fill_t const * const fill) {
    return shapeaux_get_id_str(&(fill->id));
}

/*
** The `shape_t` type.
**
** A shape represents a grid element. Inside each grid element there is a shape.
** These shapes can be edited and changed. A shape can be empty, which means
** that it was not yet edited/created, it maps to a transparent grid element.
** Each shape has a unique id by which it can be identified.
**
** In this app there is a separation from the shape template, the shape paths
** and the fills for each of these paths.
** - A shape template is the set of lines and circles that compose its blueprint
** - The shape paths are made of junctions of the shape template lines and circles.
** - The shape fills are what the `fill_t`'s that the paths are filled with.
**
** The `shape_t` is responsible to track the shape template and its
** paths. The fills association is not made here and is kept out of shape_t.
**
** A `shape_t` is at minimum just an Id and its type.
** If the type is not SHAPE_EMPTY then it is expected that `shape_t` has the
** needed data for it to be edited and represented outside of the editor.
** TODO: Shape has no editor or representation data
*/
typedef struct shape_t {
    shapeaux_id_t id;
    // The `shape_kind_t` represents the possible types a shape can be.
    // Right now a shape can be a set of geometric elements that
    // make up its content.
    enum shape_kind_t {
        SHAPE_GEOMETRIC,
    } type;
} shape_t;

/*
** The shape constructor function, returns a default initialized shape_t.
*/
static void
shapeaux_create_shape(shape_t * const shape, char const id[static 1]) {
    shape->id = shapeaux_create_id(id);
    shape->type = SHAPE_GEOMETRIC;
}

/*
** Returns true if two shapes are equal.
** Two shapes are equal if they share the exact same properties.
*/
static inline bool
shapeaux_shape_equal(shape_t const * const shape1,
                     shape_t const * const shape2) {
    return (shapeaux_id_equal(&(shape1->id), &(shape2->id))
            && shape1->type == shape2->type);
    
}

/*
** Helper function to retrieve the shape id from a shape_t pointer
*/
char const *
shape_id(shape_t const * const shape) {
    return shapeaux_get_id_str(&(shape->id));
}

/*-- MARK: The layer_t Implementation ----------------------------------------*/
/*
** Shape The Pixel is made of... pixels :D these pixels are the elements of a
** grid. They reside in a layer (layer_t down bellow). Each pixel contains the
** info about its location (x,y coordinates of the layer, where 0,0 is at the
** center), its style (the shape and fills associated with it), and
** finally its rotation (in a square grid they can have 4 rotations, one for
** each side of the square).
*/
typedef struct pixel_t {
    int2_t at;
    // An id for the style_t to show in this pixel
    shapeaux_id_t style;
    // Integer represents the amount of "sides" rotate in the grid, for a square
    // grid this is up to 4 (90deg rotations each value)
    uint8_t rotation;
} pixel_t;

/*
** A layer represents a grid of pixels. It holds the pixels in blocks.
** When it starts a layer can hold up to LAYER_BLOCK_SIZE pixels. If the user
** paints some more, then another block of LAYER_BLOCK_SIZE pixels is added to
** the layer. These pixels_block_t's are linked together and hold the pixels as
** they are drawn in the layer.
*/
#define LAYER_BLOCK_SIZE (128)
typedef struct layer_t {
    // The place where the layer pixels are stored
    struct pixels_block_t {
        // Can store up to LAYER_BLOCK_SIZE pixels
        pixel_t pixels[LAYER_BLOCK_SIZE];
        // Null terminated linked list of blocks
        struct pixels_block_t* next;
    }* blocks;
    // The layer id
    shapeaux_id_t id;
    enum layer_kind_t {
        // Currently only square grids are supported, so there is only one kind
        // of layer, the SQUARE_LAYER. In the future new types of layers will
        // be supported.
        SQUARE_LAYER,
    } type;
} layer_t;

/*
** This method returns a bool value indicating if the given pixel is empty or
** not. A pixel is considered empty if it has a null style id, i.e. the
** empty string "".
*/
static bool
shapeaux_pixel_is_empty(pixel_t const some_pixel) {
    return (some_pixel.style.value[0] == '\0');
}

/*
** Creates a new layer with the given id string.
** Initializes the pixels blocks with SHAPEAUX_CALLOC and sets the layer
** type to be a SQUARE_LAYER.
*/
static void
shapeaux_create_layer(layer_t* const layer, char const id[static 1]) {
    struct pixels_block_t* pixel_block = SHAPEAUX_CALLOC(1,
                                                sizeof(struct pixels_block_t));
    if (pixel_block == 0x0) {
        perror("Not enough memory to allocate the first pixel block of a layer"
               " in create_layer()");
        exit(EXIT_FAILURE);
    }
    layer->id = shapeaux_create_id(id);
    layer->type = SQUARE_LAYER;
    layer->blocks = pixel_block;
}

/*
** Releases the heap memory of the layer
*/
static void
shapeaux_free_layer(layer_t* const some_layer) {
    // Traverse the linked list of layer blocks, and frees their memory
    struct pixels_block_t * current = some_layer->blocks;
    while (current != 0x0) {
        // Keep the reference for the next block before freeing the current one
        struct pixels_block_t * next = current->next;
        SHAPEAUX_FREE(current);
        current = next;
    }
}

/*
** Internal function that returns the layer block of `struct pixels_block_t`
** that holds the action array for the index passed as argument.
**
** This means that for a pixel with index 128, the `layer->blocks->next` pointer
** is returned (if LAYER_BLOCK_SIZE=128), since the first block (layer->blocks)
** can only hold the pixel indices [0-127].
**
** It does not do any expansion if the index is bigger than what the layer can
** currently hold (i.e. if the index provided is outside the number of blocks
** available).
**
** If no block is found, NULL is returned.
**
** The index position for the pixels array of the returned block can then be
** calculated with `index % LAYER_BLOCK_SIZE`.

static struct pixels_block_t *
shapeaux_layer_block_for_index(layer_t const * const some_layer,
                               size_t const index) {
    // Get the number of blocks that need to be traversed to find the block
    // pointer where this pixel index resides
    size_t const block_number = index / LAYER_BLOCK_SIZE;
    // Then traverse the blocks linked list, if NULL is found, then it is
    // returned (this means that the index is out of bounds).
    struct pixels_block_t * current = some_layer->blocks;
    for (size_t i = 0; i < block_number; ++i) {
        if (current->next == 0x0) {
            return 0x0;
        }
        current = current->next;
    }
    // The pointer `current` now points to the block that holds this pixel index
    // in its pixels array.
    return current;
}
 */

/*
** Returns the layer pixel at the grid position `at`. If no pixel is found, then
** 0x0 is returned.
**
** Empty pixels are not returned.
*/
static pixel_t const *
shapeaux_layer_pixel(layer_t const * const some_layer, int2_t const at) {
    // Traverse the linked list of layer blocks
    struct pixels_block_t const * current = some_layer->blocks;
    while (current != 0x0) {
        // Traverse the current block pixels array
        for (size_t i = 0; i < LAYER_BLOCK_SIZE; ++i) {
            pixel_t const pixel = current->pixels[i];
            // If found, return the pixel immediately
            if (pixel.at.x == at.x && pixel.at.y == at.y &&
                shapeaux_pixel_is_empty(pixel) == false) {
                return &(current->pixels[i]);
            }
        }
        
        // Update the pointer to be the next linked block list
        current = current->next;
    }
    
    // Nothing was found, return 0x0 at the requested position.
    // Empty elements have the style id with value "" (null string).
    // An element can be tested if it is empty with the function
    // `shapeaux_element_is_empty()`
    return 0x0;
}

/*
** Internal method that finds the address to paint a pixel at the position
** passed as argument.
** It goes through the pixels blocks list, and then looks for the first empty
** pixel. It needs to look in all positions because if the "at" position is
** found (which happens when it is already painted and is being repainted),
** then it must be selected as the address to write at.
*/
static pixel_t*
shapeaux_layer_find_paint_pos(layer_t const * const some_layer,
                              int2_t const at) {
    // Traverse the linked list of layer blocks
    struct pixels_block_t* current = some_layer->blocks;

    // Find the first empty element position and store its address
    // The position is the address found for that pixel, where to write at
    pixel_t* position = 0x0;
    while (current != 0x0) {
        // Traverse the current block elements array
        for (size_t i = 0; i < LAYER_BLOCK_SIZE; ++i) {
            pixel_t pixel = current->pixels[i];
            // If empty store the address of the first empty element found
            // Do this only once, when the position is still 0
            if (position == 0x0 && shapeaux_pixel_is_empty(pixel)) {
                position = &(current->pixels[i]);
            }
            // Continue looking for an element with the same `at` values.
            // If this element position was already written, then it needs to
            // be rewritten with the new element. Otherwise an empty position
            // will do.
            if (pixel.at.x == at.x && pixel.at.y == at.y) {
                return &(current->pixels[i]);
            }
        }
        // Update the pointer to be the next linked block list
        current = current->next;
    }
    
    return position;
}

/*
** Internal method that expands the layer blocks list with a new block.
** It updates the ->next pointer of the last block list (the one with a null
** ->next pointer) to point to the newly created block list.
**
** It returns the first available address to paint a pixel at.
*/
static pixel_t*
shapeaux_layer_expand(layer_t const * const some_layer) {
    struct pixels_block_t* current = some_layer->blocks;
    // Traverse through the linked list of blocks and find the block with
    // a null .next pointer.
    while (current->next != 0x0) {
        // Update the pointer to be the next linked block list
        current = current->next;
    }
    current->next = SHAPEAUX_CALLOC(1, sizeof(struct pixels_block_t));
    if (current->next == 0x0) {
        perror("No enough memory to expand layer in shapeaux_layer_expand()");
        exit(EXIT_FAILURE);
    }
    // Return the first available position to pain in the block
    return &(current->next->pixels[0]);
}

/*
** Paints a pixel. Painting is the action of setting an style string id
** to a pixel. The style id is copied into the pixel style attribute.
*/
static void
shapeaux_layer_paint(layer_t const * const some_layer,
                     shapeaux_id_t const * const style,
                     int2_t const at, uint8_t rotation) {
    // Find the correct position to paint at
    pixel_t* position = shapeaux_layer_find_paint_pos(some_layer, at);
    if (position == 0x0) {
        // No more free positions to paint at, create a new block
        position = shapeaux_layer_expand(some_layer);
    }
    // This is where the painting operation happens, the pixel position is set
    // and the style id copied.
    position->at = at;
    // creating and id from a string copies the string:
    position->style = *style;
    position->rotation = rotation;
}

/*
** Clears the pixel in layer position specified by the argument at.
** An empty pixel is signaled by having an empty style.id string.
*/
static void
shapeaux_layer_delete(layer_t const * const some_layer, int2_t const at) {
    pixel_t* const px = shapeaux_layer_find_paint_pos(some_layer, at);
    if (px != 0x0) {
        px->style.value[0] = '\0';
    }
}

/*
** Helper function to retrieve the layer id from a layer_t pointer.
*/
char const * layer_id(layer_t const * const layer) {
    return shapeaux_get_id_str(&(layer->id));
}

/*-- MARK: The scene_t Implementation ----------------------------------------*/

/*
** In Shape The Pixel, pixels can be styled, a style corresponds to the
** contents of a pixel, this includes: its shape, the background, and the
** association of fill_t's for each visible drawn path of the shape it has.
**
** Styles are identified by its id, and thus can be empty (the "" id marks it as
** empty)
*/
#define SHAPE_MAX_PATHS 128
typedef struct style_t {
    // This style unique id:
    shapeaux_id_t id;
    // The shape that this style applies to:
    shapeaux_id_t shape_id;
    // The fill_id for the background:
    shapeaux_id_t background_id;
    // The association between a shape path and a fill as a 2d array of:
    // [SHAPE_MAX_PATHS][path_id, fill_id]
    shapeaux_id_t path_fills[SHAPE_MAX_PATHS][2];
} style_t;

/*
** Creates a style_t initialized with default values.
*/
static void
shapeaux_create_style(style_t * const style,
                      char const id[static 1],
                      char const shape_id[static 1],
                      char const background_id[static 1]) {
    style->id = shapeaux_create_id(id);
    style->shape_id = shapeaux_create_id(shape_id);
    style->background_id = shapeaux_create_id(background_id);
}

/*
** Returns true if two styles are equal.
** Two styles are equal if they share the exact same properties.
*/
static bool
shapeaux_style_equal(style_t const * const style1,
                     style_t const * const style2) {
    bool are_equal = shapeaux_id_equal(&(style1->id), &(style2->id))
                  && shapeaux_id_equal(&(style1->shape_id), &(style2->shape_id))
                  && shapeaux_id_equal(&(style1->background_id),
                                       &(style2->background_id));
    if (are_equal) {
        // Compare the path fills of both styles
        for (size_t i = 0; i < SHAPE_MAX_PATHS && are_equal; i++) {
            are_equal = shapeaux_id_equal(&(style1->path_fills[i][0]),
                                          &(style2->path_fills[i][0]))
                     && shapeaux_id_equal(&(style1->path_fills[i][1]),
                                          &(style2->path_fills[i][1]));
        }
    }
    return are_equal;
}

/*
** Helper function to retrieve the style id from a style_t pointer.
*/
char const * style_id(style_t const * const style) {
    return shapeaux_get_id_str(&(style->id));
}

/*
** A scene is the main holder of a Shape The Pixel project. It corresponds to
** the current working environment. A scene has a maximum possible set of
** layers (which are grids), and is the main repository of fill_t's, shape_t's
** and style_t's.
**
** The style_t's make the relation between a shape and its colors (fill's),
** style_t's are what gets "painted" in the layer when the user clicks somewhere
** in the grid.
*/
#define MAX_FILLS_SCENE 512
#define MAX_SHAPES_SCENE 512
#define MAX_LAYERS_SCENE 8
#define MAX_STYLES_SCENE 512
typedef struct scene_t {
    layer_t layers[MAX_LAYERS_SCENE];
    fill_t fills[MAX_FILLS_SCENE];
    shape_t shapes[MAX_SHAPES_SCENE];
    style_t styles[MAX_STYLES_SCENE];
    // Lengths, these values determine the number of items currently in the
    // arrays defined above
    size_t layers_length;
    size_t fills_length;
    size_t shapes_length;
    size_t styles_length;
} scene_t;

/*
** Check if a scene is empty.
** A scene is empty if it has no layers (i.e. all layers have the id "")
*/
static bool
shapeaux_is_scene_empty(scene_t const * const scene) {
    for (size_t i = 0; i < MAX_LAYERS_SCENE; ++i) {
        if (scene->layers[i].id.value[0] != '\0') {
            // A non-empty layer id was found
            return false;
        }
    }
    // All layers traversed had an empty id, this scene can be considered empty.
    return true;
}

/*
** Makes a full copy of the scene.
*/
static void
shapeaux_clone_scene(scene_t * const restrict to,
                     scene_t const * const restrict from) {
    // Only the layer blocks need to be copied, because this is the only part
    // that has allocated memory in the heap, all the other parts are handled
    // by the C automatic value copy.
    *to = *from;
    // memcpy(to, from, sizeof(scene_t));
    // Copy the layers:
    for (size_t i = 0; i < MAX_LAYERS_SCENE; ++i) {
        // The layer block that is going to be copied
        struct pixels_block_t* block = to->layers[i].blocks;
        // The previous block, at start this is just the address of the block
        struct pixels_block_t** prev = &(to->layers[i].blocks);
        while (block != 0x0) {
            // Allocate space for the copy
            struct pixels_block_t* new_block =
                SHAPEAUX_CALLOC(1, sizeof(struct pixels_block_t));
            if (new_block == 0x0) {
                perror("Not enough memory to allocate memory in clone_scene()");
                exit(EXIT_FAILURE);
            }
            // And copy it...
            memcpy(new_block, block, sizeof(struct pixels_block_t));
            // Update the pointer that was pointing to the old memory address
            *prev = new_block;
            // Prepare the next iteration:
            prev = &new_block;
            block = new_block->next;
        }
    }
}

/*
** Retrieves a layer from a scene. Goes through all of the scene layers and
** returns the first layer that has the same id string as the `layer_id` arg.
** If no layer is found NULL is returned.
*/
static layer_t*
shapeaux_get_layer(scene_t* const scene, shapeaux_id_t const * const layer_id) {
    for (size_t i = 0; i < MAX_LAYERS_SCENE; ++i) {
        if (shapeaux_id_equal(&(scene->layers[i].id), layer_id)) {
            return &(scene->layers[i]);
        }
    }
    // Not found
    return 0x0;
}

/*
** Retrieves a pixel from a scene. Goes through all of the pixels of the layer
** from the scene and returns the first pixel that has the same position as the
** one passed as argument.
** If no pixel is found NULL is returned.
*/
static inline pixel_t const *
shapeaux_get_pixel(scene_t const * const scene,
                   size_t const layer_index,
                   int2_t const at) {
    return shapeaux_layer_pixel(&(scene->layers[layer_index]), at);
}

/*
** Retrieves a style from a scene. Goes through all of the scene styles and
** returns the first style that has the same id string as the `style_id` arg.
** If no style is found NULL is returned.
*/
static style_t const *
shapeaux_get_style(scene_t const * const scene,
                   shapeaux_id_t const * const style_id) {
    for (size_t i = 0; i < MAX_STYLES_SCENE; ++i) {
        if (shapeaux_id_equal(&(scene->styles[i].id), style_id)) {
            return &(scene->styles[i]);
        }
    }
    // Not found
    return 0x0;
}

/*
** Retrieves a shape from a scene. Goes through all of the scene shapes and
** returns the first that has the same id string as the `shape_id` arg.
** If no shape is found NULL is returned.
*/
static shape_t const *
shapeaux_get_shape(scene_t const * const scene,
                   shapeaux_id_t const * const shape_id) {
    for (size_t i = 0; i < MAX_STYLES_SCENE; ++i) {
        if (shapeaux_id_equal(&(scene->shapes[i].id), shape_id)) {
            return &(scene->shapes[i]);
        }
    }
    // Not found
    return 0x0;
}

/*
** Retrieves a fill_t from a scene. Goes through all of the scene fills and
** returns the first fill that has the same id string as the `fill_id` arg.
** If no fill is found NULL is returned.
*/
static fill_t const*
shapeaux_get_fill(scene_t const * const scene,
                  shapeaux_id_t const * const fill_id) {
    for (size_t i = 0; i < MAX_FILLS_SCENE; ++i) {
        if (shapeaux_id_equal(&(scene->fills[i].id), fill_id)) {
            return &(scene->fills[i]);
        }
    }
    // Not found
    return 0x0;
}

/*
** Internal function that releases the heap memory associated with a scene.
*/
static void
shapeaux_free_scene(scene_t * const some_scene) {
    for(size_t i = 0; i < MAX_LAYERS_SCENE; ++i) {
        shapeaux_free_layer(&(some_scene->layers[i]));
    }
}

/*-- MARK: Errors ------------------------------------------------------------*/


static char *
shapeaux_error_d(char const error[static 1], int const d) {
    size_t const ERROR_MAX = 255;
    char * error_msg = SHAPEAUX_CALLOC(ERROR_MAX, 1);
    if (error_msg == 0x0) {
        perror("Not enough memory to allocate memory for error message.");
        exit(EXIT_FAILURE);
    }
    if (snprintf(error_msg, ERROR_MAX, "%s %d", error, d) < 0) {
        perror("Cannot create error message");
        exit(EXIT_FAILURE);
    }
    return error_msg;
}

static char *
shapeaux_error_s(char const error[static 1], char const * const s) {
    size_t const ERROR_MAX = 255;
    char * error_msg = SHAPEAUX_CALLOC(ERROR_MAX, 1);
    if (error_msg == 0x0) {
        perror("Not enough memory to allocate memory for error message.");
        exit(EXIT_FAILURE);
    }
    if (snprintf(error_msg, ERROR_MAX, "%s %s", error, s) < 0) {
        perror("Cannot create error message");
        exit(EXIT_FAILURE);
    }
    return error_msg;
}


/*-- MARK: The project_t Implementation --------------------------------------*/
typedef enum action_kind_t {
    NOP = 0,
    PAINT_LAYER_PIXEL,
} action_kind_t;

// Actions get applied to the state by the `shapeaux_apply_one_action()` method
#define MAX_IDS_ACTION 4
#define MAX_VALUES_ACTION 16
#define MAX_INT_VECTORS_ACTION 4
typedef struct action_t {
    action_kind_t type;
    shapeaux_id_t ids[MAX_IDS_ACTION];
    int2_t int_vectors[MAX_INT_VECTORS_ACTION];
    double values[MAX_VALUES_ACTION];
} action_t;

#define BLOCK_SIZE_PROJECT_ACTION 512
typedef struct project_t {
    // The place where the actions are stored, this is a pointer to a linked
    // list of action arrays. It needs to be allocated upon creation in the
    // `shapeaux_create_project()` method.
    struct actions_block_t {
        // Each block can store up to BLOCK_SIZE_PROJECT_ACTION actions
        action_t actions[BLOCK_SIZE_PROJECT_ACTION];
        // A checkpoint points to the state at the start of this block actions
        // This is the project state that the previous block left. So the state
        // right before the action `actions[0]` of this block is applied.
        scene_t checkpoint;
        // Null terminated linked list of blocks
        struct actions_block_t* next;
    }* blocks;
    // The number of actions applied in this project.
    size_t at;
    // The current state is the scene to render
    scene_t state;
    // The state can be pointing to a previous action of this project.
    // In a normal working scenario, the `state_at` matches the project `at`,
    // however, there can situations where the user reverts the state to a
    // previous action, the `state_at` points at the last action applied to the
    // `state` var.
    size_t state_at;
    // The project id
    shapeaux_id_t id;
} project_t;

/*
** Create a new project with an initial scene. The provided id is set as the
** project id. It receives a pointer to the memory area where the project will
** be created at.
*/
static void
shapeaux_create_project(project_t* project, shapeaux_id_t const id) {
    // Allocate the memory for the first actions block, it gets free in the
    // `shapeaux_free_project()` function.
    struct actions_block_t * first_block =
        SHAPEAUX_CALLOC(1, sizeof(struct actions_block_t));
    if (first_block == 0x0) {
        perror("Not enough memory to allocate memory for the first project "
               "actions block in create_project()");
        exit(EXIT_FAILURE);
    }
    // Create the initial scene state
    // Initial scene must contain:
    // - Two fills (one transparent and one color)
    // - A shape to paint (the simple filled square)
    // - One empty layer
    // - A style made of the shape and the color fill
    
    // The fill [0] is implicit, it is the "" fill id, with a transparent
    // color.
    // For all effects, there is one colored fill.
    // This is the color that can be painted with right at the start
    shapeaux_create_fill(&(project->state.fills[1]),
                         "Fill1",
                         FILL_COLOR,
                         (color_t){
                            .r = 128,
                            .g = 188,
                            .b = 163,
        .a = 1.0f });
    // Projects start with two fills: "" and "Fill1"
    project->state.fills_length = 2;
    // Create the default shape in the initial state
    // There is an empty shape at [0], with id "".
    shapeaux_create_shape(&(project->state.shapes[1]), "Shape1");
    // Projects start with two shapes: "" and "Shape1"
    project->state.shapes_length = 2;
    // The initial layer
    shapeaux_create_layer(&(project->state.layers[0]), "Layer0");
    project->state.layers_length = 1;
    // There is an empty, transparent style at [0], with id "".
    // Create the default pixel style, it is the default shape "Shape1"
    // with the color set at "Fill1" as the background.
    shapeaux_create_style(&(project->state.styles[1]),
                          "PixelStyle1",
                          "Shape1",
                          "Fill1");
    // Projects start with two styles: "" and "PixelStyle1"
    project->state.styles_length = 2;
    // Clone the state for the first block checkpoint:
    shapeaux_clone_scene(&(first_block->checkpoint), &(project->state));
    // The empty project is initialized with the initial state and the
    // provided project id from this function argument
    project->id = id;
    project->blocks = first_block;
}

/*
** Internal function that releases the heap memory of a project.
*/
static void
shapeaux_free_project(project_t * const some_project) {
    // Traverses the linked list of action blocks, and frees their memory
    struct actions_block_t * current = some_project->blocks;
    while (current != 0x0) {
        // Keep the reference for the next block before freeing the current one
        struct actions_block_t * next = current->next;
        SHAPEAUX_FREE(current);
        current = next;
    }
    // Update the blocks pointers to be null:
    some_project->blocks = 0x0;
    // Release the memory allocated by the scene
    shapeaux_free_scene(&(some_project->state));
}

/*
** Internal function that returns the project block of `struct actions_block_t`
** that holds the action array for the index passed as argument.
**
** It expands the actions blocks linked list if the index is bigger than what
** the project can currently hold.
**
** The index position for the actions array of the returned block can then be
** calculated with `index % BLOCK_SIZE_PROJECT_ACTION`.
*/
static struct actions_block_t *
shapeaux_project_block_for_index(project_t * const some_project,
                                 size_t const index) {
    // Check if a new actions block is needed, to do that, start by finding
    // what is the block that this index falls into.
    size_t const block_number = index / BLOCK_SIZE_PROJECT_ACTION;
    // Then traverse the blocks linked list, and create a new block if the
    // next pointer is null before reaching the desired block number to put this
    // action at.
    struct actions_block_t * current = some_project->blocks;
    for (size_t i = 0; i < block_number; ++i) {
        if (current->next == 0x0) {
            current->next = SHAPEAUX_CALLOC(1, sizeof(struct actions_block_t));
            if (current->next == 0x0) {
                perror("Not enough memory to expand the project actions blocks "
                       "in project_block_for_index()");
                exit(EXIT_FAILURE);
            }
        }
        current = current->next;
    }
    // The `current` pointer now points to the block that this action should
    // go into. The location to place it in its actions static array must be
    // calculated by the remainder of the BLOCK_SIZE_PROJECT_ACTION.
    return current;
}

/*
** Internal method that opens a blank space at the desired index in the project
** action blocks. This blank space is set to a NOP, and can then hold an action.
**
** It works by shifting all the actions after that index by 1 position to the
** right. It takes advantage of any NOP actions available to prevent a full
** right shift of all the actions in the blocks.
*/
static char*
shapeaux_project_shift_actions_block(project_t * const some_project,
                                     size_t const index) {
    // Guarantee that the project at is pointing to a value greater than 0, and
    // that the index is pointing to a position before it
    if (some_project->at == 0 || index > some_project->at) {
        // If it is not, then return an error string
        return shapeaux_error_d("Unable to shift actions block "
                                "with project at position", (int)index);
    } else if (index == some_project->at) {
        // A shortcut, if the index is pointing to the position of the
        // project->at, then no actual shift is needed.
        some_project->at += 1;
        return 0x0;
    }
    
    // Traverse the actions blocks backwards, and copy each action to the
    // position to the right. Starting from the `some_project->at` which
    // holds a NOP, downwards to the `index` (the position that this
    // function wans to set as available).
    struct actions_block_t * to_block = shapeaux_project_block_for_index(
                                                some_project,
                                                some_project->at);
    struct actions_block_t * from_block = shapeaux_project_block_for_index(
                                                some_project,
                                                some_project->at - 1);
    for (size_t i = some_project->at; i > index; --i) {
        // Actions will be copied from the index before the current one (i - 1)
        // into the current index (i). These two indices can be in different
        // blocks, so it is important to start by keeping track of each relative
        // index in their respective block actions arrays.
        size_t const to_block_actions_index = i % BLOCK_SIZE_PROJECT_ACTION;
        size_t const from_block_actions_index =
            (i - 1) % BLOCK_SIZE_PROJECT_ACTION;
        
        // Check if the blocks pointers need to be adjusted to a different block
        // than the one they are currently pointing at
        if (to_block_actions_index == (BLOCK_SIZE_PROJECT_ACTION - 1)) {
            to_block = shapeaux_project_block_for_index(some_project, i);
        }
        if (from_block_actions_index == (BLOCK_SIZE_PROJECT_ACTION - 1)) {
            from_block = shapeaux_project_block_for_index(some_project, i - 1);
        }
        
        // Perform the copy
        // The action is copied forwards (to its right)
        to_block->actions[to_block_actions_index] =
            from_block->actions[from_block_actions_index];
    }
    // Set a NOP at the index
    struct actions_block_t * index_block = shapeaux_project_block_for_index(
                                                some_project,
                                                index);
    index_block->actions[index % BLOCK_SIZE_PROJECT_ACTION] = (action_t){0};
    
    // Update the project->at, the shift right was done and there is now an
    // extra position
    some_project->at += 1;
    return 0x0;
}

/*
** Inserts an action into the position specified by the provided argument.
** The action is not applied to the project state. In order to change the
** project state, call `change_state()` and pass it the number returned from
** this function.
*/
static size_t
shapeaux_project_insert_action(project_t * const some_project,
                               action_t const * const some_action,
                               size_t const index) {
    // Get the action block that holds the actions array where the index is
    // pointing to.
    struct actions_block_t * block =
        shapeaux_project_block_for_index(some_project, index);
    // Adjust the index to the actions array in the block
    size_t const action_in_block_index = index % BLOCK_SIZE_PROJECT_ACTION;
    // Add the action to the position in the actions block that corresponds to
    // the intended overal index passed in the argument
    block->actions[action_in_block_index] = *some_action;
    // Update `project.at`, which is the number of actions present
    // in the project.
    if (index >= some_project->at) {
        some_project->at = index + 1;
    }
    return index;
}

/*
** Internal function that applies the PAINT_LAYER_PIXEL action to a `scene_t`
** Returns an error string if the action is malformed or of it cannot be applied
** to the scene_t. Returns null if successful and no error was found.
*/
static char *
shapeaux_paint_layer_pixel(scene_t* const state,
                           action_t const * const action) {
    shapeaux_id_t const * const layer_id = &(action->ids[0]);
    shapeaux_id_t const * const style_id = &(action->ids[1]);
    int2_t const position = action->int_vectors[0];
    // Transform the rotation from a double to an int
    uint8_t const rotation = (uint8_t)(action->values[0]);
    
    // Get the layer
    layer_t* const layer = shapeaux_get_layer(state, layer_id);
    if (layer == 0x0) {
        // Not found, return an error string:
        return shapeaux_error_s("paint_layer_pixel(): Layer id was not"
                                " found", layer_id->value);
    }
    // Get the style
    style_t const * const style = shapeaux_get_style(state, style_id);
    if (style == 0x0) {
        // Not found, return an error string:
        return shapeaux_error_s("paint_layer_pixel(): Style id was not"
                                " found", style_id->value);
    }
    // Paint it with the action position, style and rotation
    shapeaux_layer_paint(layer, style_id, position, rotation);
    // All good, return NULL
    return 0x0;
}

static char*
shapeaux_apply_one_action(scene_t* const scene, action_t const * const action) {
    // Chose the function to call based on the type of the action provided
    switch (action->type) {
        // NOP -> Do nothing
        case NOP: return 0x0;
        // Paint:
        case PAINT_LAYER_PIXEL:
            return shapeaux_paint_layer_pixel(scene, action);
    }
}

/*
** Updates all the checkpoints of the blocks that start at the block provided
** in the `from` argument.
** An is returned if it occurs while applying actions. Otherwise, if sucessful
** 0x0 is returned (no error).
*/
static char *
shapeaux_update_checkpoints(struct actions_block_t* const from) {
    struct actions_block_t * block = from;
    // The next block is where the checkpoint is going to be set if it is
    // empty
    struct actions_block_t * next_block = block->next;
    
    while (next_block != 0x0) {
        // Run through the block actions and set a new checkpoint
        // Start by cloning the checkpoint of the block, this is going to be
        // used as the state to apply the actions to:
        shapeaux_clone_scene(&(next_block->checkpoint), &(block->checkpoint));
        for (size_t i = 0; i < BLOCK_SIZE_PROJECT_ACTION; ++i) {
            char* error = shapeaux_apply_one_action(&(next_block->checkpoint),
                                                    &(block->actions[i]));
            if (error != 0x0) {
                return error;
            }
        }
        // Update the blocks for the next iteration:
        block = next_block;
        next_block = block->next;
    }
    return 0x0;
}

static char *
shapeaux_update_empty_checkpoints(project_t * const some_project) {
    // The current block that will hold the actions to create the checkpoint
    // of the next block (in case it has an empty checkpoint).
    struct actions_block_t * block = some_project->blocks;
    // The next block is where the checkpoint is going to be set if it is
    // empty
    struct actions_block_t * next_block = block->next;
    
    // Find the block with the first empty checkpoint, and update all the
    // blocks that succeed it.
    while (next_block != 0x0) {
        // Check if it has an empty checkpoint
        if (shapeaux_is_scene_empty(&(next_block->checkpoint))) {
            // Found the first block with an empty checkpoint, it sits at the
            // "next_block" var. Start the update action with it and break this
            // loop.
            return shapeaux_update_checkpoints(block);
        }
        // Update the blocks for the next iteration:
        block = next_block;
        next_block = block->next;
    }
    return 0x0;
}

/*-- MARK: App ---------------------------------------------------------------*/
#define MAX_DELTA_CHANGES 512
typedef struct app_t {
    project_t working_project;
    struct deltas_t {
        bool active;
        struct changes_t {
            enum delta_kind_t {
                NOP_DELTA,
                DELTA_INSERT,
                DELTA_RETAIN,
                DELTA_DELETE
            } type;
            action_t action;
            size_t amount;
        } changes[MAX_DELTA_CHANGES];
        size_t at;
    } deltas;
    char* error;
} app_t;

/*
** Initialize the app. This function returns a pointer that is intended to be
** used in all of the public API functions.
*/
app_t*
create_shape_the_pixel(void) {
    app_t* app = SHAPEAUX_CALLOC(1, sizeof(app_t));
    app->error = "";
    return app;
}

void
destroy_shape_the_pixel(struct app_t* stp_app) {
    if (stp_app != 0x0) {
        // Free the memory of the existing project - if there is no existing project
        // the calls to SHAPEAUX_FREE are not done.
        shapeaux_free_project(&(stp_app->working_project));
        // Free the whole app:
        SHAPEAUX_FREE(stp_app);
    }
}

void
create_project(app_t* const app, char const project_id[static 1]) {
    shapeaux_id_t const some_id = shapeaux_create_id(project_id);
    
    shapeaux_create_project(&(app->working_project), some_id);
}

size_t fills_length(app_t const * const app) {
    return app->working_project.state.fills_length;
}

fill_t const * fill(app_t const * const app, size_t index) {
    return
        (index >= app->working_project.state.fills_length) ?
        0x0 : &(app->working_project.state.fills[index]);
}

size_t shapes_length(app_t const * const app) {
    return app->working_project.state.shapes_length;
}

shape_t const * shape(app_t const * const app, size_t index) {
    return
        (index >= app->working_project.state.shapes_length) ?
        0x0 : &(app->working_project.state.shapes[index]);
}

size_t styles_length(app_t const * const app) {
    return app->working_project.state.styles_length;
}

style_t const * style(app_t const * const app, size_t index) {
    return
        (index >= app->working_project.state.styles_length) ?
        0x0 : &(app->working_project.state.styles[index]);
}

size_t layers_length(app_t const * const app) {
    return app->working_project.state.layers_length;
}

layer_t const * layer(app_t const * const app, size_t index) {
    return
        (index >= app->working_project.state.layers_length) ?
        0x0 : &(app->working_project.state.layers[index]);
}


static size_t
shapeaux_deltas_insert_action(app_t* const stp_app, action_t const * action) {
    stp_app->deltas.changes[stp_app->deltas.at] =
    (struct changes_t){
        .type = DELTA_INSERT,
        .action = *action
    };
    stp_app->deltas.at += 1;
    return stp_app->deltas.at;
}

/*
** Inserts an action in shape the pixel app. This function looks at the current
** delta state, and adds this action to the delta changes if it is happening
** while deltas are active (between `deltas_start()` and `deltas_apply()` calls)
**
** Otherwise it adds the action to the project list of actions to be
** applied directly to the state when a `change_state()` occurs.
**
** It returns the position where the action was placed at. Either
** project.at or deltas.at (the action index within each respective array
** can be calculated with the return value of this function - 1).
*/
static size_t
shapeaux_insert_action(app_t* const stp_app, action_t * const action) {
    // Check if this is a delta action, if so, then add it to the deltas
    if (stp_app->deltas.active) {
        // Otherwise apply the action to the project state
        return shapeaux_deltas_insert_action(stp_app, action);
    } else {
        // Otherwise apply the action to the project state
        return shapeaux_project_insert_action(
                                            &(stp_app->working_project),
                                            action,
                                            stp_app->working_project.at);
    }
}

/*
** Public function that creates the action PAINT_LAYER_PIXEL.
** The action is inserted into the project, and the position it was inserted
** is returned. It can be inserted into the current delta changes or directly
** into the project action blocks list - it depends whether there is an ongoing
** delta change or not - by calling `shapeaux_insert_action()`.
**
*/
size_t
action_paint_layer_pixel(app_t* const app,
                         char const * const layer_id,int32_t const x,
                         int32_t const y, char const style_id[static 1],
                         uint8_t const rotation) {
    action_t action = {
        .type = PAINT_LAYER_PIXEL,
        .int_vectors = { [0] = (int2_t){ .x = x, .y = y } },
        .values = { [0] = (double) rotation },
        .ids = {
            [0] = shapeaux_create_id(layer_id),
            [1] = shapeaux_create_id(style_id),
        }
    };
    // Create and add action to the project
    return shapeaux_insert_action(app, &action);
}

/*
** Applies a set of actions to a scene. Returns an error if unsuccessful or
** 0x0 if successful (no errors happened).
** This method receives the state to change, a project with a block of actions,
** and the position on that block to start applying the actions to the state.
** All actions are applied from the specified position of the block up until
** the end - the last action of the last block available.
*/
static char *
shapeaux_apply_actions(scene_t* const state,
                       project_t* const project, size_t const from) {
    // Start by checking if the `from` action index is within bounds
    if (from >= project->at) {
        if (from != 0) {
            return shapeaux_error_d("paint_layer_pixel(): index is out of bounds"
                             , (int)(project->at));
        } else {
            return 0x0;
        }
    }
    // Fetch the starting block
    struct actions_block_t * block = shapeaux_project_block_for_index(project,
                                                                      from);
    // Adjust the index to the actions array in the block
    size_t action_in_block_index = from % BLOCK_SIZE_PROJECT_ACTION;
    // Keep track of the number of actions processed, this is useful to know
    // the stopping condition, which happens at the number of actions that the
    // project contains
    size_t applied_actions = 0;
    char * error = 0x0;
    while (block) {
        // Traverse the actions fixed array in the block. If this is the first
        // block, then `i` starts at the offset of its action arrays where
        // `from` is pointing at
        for(size_t i = action_in_block_index;
            i < BLOCK_SIZE_PROJECT_ACTION;
            ++i, ++applied_actions) {
            // Don't apply more actions after the project maximum is reached
            if ((from + applied_actions) >= project->at) {
                return 0x0;
            }
            // Apply the current action
            error = shapeaux_apply_one_action(state, &(block->actions[i]));
            // If an error was produced in this action, then immediately
            // return it and don't process any more actions
            if (error != 0x0) {
                return error;
            }
        } // for()
        block = block->next;
        // The action index offset into the block actions array is only needed
        // for the first block where the `from` index falls into
        // all other blocks that come after it start applying actions at the 0
        // index of their actions fixed array:
        action_in_block_index = 0;
    } // while()

    return 0x0;
}

/*
** Changes the current working project state by applying all the project actions
** from the provided position in the `from` argument up until the end of the
** actions list.
** Returns the position of the current project action log after the actions
** have been applied to it.
*/
size_t
change_state(app_t* const app, size_t from) {
    // Update the checkpoints:
    
    char * error = shapeaux_update_empty_checkpoints(
                                        &(app->working_project));
    
    if (error == 0x0) {
        error =
            shapeaux_apply_actions(&(app->working_project.state),
                                   &(app->working_project),
                                   from);
    }
    // If some action was unsuccesful, put its error in the main app error
    // string
    if (error != 0x0) {
        if (app->error[0] == '\0') {
            app->error = error;
            perror(app->error);
        } else {
            // Disregard the error if there is already one error in the state
            // that has not yet been addressed and consequently cleared.
            SHAPEAUX_FREE(error);
        }
    } else {
        // No errors, everything was successful.
        // Update the state_at to point to the last applied action + 1
        app->working_project.state_at = app->working_project.at;
    }
    return app->working_project.state_at;
}

/*
** Clears the error message from the main app state.
*/
void
clear_error(app_t* const app) {
    // Error state is the empty string when there are no errors to report
    if (app->error[0] != '\0') {
        // Error messages are set with the internal `shapeaux_error_s|d`
        // functions. These functions call SHAPEAUX_CALLOC to create the error
        // message.
        SHAPEAUX_FREE(app->error);
        // No errors, set it as the empty string:
        app->error = "";
    }
}

/*
** Reverts the project state to match the one with all the actions applied to
** it up to, but not including, the action with the index supplied in the `to`
** argument.
** The action at the index "to" is not applied. This makes it possible to
** revert back to the initial state with `revert_state(0)`.
*/
void
revert_state(app_t* const app, size_t to) {
    // Start by checking if the `to` action index is within bounds
    if (to >= app->working_project.at) {
        // Set an error and exit
        app->error = shapeaux_error_d("revert_state(): out of bounds",
                                      (int)to);
        return;
    }
    // Get the action block that the `to` index points to
    struct actions_block_t * block = shapeaux_project_block_for_index(
                                            &(app->working_project),
                                            to);
    // Clone the checkpoint of that block
    scene_t rev_state = {0};
    shapeaux_clone_scene(&rev_state, &(block->checkpoint));
    // apply all the actions of that block up to the `to` index from the arg
    size_t const action_in_block_index = to % BLOCK_SIZE_PROJECT_ACTION;
    for (size_t i = 0; i < action_in_block_index; ++i) {
        // Get the action to apply
        action_t const action = block->actions[i];
        // Apply the current action
        char * const error = shapeaux_apply_one_action(&rev_state, &action);
        // If an error was produced in this action, then immediately
        // return and don't process any more actions
        if (error != 0x0) {
            app->error = error;
            return;
        }
    }
    // Set the project "state_at" attribute to point to the action index it
    // is at
    app->working_project.state_at = to;
    // Finally set the state to be the state reverted to
    app->working_project.state = rev_state;
}

/*-- MARK: Deltas ------------------------------------------------------------*/

// https://docs.yjs.dev/api/delta-format

void deltas_start(app_t* const app) {
    app->deltas.active = true;
    app->deltas.at = 0;
    // Clear the changes array:
    struct changes_t delta_nop = {0};
    for (size_t i = 0; i < MAX_DELTA_CHANGES; ++i) {
        app->deltas.changes[i] = delta_nop;
    }
}

void deltas_stop(app_t* const app) {
    app->deltas.active = false;
}

void delta_retain(app_t* const app, size_t amount) {
    app->deltas.changes[app->deltas.at] =
    (struct changes_t){
        .type = DELTA_RETAIN,
        .amount = amount,
    };
    app->deltas.at += 1;
}

void delta_delete(app_t* const app, size_t amount) {
    app->deltas.changes[app->deltas.at] =
    (struct changes_t){
        .type = DELTA_DELETE,
        .amount = amount,
    };
    app->deltas.at += 1;
}

/*
** Adjusts the project actions list with each delta present on the deltas
** struct. This adjustment can include the following changes to the project
** actions list:
** - Shift all actions to create space for a new action to be inserted in a
**   given index
** - Remove an action (setting its type as a NOP)
** - Retain a given number of actions in the array
**
** This function is responsible to transform a delta into a project action. The
** list of project actions can be applied to the current working state with
** `change_state()`.
*/
void deltas_apply(app_t* const app) {
    project_t * const proj = &(app->working_project);
    // Keeps track of the index in the project actions list where the current
    // action is going to be applied at.
    // The value of `project_actions_at` starts at 0 and changes with the
    // RETAIN delta, or when an action is inserted (increasing it by 1).
    size_t project_actions_at = 0;
    char * error;
    for (size_t i = 0; i < app->deltas.at; ++i) {
        struct changes_t const change = app->deltas.changes[i];
        switch (change.type) {
            case NOP_DELTA: continue;
            case DELTA_INSERT:
                // A delta is always inserted at position 0 (the start of the
                // actions list). In order to insert a delta in other positions
                // the list of actions needs to be retained or
                if (proj->at > 0) {
                    // Shift the project actions blocks in order for it to have
                    // an empty space in the index `project_actions_at` to hold
                    // the current action
                    error = shapeaux_project_shift_actions_block(
                                                            proj,
                                                            project_actions_at);
                    if (error != 0x0) {
                        perror(error);
                        exit(EXIT_FAILURE);
                    }
                }
                
                // Insert the action into the project actions block, at the
                // index of `project_actions_at`
                shapeaux_project_insert_action(proj,
                                               &(change.action),
                                               project_actions_at);
                // Finally, update the `project_actions_at` to point at the next
                // position:
                project_actions_at += 1;
                break;
            case DELTA_DELETE:
                // The delta action has an amount property that specifies the
                // number of items to delete.
                for (size_t j = 0; j < change.amount; j++) {
                    // Set the project action at the index to
                    // be a NOP
                    action_t nop = {0};
                    shapeaux_project_insert_action(proj,
                                                   &nop,
                                                   project_actions_at);
                    project_actions_at += 1;
                }
                break;
            case DELTA_RETAIN:
                // Move the current position forward in order to retain the
                // amount of elements specified by the retain delta amount
                project_actions_at += change.amount;
                break;
        }
    }
    // After all deltas are applied, clear the delta state
    app->deltas.at = 0;
    // Clear the changes array:
    struct changes_t delta_nop = {0};
    for (size_t i = 0; i < MAX_DELTA_CHANGES; ++i) {
        app->deltas.changes[i] = delta_nop;
    }
}

/*-- MARK: Pixel Iterator ----------------------------------------------------*/
typedef struct pixel_iter_value_t {
    pixel_t const * px;
    style_t const * style;
    fill_t const * background;
    struct diff_state {
        diff_kind_t px;
        diff_kind_t background;
        diff_kind_t style;
        diff_kind_t shape;
    } diff;
} pixel_iter_value_t;

#define VALUES_PER_ITER 4
typedef struct pixel_iter_t {
    // The current iterator position
    size_t at;
    bool is_done;
    // This is a pixel_iter_value_t[VALUES_PER_ITER]
    // it is stored as a pointer because the user can specify the memory where
    // each set of iterator values will be written at.
    pixel_iter_value_t* value;
    // This parameter is set to true when the iterator is started without a
    // memory storage being supplied (i.e. memory for the iterator is set as
    // null when it starts, which means that the iterator must allocate its own
    // memory and consequently free it when it ends)
    bool value_needs_free;
    // The layer_index is useful to know which layer to fetch the next
    // layer_block once the iteration is done with the current `layer_block`
    size_t layer_index;
    // The current layer_block being iterated
    struct pixels_block_t const * layer_block;
    // The app of this iterator
    app_t* app;
    // The previous scene is a reference to the current project scene at the
    // moment of the last iteration. It is set at the `pixel_iter_stop()` and is
    // useful to check the diffs of each pixel (in pixel_iter_value_t.diff).
    // The value diffing is performed by the local function
    // `shapeaux_set_iterator_value()`
    scene_t previous_scene;
    // Is this iterator currently processing the deleted pixels?
    // After processing all the pixels in the current scene, the iterator
    // proceeds to check which pixels were deleted. This operation consists in
    // reading the previous scene pixels that are not present at the current
    // scene. To do this the `layer_block` pointer must be set to be pointing
    // at the previous scene instead of the current one. This flag signals
    // that this change of scene pointers has happened and is useful to allow it
    // to be done exactly once per full iteration cycle. Read more about the
    // deletion process in the function `shapeaux_iter_deleted()`.
    bool is_processing_deleted;
} pixel_iter_t;

static void
shapeaux_clear_iter_value(pixel_iter_t * const iter) {
    for (size_t i = 0; i < VALUES_PER_ITER; i++) {
        iter->value[i] = (pixel_iter_value_t){0};
    }
}

pixel_iter_t*
create_pixel_iter(void) {
    return SHAPEAUX_CALLOC(1, sizeof(pixel_iter_t));
}

size_t pixel_iter_value_size(void) {
    return sizeof(pixel_iter_value_t);
}
size_t pixel_size(void) {
    return sizeof(pixel_t);
}
size_t pointer_size(void) {
    return sizeof(pixel_iter_value_t*);
}
size_t fill_type_size(void) {
    return sizeof(fill_kind_t);
}

void
pixel_iter_stop(pixel_iter_t * const iter) {
    // The iterator has ended, clone the current scene
    shapeaux_clone_scene(&(iter->previous_scene),
                         &(iter->app->working_project.state));
    // Free the iterator value memory area if it needs to be free
    // This area needs to be free when it was allocated by the pixel_iter_start
    // function (instead of being user provided).
    if (iter->value_needs_free) {
        SHAPEAUX_FREE(iter->value);
    }
}

size_t
pixel_iter_start(pixel_iter_t * const iter,
                 app_t * const app,
                 size_t const layer_index,
                 pixel_iter_value_t * const value_storage) {
    
    // Reset it as 0
    // *iter = (pixel_iter_t){0};
    iter->at = 0;
    iter->is_done = false;
    
    // Check if the working project has actions to iterate
    // Set the iter layer index and block from the working project state
    iter->layer_index = layer_index;
    iter->layer_block = app->working_project.state.layers[layer_index].blocks;
    iter->app = app;
    iter->is_processing_deleted = false;
    
    if (value_storage != 0x0) {
        iter->value = value_storage;
        iter->value_needs_free = false;
    } else {
        iter->value = SHAPEAUX_CALLOC(VALUES_PER_ITER,
                                      sizeof(pixel_iter_value_t));
        iter->value_needs_free = true;
    }
    
    return sizeof(pixel_iter_value_t);
}

static void
shapeaux_iterator_diff_value(pixel_iter_t * const iter,
                             size_t const value_at) {
    // If the iterator is currently processing the deleted pixels, then
    // the diff for the `px` struct is always "WAS_REMOVED" and all the other
    // diff values are set as UNCHANGED, since this means that the iterator is
    // pointing at a pixel from the previous state, so there is no change to be
    // considered at this point (changes reflect the pixels in the current
    // state)
    if (iter->is_processing_deleted) {
        iter->value[value_at].diff.px = DIFF_WAS_REMOVED;
        iter->value[value_at].diff.background = DIFF_UNCHANGED;
        iter->value[value_at].diff.style = DIFF_UNCHANGED;
        iter->value[value_at].diff.shape = DIFF_UNCHANGED;
        return;
    }

    // The current pixel value to check for differences
    pixel_iter_value_t const current = iter->value[value_at];

    // Get the pixel from the previous state
    pixel_t const * const px = shapeaux_get_pixel(&(iter->previous_scene),
                                                  iter->layer_index,
                                                  current.px->at);
    // A pixel is new if the previous pixel is null
    if (px == 0x0) {
        // This is a new pixel, it did not exist in the previous state
        iter->value[value_at].diff.px = DIFF_IS_NEW;
    } else {
        // A pixel is different if its rotation or style_id has changed
        bool is_changed_px = px->rotation != current.px->rotation
                      || !shapeaux_id_equal(&(px->style), &(current.px->style));
        iter->value[value_at].diff.px =
            is_changed_px ? DIFF_HAS_CHANGED : DIFF_UNCHANGED;
    }

    // Get the bg fill from the previous state
    fill_t const * const bg = shapeaux_get_fill(&(iter->previous_scene),
                                                &(current.background->id));
    if (bg == 0x0) {
        // This is a new background, it did not exist in the previous state
        iter->value[value_at].diff.background = DIFF_IS_NEW;
    } else {
        // This is not a new background, check if something has changed
        bool is_changed_bg = !shapeaux_fill_equal(current.background, bg);
        iter->value[value_at].diff.background =
            is_changed_bg ? DIFF_HAS_CHANGED : DIFF_UNCHANGED;
    }

    // Get the style from the previous state
    style_t const * const style = shapeaux_get_style(&(iter->previous_scene),
                                                     &(current.style->id));
    if (style == 0x0) {
        // This is a new style, it did not exist in the previous state
        iter->value[value_at].diff.style = DIFF_IS_NEW;
    } else {
        // This is not a new style, check if something has changed
        bool is_changed_style = !shapeaux_style_equal(current.style, style);
        iter->value[value_at].diff.style =
            is_changed_style ? DIFF_HAS_CHANGED : DIFF_UNCHANGED;
    }

    // Get the shape from the previous state
    shape_t const * const prev_shape = shapeaux_get_shape(
                                                &(iter->previous_scene),
                                                &(current.style->shape_id));
    // Get the shape from the current state
    shape_t const * const cur_shape = shapeaux_get_shape(
                                            &(iter->app->working_project.state),
                                            &(current.style->shape_id));
    if (prev_shape == 0x0) {
        // This is a new shape, it did not exist in the previous state
        iter->value[value_at].diff.shape = DIFF_IS_NEW;
    } else {
        // This is not a new shape, check if something has changed
        iter->value[value_at].diff.shape =
            shapeaux_shape_equal(cur_shape, prev_shape) ? DIFF_UNCHANGED
                                                        : DIFF_HAS_CHANGED;
    }

}

static void
shapeaux_set_iterator_value(pixel_iter_t * const iter,
                            pixel_t const * const px,
                            size_t const value_at) {
    // Get the style from the current working project scene
    scene_t const * const scene = &(iter->app->working_project.state);
    style_t const * const style = shapeaux_get_style(scene, &(px->style));
    if (style == 0x0) {
        // No style was found, return an empty value
        iter->value[value_at] = (pixel_iter_value_t){0};
        return;
    }

    iter->value[value_at] = (pixel_iter_value_t) {
        .px = px,
        .style = style,
        .background = shapeaux_get_fill(scene, &(style->background_id))
    };
    
    // Perform the diff with the previous scene values and set the iterator diff
    shapeaux_iterator_diff_value(iter, value_at);
}

static pixel_iter_t*
shapeaux_iter_deleted(pixel_iter_t * const iter, size_t const value_at) {
    size_t pixels_found = value_at;
    // The steps performed by this function are similar to the `pixel_iter_next`
    // but there are some nuances, as they are focused on looking for the
    // pixels present in the previous state.
    // At this point, any pixel present in the previous state is considered
    // a "deleted" pixel, because the pixels found by pixel_iter_next were also
    // removed from the previous state.
    
    // Clear the current values array ff this is a new set of iterator values
    if (value_at == 0) {
        shapeaux_clear_iter_value(iter);
    }
    struct pixels_block_t const * block = iter->layer_block;
    while (block != 0x0) {
        // Iterate through all the non-empty pixels and fill the iterator values
        // array
        for (size_t i = iter->at % LAYER_BLOCK_SIZE;
             i < LAYER_BLOCK_SIZE;
             ++i) {
            iter->at++;
            // Get the pixel for this block position index
            pixel_t const px = block->pixels[i];
            // Check if it is empty, empty pixels have an empty style id string
            if (px.style.value[0] != '\0') {
                // Found a non-empty pixel
                // Set the iter.value with it and perform the diffs
                // When looking at the deleted pixels, the diffs are always
                // UNCHANGED, except for the `px` value, which is set as
                // WAS_REMOVED
                shapeaux_set_iterator_value(iter,
                                            &(block->pixels[i]),
                                            pixels_found);
                // A new pixel was set in this iterator values array,
                // advance the current value counter and check if this is the
                // time to return the iterator
                pixels_found++;
                if (pixels_found == VALUES_PER_ITER) {
                    // The block could have changed, in which case update it in
                    // the iterator. Updating it is not an immediate operation
                    // because the block might need to be advanced if the
                    // iter->at is now a multiple of LAYER_BLOCK_SIZE (which
                    // means that this iterator "at" has reached the end of the
                    // current block)
                    iter->layer_block =
                        iter->at % LAYER_BLOCK_SIZE == 0 ? block->next : block;
                    return iter;
                }
            }
        }
        // All the pixels in this block were processed, if this point is reach
        // this means that not a single valid pixel was found and it is time to
        // proceed to the next block.
        block = block->next;
    }
    
    // The iterator has reached the end.
    // Check if there are still values to process, in which case, do not return
    // 0x0, but return an iterator that cannot proceed - thus allowing the
    // remained values to be read/processed by the iterator caller.
    if (pixels_found > 0) {
        iter->layer_block = 0x0;
        return iter;
    }
    // All pixels were empty, this is the end of this layer iterator
    iter->is_done = true;
    iter->layer_block = 0x0;
    return 0x0;
}

pixel_iter_t*
pixel_iter_next(pixel_iter_t * const iter) {
    // Check that this iterator is valid to proceed.
    if (iter == 0x0 || iter->is_done) {
        return iter;
    }
    // If the iterator is currently processing deleted pixels, pass the control
    // to the function that performs this.
    if (iter->is_processing_deleted) {
        return shapeaux_iter_deleted(iter, 0);
    }
    // Each iterator returns more than 1 value. The number of values
    // returned is VALUES_PER_ITER. Advancing the iterator means that it
    // will carry another set of VALUES_PER_ITER non-empty pixels.
    size_t pixels_found = 0;
    // Start by clearing the iterator value struct of arrays
    shapeaux_clear_iter_value(iter);
    // Get the next non-empty pixel from this iterator layer
    struct pixels_block_t const * block = iter->layer_block;
    while (block != 0x0) {
        // The pixel entry could be returned immediately instead of using this
        // `for()`; however that would mean that empty pixels could be returned
        // by the iterator, and as a design decision the iterator should always
        // return valid non-empty pixels, which means that the block pixels
        // array needs to be traversed up until the next non-empty pixel.
        for (size_t i = iter->at % LAYER_BLOCK_SIZE;
             i < LAYER_BLOCK_SIZE;
             ++i) {
            // Incrementing the iterator position is done here, before
            // processing the pixel value from the layer. This ensures that
            // the iterator position is valid in case of an early return
            // i.e. a non-empty pixel is found
            iter->at++;
            // Get the pixel for this block position index
            pixel_t const px = block->pixels[i];
            // Check if it is empty, empty pixels have an empty style id string
            if (px.style.value[0] != '\0') {
                // Found a non-empty pixel
                // Set the iter.value with it and perform the diffs/changes
                shapeaux_set_iterator_value(iter,
                                            &(block->pixels[i]),
                                            pixels_found);
                // A new pixel was set in this iterator values array,
                // advance the current value counter and check if this is the
                // time to return the iterator
                pixels_found++;
                // Clear this pixel from the previous state of the iterator,
                // this is useful to allow the iteration to signal which pixels
                // have been removed. The pixels that were removed will be the
                // ones left on the previous state after the iteration has
                // finished
                shapeaux_layer_delete(
                            &(iter->previous_scene.layers[iter->layer_index]),
                            px.at);
                // Check if the iterator values array has been filled, in which
                // case, it is not needed to cycle further this iteration next()
                // call
                if (pixels_found == VALUES_PER_ITER) {
                    // The block could have changed, in which case update it in
                    // the iterator. Updating it is not an immediate operation
                    // because the block might need to be advanced if the
                    // iter->at is now a multiple of LAYER_BLOCK_SIZE (which
                    // means that this iterator "at" has reached the end of the
                    // current block)
                    iter->layer_block =
                        iter->at % LAYER_BLOCK_SIZE == 0 ? block->next : block;
                    return iter;
                }
            }
        }
        // All the pixels in this block were processed, if this point is reach
        // this means that not a single valid pixel was found and it is time to
        // proceed to the next block.
        block = block->next;
    }
    // The current scene pixels have all been processed, start processing the
    // deleted pixels.
    iter->is_processing_deleted = true;
    iter->at = 0;
    // Set the layer_block to be pointing at the first layer block of the
    // previous state, because the deletion process iterates through all the
    // pixels that are still present at the previous state layer.
    iter->layer_block = iter->previous_scene.layers[iter->layer_index].blocks;
    return shapeaux_iter_deleted(iter, pixels_found);
}

int32_t x(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].px->at.x;
}

int32_t y(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].px->at.y;
}

uint8_t rotation(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].px->rotation;
}

const fill_t *
background(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].background;
}

const pixel_t *
px(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].px;
}

diff_kind_t
diff_px(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].diff.px;
}

diff_kind_t
diff_background(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].diff.background;
}

diff_kind_t
diff_style(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].diff.style;
}

diff_kind_t
diff_shape(pixel_iter_t const * const iter, size_t const pos) {
    return iter->value[pos].diff.shape;
}

/*
 
 Pixel Iterator
 
 px_iter = start(layer_index_number)
 
 bg = background(px_iter) // fill_t
 
 editor_iter = editor(px_iter)
 
 relation(editor_iter)
 ...
 
 pxi = next(pxi)
 
 
 */




/*-- MARK: Tests -------------------------------------------------------------*/
/*
 ** Include the unit tests framework if shapeaux is being built for tests
 **/
#ifdef SHAPEAUX_TESTS
/*
** The color constructor where alpha is initialized to be 1.0f.
*/
static color_t
shapeaux_create_color(uint8_t r, uint8_t g, uint8_t b) {
    return (color_t){ .r = r, .g = g, .b = b, .a = 1.0 };
}

/*
** Checks if the fill id is null. Like the other objects in shapeaux, a fill is
** empty if its id is null/empty string.
*/
static bool
shapeaux_fill_is_empty(fill_t some_fill) {
    return some_fill.id.value[0] == '\0';
}


#include "./Tests/shapeaux_tests.h"
#endif /* SHAPEAUX_TEST */
#ifdef __EMSCRIPTEN__
#include <emscripten.h>

int main(void) {
    return 1;
}

#endif /* __EMSCRIPTEN__ */
