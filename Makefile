CC = clang
CFLAGS = -Weverything -Wno-poison-system-directories -Wno-padded -Werror -std=c17
exported_functions = _malloc _create_shape_the_pixel _create_project _action_paint_layer_pixel _deltas_start _deltas_apply _deltas_stop _delta_retain _delta_delete _change_state _revert_state _clear_error _create_pixel_iter _pixel_iter_start _pixel_iter_next _pixel_iter_stop _fill_color _fill_type _fill_id _background _x _y _rotation _pixel_iter_value_size _pixel_size _pointer_size _px _fill_type_size _diff_px _diff_background _diff_style _diff_shape _layer _layer_id _layers_length _style _style_id _styles_length _shape _shape_id _shapes_length _fill _fills_length
comma := ,
empty:=
space := $(empty) $(empty)
EMFLAGS = $(CFLAGS) -s ASSERTIONS=1 -s WASM=1 -s EXPORTED_FUNCTIONS='[$(subst $(space),$(comma),$(exported_functions))]' -s EXPORTED_RUNTIME_METHODS='["ccall","cwrap","UTF8ToString", "getValue"]' -s MODULARIZE=1 -s 'EXPORT_NAME="shapeaux"' -s ALLOW_MEMORY_GROWTH=1 -s SAFE_HEAP=1

build: deps Web/js/shapeaux.wasm

Web/external/dependencies.js: Web/external/node_modules 
Web/external/node_modules:
	yarn install --cwd Web/external

Web/external/dist/dependencies.js: Web/external/dependencies.js
	@-mkdir -p Web/external/dist
	parcel build --no-scope-hoist $<

Web/external/dist/deno_bundle.js: Web/external/deno_dependencies.ts
	@-mkdir -p Web/external/dist
	deno bundle $< > $@

Web/external/dist/environment.js: Web/external/environment.ts
	deno run --allow-read --allow-env $< > $@

deps: Web/external/dist/deno_bundle.js Web/external/dist/dependencies.js Web/external/dist/environment.js
	@-mkdir -p Web/js/deps
	@-cp -R Web/external/dist/* Web/js/deps

.cbuild/munit.o:
	@mkdir -p .cbuild
	@$(CC) -c Shapeaux/Tests/munit.c -o .cbuild/munit.o

shapeaux_tests: .cbuild/munit.o
	@rm -f .cbuild/tests~
	@rm -f .cbuild/shapeaux_tests.o~
	@$(CC) -DSHAPEAUX_TESTS -O2 $(CFLAGS) -Wno-unused-parameter -Wno-unused-variable -Wno-vla -c Shapeaux/shapeaux.c -o .cbuild/shapeaux_tests.o
	@$(CC) -DSHAPEAUX_TESTS -O2 .cbuild/munit.o .cbuild/shapeaux_tests.o -o .cbuild/tests
	@./.cbuild/tests

debug: 
	@rm -f .cbuild/tests~
	@rm -f .cbuild/shapeaux_tests.o~
	@rm -f .cbuild/munit.o~
	$(CC) -O0 -g -c Shapeaux/Tests/munit.c -o .cbuild/munit.o
	$(CC) -DSHAPEAUX_TESTS -O0 -g $(CFLAGS) -Wno-unused-parameter -Wno-unused-variable -Wno-vla -c Shapeaux/shapeaux.c -o .cbuild/shapeaux_tests.o
	$(CC) -DSHAPEAUX_TESTS -O0 -g .cbuild/munit.o .cbuild/shapeaux_tests.o -o .cbuild/tests
	lldb .cbuild/tests

.cbuild/shapeaux.o:
	@$(CC) -O2  -c Shapeaux/shapeaux.c -o .cbuild/shapeaux.o

.cbuild/ccompmunit.o:
	@mkdir -p .cbuild
	@ccomp -c Shapeaux/Tests/munit.c -o .cbuild/ccompmunit.o

typecheck:
	@rm -f .cbuild/tests~
	@rm -f .cbuild/shapeaux_tests.o~
	@ccomp -DSHAPEAUX_TESTS -Wall -c Shapeaux/shapeaux.c -o .cbuild/shapeaux_tests_ccomp.o
	@ccomp -DSHAPEAUX_TESTS .cbuild/ccompmunit.o .cbuild/shapeaux_tests_ccomp.o -o .cbuild/typecheck
	@./.cbuild/typecheck

serve:
	@mkdir -p .logs
	@touch .logs/error.log
	@echo "" > .logs/error.log
	@echo "Listening at localhost:8000"
	@echo "Error log at ./logs/error.log"
	@nginx -c $(shell pwd)/Web/dev.nginx.conf -p $(shell pwd)

Web/js/shapeaux.wasm: Shapeaux/shapeaux.c
	emcc -O2 Shapeaux/shapeaux.c -o "Web/js/shapeaux.mjs" $(EMFLAGS)

wasm: Shapeaux/shapeaux.c
	@touch Web/js/shapeaux.wasm
	@rm Web/js/shapeaux.wasm
	emcc -O2 Shapeaux/shapeaux.c -o "Web/js/shapeaux.mjs" $(EMFLAGS)

watch_wasm:
	find Shapeaux/shapeaux.* | entr -s 'make wasm'

clean:
	#@rm .cbuild/shapeaux.*
	#@rm .cbuild/libshapeaux.*
	@rm Web/js/shapeaux.wasm
	@rm Web/external/dist/*
	@rm Web/js/deps/*
	@git checkout Web/js/deps/README.md
